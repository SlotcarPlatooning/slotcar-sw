s = tf('s')
% G = tf(3.2, [0.3 1])
% F = 1/ (1/3.2 * s + 1)

% h  = 1


k = 0.156
Omega = 0.661
omega = 0.396
Td = 0.146 

k = 1.136
Omega = 0.5
omega = 1.067
Td = 0.287

F = k / (s^2 + 2*Omega*omega*s + omega^2) * exp(-Td*s)

G = 1/s * F
k1 = 0.45
k2 = 0.25
k3 = 0.15
k4 = 0.1

hp = 0.9
hl = 1.1

Kp = k1 * s + k2;
Kl = k3 * s + k4;

Pp = (hp*s + 1);
Pl = (hl*s + 1);

% D = exp(-delta * s)
D = 1

T = (D + G * Kp )/ (1 + G * (Kp * Pp + Kl*Pl)) 

bode(T)

%%

