close all
% dir = 'D:\Dropbox\slotcar\slotcar-git\slotcar-doc\ladmartin-thesis\fig\';
dir = 'C:\Users\ladmartin\Dropbox\slotcar\slotcar-git\slotcar-doc\ladmartin-thesis\fig\'

% N = 4
% my_plot(strcat(dir,'pd'),'PD.mat',[3 4.5],[0.1 0.6],[0.15 0.3])
% my_plot(strcat(dir,'pi_bid'),'PI_2_2_BID_1.mat',[0 12],[-0.1 0.6],[0 0.4])
% my_plot(strcat(dir,'pi_bid_2'),'PI_BID_draha_4auta_3.mat',[2 8],[-0.1 0.6],[0 0.4])
% my_plot(strcat(dir,'pi'),'PI_draha_4auta.mat',[1 8],[-0.1 0.6],[0 0.4])
% my_plot(strcat(dir,'pi_2'),'PI_draha_4auta.mat',[7 12],[0.2 0.7],[0.1 0.3])
% my_plot(strcat(dir,'pi_1'),'PI_draha_4auta_2.mat',[1 8],[-0.1 0.6],[0 0.4])
% my_plot(strcat(dir,'pi_bid_2'),'PI_BID_draha_4auta.mat',[0 12],[-0.1 0.6],[0 0.4])
% 
% % my_plot(strcat(dir,'pi_bid_2'),'kruh_PI_01.mat',[0 12],[-0.1 0.6],[0 0.4])
% my_plot(strcat(dir,'pi_bid_2'),'draha_PI_5aut.mat',[0 12],[-0.1 0.6],[0 0.4])
% 
% my_plot(strcat(dir,'pd_ff_pred'),'PD_FF_LEADER_draha_4auta.mat',[0 12],[-0.1 1],[0 0.4])
% my_plot(strcat(dir,'pi_bid_2'),'draha_PI_5aut.mat',[0 12],[-0.1 0.6],[0 0.4])

my_plot_model(strcat(dir,'model_free'), 'model_5.mat',[0 20],[0 4], [-0.1 0.25], [-0.1 6])
my_plot_model(strcat(dir,'model'), 'model_on_track.mat',[4 20], [0 3], [0 1], [0 4])
% my_plot_v_model(strcat(dir,'v_model'), 'model_v_3.mat',[0 12], [0 2], [0 1])
% %  my_plot_v_model(strcat(dir,'v_model'), 'model_v_track_1.mat',[0 12], [0 2], [0 0.8])
% 
% my_plot(strcat(dir,'cacc_leader'), 'CACC_leader_1.mat',[4 6], [0.2 0.8], [0.1 0.2])
% my_plot(strcat(dir,'cacc_pred_v'), 'cacc_pred_v.mat',[4 6], [0.2 0.8], [0.1 0.2])
% my_plot(strcat(dir,'cacc_pred_u'), 'cacc_pred_u.mat',[4 6], [0.2 0.8], [0.1 0.2])
%  
% my_plot(strcat(dir,'pd_best'), 'PD_best.mat',[4 12], [0.2 0.8], [0.1 0.3])
% 
% 
% my_plot(strcat(dir,'pd_bid_ff'), 'PD_bid_ff_1.mat',[4 8], [0.0 0.8], [0.1 0.2])
% 
% 
my_plot(strcat(dir,'cacc_leader'), 'CACC_leader_1.mat',[4 6], [0.2 0.8], [0.1 0.2])

function my_plot_model(file, data_name,t_lim, v_lim, c_lim, b_lim)
load(data_name)
figNum = createfiguremodel(velocity, current, bemf, t_lim, v_lim, c_lim,b_lim)
set(figNum, 'PaperPositionMode', 'manual');
set(figNum, 'PaperUnits', 'centimeters');
set(figNum, 'PaperPosition', [0 0 15 12]); % last 2 are width/height.
print(figNum,file,'-depsc','-painters');
end

function my_plot_v_model(file, data_name,t_lim, v_lim, pwm_lim)
load(data_name)
figNum = createfigurevmodel(velocity, pwm, t_lim, v_lim, pwm_lim)
set(figNum, 'PaperPositionMode', 'manual');
set(figNum, 'PaperUnits', 'centimeters');
set(figNum, 'PaperPosition', [0 0 15 10]); % last 2 are width/height.
print(figNum,file,'-depsc','-painters');
end

function my_plot(file, data_name, x_lim, y1_lim, y2_lim)
load(data_name)
% persistent N
[N_v, is_v_ref] = get_car_count(velocity)
[N_d, is_d_ref] = get_car_count(distance)
% sim('rozdelovani','StopTime', num2str(velocity.Time(end)))

if is_v_ref == 1
[t,v_car] = get_car(velocity, 2:N_v+1);
[t,v_sim] = get_car(velocity, N_v+2:2*N_v+1);
else
[t,v_car] = get_car(velocity, 1:N_v);
[t,v_sim] = get_car(velocity, N_v+1:2*N_v);    
end

if is_d_ref == 1
[t,d_car] = get_car(distance, 2:N_d+1);
[t,d_sim] = get_car(distance, N_d+2:2*N_d+1);
else
[t,d_car] = get_car(distance, 1:N_d);
[t,d_sim] = get_car(distance, N_d:2*N_d);    
end

%  = squeeze(velocity.Data());
% d_sim = squeeze(distance_sim.Data);
% v_car = squeeze(velocity_car.Data);
% d_car = squeeze(distance_car.Data);
% d_car = d_car(2:end,:);

figNum = createfigureplatoon(t,v_sim,d_sim,v_car,d_car,x_lim,y1_lim,y2_lim);


% figure(figNum)
% plot(somestuff);  
% pos = get(figNum,'Position');
% set(figNum,'PaperPositionMode','Auto','PaperUnits','centimeters','PaperSize',[13, 10], 'Renderer','painters')
% string(name)
% file = strcat(dir, name);
set(figNum, 'PaperPositionMode', 'manual');
set(figNum, 'PaperUnits', 'centimeters');
set(figNum, 'PaperPosition', [0 0 15 8]); % last 2 are width/height.

print(figNum,file,'-depsc','-painters');
% set(figNum, 'units', 'centimeters', 'pos', [0 0 10 10])

% subplot 221
% plot(velocity_sim)
% subplot 222
% plot(distance_sim)
% 
% subplot 223
% plot(velocity_car)
% subplot 224
% plot(distance_car)
end

function [car_count,is_ref] = get_car_count(ts)
N = getdatasamplesize(ts);
N = N(1);

if mod(N,2) == 0 
    is_ref = 0; 
else
    is_ref = 1;
    N = N -1;
end
N = N/2;
car_count = N;
end


function [t,x] = get_car(ts,car)
x = squeeze(ts.Data(car,1,:)) ;
t = ts.Time;
end
