%% BEMF filter
T = 0.005;
Ts = 1/500;
%% El. current filter
T = 1e-3;
Ts = 1/16000;

%%
% H = tf(Ts/T,[1 -(1-(Ts/T))],Ts)
% step(H)
% bode(H)
%%
% ###############
% T system time const
% Ts sampling period
s = tf('s');
C = 1 / (T * s + 1)
bode(C)
Cd = c2d(C,Ts,'zoh')
% Result is in form 
% b /(z - a)
% y[n] = b * u[n-1] + a * y[n-1]

%%
G = 10*1/(L*s + R);
% C = (L*s + R)/(0.001*s+1)
C = pidtune(G,'PI')
H = (C * G)/(1 + C * G)
Cd = c2d(C,1e-3,'zoh')

bode(H)
step(H)

% G2 = H*k/n/r*(1/(m*s+R))
% C2 = pidtune(G2,'PI')

% step(H)





