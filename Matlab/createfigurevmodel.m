function figure1 = createfigurevmodel(velocity, pwm, t_lim, v_lim, pwm_lim)
figure1 = figure('InvertHardcopy','off','PaperUnits','centimeters',...
    'PaperSize',[15 7],...
    'Color',[1 1 1],...
    'Renderer','painters');

% Create subplot
subplot1 = subplot(2,1,1,'Parent',figure1);
hold(subplot1,'on');
d = squeeze(velocity.Data);
d = d(:,1:2);
plot1 = creategraph(velocity.Time, d, t_lim, v_lim, 'Time (s)', 'Velocity (m/s)');
set(plot1(2),'LineStyle','--','LineWidth',2);
set(plot1(1),'LineWidth',2);
legend({'real', 'sim'},'Interpreter','latex')
box(subplot1,'on');
grid(subplot1,'on');

%%
subplot2 = subplot(2,1,2,'Parent',figure1);
hold(subplot2,'on');
plot2 = creategraph(pwm.Time, squeeze(pwm.Data), t_lim, pwm_lim, 'Time (s)', 'Duty cycle (\%)');
set(plot2(2),'LineStyle','--','LineWidth',2);
set(plot2(1),'LineWidth',2);
box(subplot2,'on');
grid(subplot2,'on');


