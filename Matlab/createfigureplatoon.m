function figure1 = createfigureplatoon(X1, YMatrix1, YMatrix2, YMatrix3, YMatrix4, t_lim, y1_lim, y2_lim)

line_width = 1.2;
text_size = 10;
% Create figure

figure1 = figure('InvertHardcopy','off','Color',[1 1 1]);

% figSize = get(figure1, 'Position') %// gives x left, y bottom, width, height
% text(0.5,0.5,'\textbf{Simulation of the platoon}');

% Create subplot
subplot2 = subplot(2,2,2,'Parent',figure1);
hold(subplot2,'on');

title('\textbf{Front distances of cars}','Interpreter','latex')
[n,m] = size(YMatrix2);
YMatrix2 = [nan*ones(1,m); YMatrix2];
creategraph(X1,YMatrix2,t_lim,y2_lim, '','Distance (m)');
% cstring=[];
% [N,m] = size(YMatrix1);
% for i=1:N-1
%    cstring{end+1} = strcat('Car',num2str(i)); 
% end
% legend(cstring)
% legend('Location','best')

box(subplot2,'on');
grid(subplot2,'on');
% Set the remaining axes properties
set(subplot2,'FontSize',text_size, 'LineWidth', line_width);
% Create subplot
subplot3 = subplot(2,2,3,'Parent',figure1);
hold(subplot3,'on');

creategraph(X1,YMatrix3,t_lim,y1_lim, 'Time (s)',{'\textbf{Experiment}','Velocity (m/s)'});

box(subplot3,'on');
grid(subplot3,'on');
% Set the remaining axes properties
set(subplot3,'FontSize',text_size, 'LineWidth', line_width );
% Create subplot
subplot4 = subplot(2,2,4,'Parent',figure1);
hold(subplot4,'on');
[n,m] = size(YMatrix4);
YMatrix4 = [nan*ones(1,m); YMatrix4];
creategraph(X1,YMatrix4,t_lim,y2_lim, 'Time (s)','Distance (m)');

box(subplot4,'on');
grid(subplot4,'on');
% Set the remaining axes properties
set(subplot4,'FontSize',text_size, 'LineWidth', line_width);


% Create subplot
subplot1 = subplot(2,2,1,'Parent',figure1);
hold(subplot1,'on');

% figSize = get(subplot1, 'Position') %// gives x left, y bottom, width, height

title('\textbf{Velocities of cars}','Interpreter','latex')

creategraph(X1,YMatrix1,t_lim,y1_lim, '',{'\textbf{Simulation}','Velocity (m/s)'})

cstring=[];
[N,m] = size(YMatrix1);
 cstring{end+1} = 'Leader'
for i=1:N-1
   cstring{end+1} = strcat('Car',num2str(i)); 
end



box(subplot1,'on');
grid(subplot1,'on');
% Set the remaining axes properties
set(subplot1,'FontSize',text_size,'LineWidth', line_width);
hL = legend(cstring)
newPosition = [0.52 0.505 0.0 0.0];
newUnits = 'normalized';
set(hL,'Position', newPosition,'Units', newUnits, 'Orientation', 'horizontal');