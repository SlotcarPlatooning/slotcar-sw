%% CAR model constants
is_on_track = 1;
% motor DC gain is 1/(1+R)
% L/R time constant of the motor
R = 8;      % Motor electrical resistance [Ohm]pwm, velocity, velocity2, e_v, current, bemf
L = 2E-3;   % Motor inductance [H]
k = 0.005;
ke = k;  % Motor constant [V/rad/s]
kt = k;  % Motor constant [N*m/A]
r = 10E-3;  % wheels' radius [m]
n = 1/3;    % gears ratio
%% Power source
U = 9.2;     % Power supply [V]
%%
if is_on_track == 0
    % Only wheels
    m = 0.01;   % Car's mass [kg]
    bd = 0.1; % dynamic friction coeficient
    bs = 0.09; % static friction coeficient
else
    % On track
    m = 0.15;   % Car's mass [kg]
    bd = 0.27;   % dynamic friction coeficient
    bs = 0.53;   % static friction coeficient
end

Rmnr = R*m*n*r;

 s = tf('s');
 a = - (k^2/R/m/n^2/r^2 + (bd)/m) ;
 b = k*U/Rmnr;
 c = 1;
 d = 0;
G = k*U/R/m/n/r / (s + (bd)/m + k^2/R/m/r^2/n^2)
 
% C_v = 1.6 + 15* 1/s;

kp_v = 2; % 0.3
ki_v = 10; % 1

kp_d = 5;
kd_d = 0.2;
N_d = 50;

C_v = (s*kp_v + ki_v) / s;
C_d = kp_d + kd_d * N_d / (1 + N_d/s);


G_v = C_v * G / (1 + C_v*G)
F = G_v/s;
G_d = C_d * F / (1 + C_d * F);

dc = 0.1;

(U*dc) * k /Rmnr

Kp = C_d
Kl = 0
Pl = 0
D = exp(-0.03*s)
% cacc
Pp = 0.03 * s + 1
T = (D + G * Kp )/ (1 + G * (Kp * Pp + Kl*Pl)) 


bode(T)



