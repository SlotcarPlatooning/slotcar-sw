function [] = createPlatoon( subsystem_name, N, controller_name )
%CREATEPLATOON Generating platoon of size N with a controller applied on
%each car
%   Detailed explanation goes here
 posY = 50;

%% block variables
sys = subsystem_name;
open_system(sys);
disp(['Generating a platoon with controller ' controller_name ' for the system ' sys '...'])
cont_name = controller_name;

allblocks = find_system(sys, 'LookUnderMasks', 'all', 'SearchDepth', 1, 'FollowLinks', 'on');
%// Alternative, directly, so save one line:
ToKeep = {[sys '/vref'];[sys '/dref'];...
            [sys '/x'];[sys '/v'];[sys '/d'];[sys '/v_des'];...
            [sys '/toVref'];[sys '/toDref'];...
            [sys '/fromX'];[sys '/fromV'];[sys '/fromD'];[sys '/fromCe']};

%// create mask
ToDelete = setdiff(allblocks,ToKeep);
%// filter out main system
ToDelete = setxor(ToDelete,sys);

%// try-catch inside loop as in this example not everything is deletable
for ii = 1:numel(ToDelete)  
    try
        delete_block(ToDelete{ii})
    catch 
        disp('Some objects couldn''t be deleted')
    end
end

lh=find_system(sys, 'LookUnderMasks', 'all', 'SearchDepth', 1, 'FollowLinks', 'on','FindAll','on','type','line');
for ii = 1:numel(lh)
    try
        delete_line(lh(ii))
    catch 
        disp('Some lines couldn''t be deleted')
    end
end
%Simulink.SubSystem.deleteContents(sys)



pos_step = 200;
first = 150;

%% Original subsystem
%inputs
% posVLeadIn = [20,20,50, 30];
% posDrefIn = [20,60,50, 70];
% posToVlead = [80,10,100, 25];
% posToDref = [80,55,100, 70];
% add_block(['built-in/Inport'], [sys, '/', 'vref'], 'Position', posVLeadIn);
% add_block(['built-in/Inport'], [sys, '/', 'dref'], 'Position', posDrefIn);
% add_block('simulink/Signal Routing/Goto', [sys, '/', 'toVref'], 'Position', posToVlead);
% add_block('simulink/Signal Routing/Goto', [sys, '/', 'toDref'], 'Position', posToDref);
add_line(sys, 'vref/1','toVref/1', 'autorouting', 'on');
add_line(sys, 'dref/1','toDref/1', 'autorouting', 'on');
% set_param([sys '/toVref'], 'GoToTag', 'vlead')
% set_param([sys '/toDref'], 'GoToTag', 'dref')

%% Go through all following cars - the leader is treated separately
for i=1:N-1
    %positions
    posX = i*pos_step;
    posY = 50;
   
    %car
    carLeft = first+50+posX;
    carRight = first + 50 + posX+70;
    carTop = posY;
    carBottom = posY+100;
    carName = strcat('Car', num2str(i));
    carNameFull = strcat(sys, '/', carName);
    posCar = [carLeft,carTop,carRight, carBottom];
    car = add_block(strcat('slotcar_platooning_lib/CarDist'), carNameFull, 'Position', posCar);
    
    %car parameters - general tab
    set_param(car, 'index', num2str(i)); %order in platoon
    set_param(car, 'last', 0); %is not last (treated separately)
    set_param(car, 'leader', 0); %is not leader (treated separately)
    %set_param(car, 'dist_cont', dist_controller);
    %set_param(car, 'epsilon', 'dist_epsilon');
    set_param(car, 'init_dist', 'init_dist');
    set_param(car, 'dist_min_meas', 'dist_min_meas');
    set_param(car, 'dist_max_meas', 'dist_max_meas');
%     %car parameters - pi tab
%     set_param(car, 'dist_kp', 'dist_kp');
%     set_param(car, 'dist_ki', 'dist_ki');
%     set_param(car, 'dist_per', 'dist_per');
%     set_param(car, 'dist_cont_max', 'dist_cont_max');
%     set_param(car, 'dist_cont_min', 'dist_cont_min');
%     set_param(car, 'dist_err_ignore', 'dist_err_ignore');
%     set_param(car, 'speed_des_ignore', 'speed_des_ignore');
%     %car parameters - pd tab
%     set_param(car, 'dist_kp_pd', 'dist_kp_pd');
%     set_param(car, 'dist_kd_pd', 'dist_kd_pd');
%     set_param(car, 'dist_filt', 'dist_filt');
%     set_param(car, 'dist_per_pd', 'dist_per_pd');
%     set_param(car, 'dist_cont_max_pd', 'dist_cont_max_pd');
%     set_param(car, 'dist_cont_min_pd', 'dist_cont_min_pd');
    % car parameters - speed tab
    set_param(car, 'speed_kp', 'speed_kp');
    set_param(car, 'speed_ki', 'speed_ki');
    set_param(car, 'speed_per', 'speed_per');
    set_param(car, 'speed_deadzone', 'speed_deadzone');
    %inputs
    %x-1
    fromXName = ['from_x' num2str(i-1)];
    fromXNameFull = [sys '/' fromXName];
    posFromXm = [carLeft-50,carTop-20,carLeft-20,carTop-5];
    add_block('built-in/From', fromXNameFull , 'Position', posFromXm)
    set_param(fromXNameFull, 'GoToTag', ['x' num2str(i-1)])
    add_line(sys, [fromXName '/1'],[carName, '/1'], 'autorouting', 'on')
    
    %x+1
    fromXNameP = ['from_xp' num2str(i+1)];
    fromXNamePFull = [sys '/' fromXNameP];
    posFromXp = [carLeft-50,carTop+10,carLeft-20,carTop+25];
    add_block('built-in/From', fromXNamePFull , 'Position', posFromXp)
    set_param(fromXNamePFull, 'GoToTag', ['x' num2str(i+1)])
    add_line(sys, [fromXNameP '/1'],[carName, '/2'], 'autorouting', 'on')
    
    %dref
%     fromDref = ['from_dref' num2str(i)];
%     fromDrefFull = [sys '/' fromDref];
%     posFromDref = [carLeft-50,carTop+40,carLeft-20,carTop+55];
%     add_block('built-in/From', fromDrefFull , 'Position', posFromDref)
%     set_param(fromDrefFull, 'GoToTag', ['dref'])
%     add_line(sys, [fromDref '/1'],[carName, '/3'], 'autorouting', 'on')
    
    %vref
%     fromVref = ['from_vref' num2str(i)];
%     fromVrefFull = [sys '/' fromVref];
%     posFromVref = [carLeft-50,carTop+60,carLeft-20,carTop+75];   
%     add_block('built-in/From', fromVrefFull , 'Position', posFromVref)
%     set_param(fromVrefFull, 'GoToTag', ['vref' num2str(i)])
%     add_line(sys, [fromVref '/1'],[carName, '/4'], 'autorouting', 'on')
    
    %des vel from dist cont
    fromContEff = ['from_ceff' num2str(i)];
    fromContEffFull = [sys '/' fromContEff];
    posFromContEff = [carLeft-50,carTop+80,carLeft-20,carTop+95];   
    add_block('built-in/From', fromContEffFull , 'Position', posFromContEff)
    set_param(fromContEffFull, 'GoToTag', ['ce' num2str(i)])
    add_line(sys, [fromContEff '/1'],[carName, '/3'], 'autorouting', 'on')
    
    %outputs
    %x_i
    toXName = ['to_x' num2str(i)];
    toXNameFull = [sys '/' toXName];
    posToX = [carRight+20,carTop-20,carRight+50,carTop-5];
    add_block('built-in/Goto', toXNameFull , 'Position', posToX)
    set_param(toXNameFull, 'GoToTag', ['x' num2str(i)])
    add_line(sys,[carName, '/1'], [toXName '/1'], 'autorouting', 'on')
    
    %v_i
    toVName = ['to_v' num2str(i)];
    toVNameFull = [sys '/' toVName];
    posToV = [carRight+20,carTop+10,carRight+50,carTop+25];
    add_block('built-in/Goto', toVNameFull , 'Position', posToV)
    set_param(toVNameFull, 'GoToTag', ['v' num2str(i)])
    add_line(sys,[carName, '/2'], [toVName '/1'], 'autorouting', 'on')
    
    %dist_front
    toDName = ['to_d' num2str(i)];
    toDNameFull = [sys '/' toDName];
    posToD = [carRight+20,carTop+40,carRight+50,carTop+55];
    add_block('built-in/Goto', toDNameFull , 'Position', posToD)
    set_param(toDNameFull, 'GoToTag', ['d' num2str(i)])
    add_line(sys,[carName, '/3'], [toDName '/1'], 'autorouting', 'on')
    
    %vref_i_out
%     toRegErfName = ['to_reg' num2str(i)];
%     toRegErfFull = [sys '/' toRegErfName];
%     posToRegErf = [carRight+20,carTop+60,carRight+50,carTop+75];
%     add_block('built-in/Goto', toRegErfFull , 'Position', posToRegErf)
%     set_param(toRegErfFull, 'GoToTag', ['erf' num2str(i)])
%     add_line(sys,[carName, '/4'], [toRegErfName '/1'], 'autorouting', 'on')
    
    %reg_error rear
    toDrName = ['to_dr' num2str(i)];
    toDrFull = [sys '/' toDrName];
    posToDr = [carRight+20,carTop+80,carRight+50,carTop+95];
    add_block('built-in/Goto', toDrFull , 'Position', posToDr)
    set_param(toDrFull, 'GoToTag', ['dr' num2str(i)])
    add_line(sys,[carName, '/4'], [toDrName '/1'], 'autorouting', 'on')
    
    
    
    %controller
    contLeft = carLeft+20;
    contRight = carRight-20;
    contTop = carTop+140;
    contBottom = carBottom+100;
    posCont = [contLeft,contTop,contRight, contBottom];
    contName = ['controller' num2str(i)];
    contNameFull = [sys '/' contName];
    add_block(cont_name, contNameFull, 'Position', posCont);
    set_param(contNameFull, 'index', num2str(i))
    
    %front distance
    fromRegErfName = ['from_regf' num2str(i)];
    fromRegErfFull = [sys '/' fromRegErfName];
    posFromRegErf = [contLeft-70,contTop-5,contLeft-35,contTop+10];
    add_block('built-in/From', fromRegErfFull , 'Position', posFromRegErf)
    set_param(fromRegErfFull, 'GoToTag', ['d' num2str(i)])
    add_line(sys, [fromRegErfName '/1'],[contName, '/1'], 'autorouting', 'on')
    
    %rear distance   
    fromRegErrName = ['from_regr' num2str(i)];
    fromRegErrFull = [sys '/' fromRegErrName];
    posFromRegErr = [contLeft-70,contTop+20,contLeft-35,contTop+35];
    add_block('built-in/From', fromRegErrFull , 'Position', posFromRegErr)
    set_param(fromRegErrFull, 'GoToTag', ['dr' num2str(i)])
    add_line(sys, [fromRegErrName '/1'],[contName, '/2'], 'autorouting', 'on')
%     
%   %reference distance for the controller  
    fromDrefName = ['from_dref' num2str(i)];
    fromDrefFull = [sys '/' fromDrefName];
    posDrefErr = [contLeft-70,contTop+50,contLeft-35,contTop+65];
    add_block('built-in/From', fromDrefFull , 'Position', posDrefErr)
    set_param(fromDrefFull, 'GoToTag', 'dref')
    add_line(sys, [fromDrefName '/1'],[contName, '/3'], 'autorouting', 'on')
    
    %velocity of previous vehicle
    fromVprevName = ['from_vPrev' num2str(i)];
    fromVprevFull = [sys '/' fromVprevName];
    posVprev = [contLeft-70,contTop+80,contLeft-35,contTop+95];
    add_block('built-in/From', fromVprevFull , 'Position', posVprev)
    set_param(fromVprevFull, 'GoToTag', ['v' num2str(i-1)])
    add_line(sys, [fromVprevName '/1'],[contName, '/4'], 'autorouting', 'on')
    
    % position of all vehicles
    fromXName = ['in_x' num2str(i)];
    fromXFull = [sys '/' fromXName];
    posX = [contLeft-70,contTop+110,contLeft-35,contTop+125];
    add_block('built-in/From', fromXFull , 'Position', posX)
    set_param(fromXFull, 'GoToTag', 'x')
    add_line(sys, [fromXName '/1'],[contName, '/5'], 'autorouting', 'on')
    
     % velocity of all vehicles
    fromVName = ['in_v' num2str(i)];
    fromVFull = [sys '/' fromVName];
    posV = [contLeft-70,contTop+130,contLeft-35,contTop+155];
    add_block('built-in/From', fromVFull , 'Position', posV)
    set_param(fromVFull, 'GoToTag', 'v')
    add_line(sys, [fromVName '/1'],[contName, '/6'], 'autorouting', 'on')
    
    % velocity of all vehicles
    fromCEName = ['in_ce' num2str(i)];
    fromCEFull = [sys '/' fromCEName];
    posCE = [contLeft-70,contTop+150,contLeft-35,contTop+185];
    add_block('built-in/From', fromCEFull , 'Position', posCE)
    set_param(fromCEFull, 'GoToTag', 'ce')
    add_line(sys, [fromCEName '/1'],[contName, '/7'], 'autorouting', 'on')
    
    %control effort from the controller
    toContEff = ['to_ceff' num2str(i)];
    toContEffFull = [sys '/' toContEff];
    posToContEff = [contRight+25,contTop+20,contRight+60,contTop+35];   
    add_block('built-in/Goto', toContEffFull , 'Position', posToContEff)
    set_param(toContEffFull, 'GoToTag', ['ce' num2str(i)])
    add_line(sys,[contName, '/1'], [toContEff '/1'], 'autorouting', 'on')
    
end;



%% Specify the platoon leader - the car with index 0
carName = strcat('CarLeader');
carNameFull = strcat(sys, '/', carName);
carLeft = 170;
carRight = 170+70;
carTop = posY;
carBottom = posY+70;
posCar = [carLeft,carTop,carRight,carBottom];
car = add_block(strcat('slotcar_platooning_lib/CarDist'), carNameFull, 'Position', posCar);
i = 0;
%INPUT
add_line(sys,'vref/1',[carName '/3'], 'autorouting', 'on')
%OUTPUTS
    %x_i
    toXName = ['to_x' num2str(i)];
    toXNameFull = [sys '/' toXName];
    posToX = [carRight+20,carTop-20,carRight+50,carTop-5];
    add_block('built-in/Goto', toXNameFull , 'Position', posToX)
    set_param(toXNameFull, 'GoToTag', ['x' num2str(i)])
    add_line(sys,[carName, '/1'], [toXName '/1'], 'autorouting', 'on')
    
    %v_i
    toVName = ['to_v' num2str(i)];
    toVNameFull = [sys '/' toVName];
    posToV = [carRight+20,carTop+10,carRight+50,carTop+25];
    add_block('built-in/Goto', toVNameFull , 'Position', posToV)
    set_param(toVNameFull, 'GoToTag', ['v' num2str(i)])
    add_line(sys,[carName, '/2'], [toVName '/1'], 'autorouting', 'on')
    
%     %dist_i
%     toDName = ['to_d' num2str(i)];
%     toDNameFull = [sys '/' toDName];
%     posToD = [carRight+20,carTop+40,carRight+50,carTop+55];
%     add_block('built-in/Goto', toDNameFull , 'Position', posToD)
%     set_param(toDNameFull, 'GoToTag', ['d' num2str(i)])
%     add_line(sys,[carName, '/3'], [toDName '/1'], 'autorouting', 'on')
%     
%     %vref_i_out
%     toVdesName = ['to_vr' num2str(i)];
%     toVdesNameFull = [sys '/' toVdesName];
%     posToVdes = [carRight+20,carTop+60,carRight+50,carTop+75];
%     add_block('built-in/Goto', toVdesNameFull , 'Position', posToVdes)
%     set_param(toVdesNameFull, 'GoToTag', ['vdes' num2str(i)])
%     add_line(sys,[carName, '/4'], [toVdesName '/1'], 'autorouting', 'on')
    

    
    %car parameters - general tab
    set_param(car, 'index', '0')
    set_param(car, 'last', 0); %is not last (treated separately)
    set_param(car, 'leader', 1); %is leader (treated separately)
    %set_param(car, 'dist_cont', dist_controller);
    %set_param(car, 'epsilon', 'dist_epsilon');
    set_param(car, 'init_dist', 'init_dist');
    set_param(car, 'dist_min_meas', 'dist_min_meas');
    set_param(car, 'dist_max_meas', 'dist_max_meas');
%     %car parameters - pi tab
%     set_param(car, 'dist_kp', 'dist_kp');
%     set_param(car, 'dist_ki', 'dist_ki');
%     set_param(car, 'dist_per', 'dist_per');
%     set_param(car, 'dist_cont_max', 'dist_cont_max');
%     set_param(car, 'dist_cont_min', 'dist_cont_min');
%     set_param(car, 'dist_err_ignore', 'dist_err_ignore');
%     set_param(car, 'speed_des_ignore', 'speed_des_ignore');
%     %car parameters - pd tab
%     set_param(car, 'dist_kp_pd', 'dist_kp_pd');
%     set_param(car, 'dist_kd_pd', 'dist_kd_pd');
%     set_param(car, 'dist_filt', 'dist_filt');
%     set_param(car, 'dist_per_pd', 'dist_per_pd');
%     set_param(car, 'dist_cont_max_pd', 'dist_cont_max_pd');
%     set_param(car, 'dist_cont_min_pd', 'dist_cont_min_pd');
    % car parameters - speed tab
    set_param(car, 'speed_kp', 'speed_kp');
    set_param(car, 'speed_ki', 'speed_ki');
    set_param(car, 'speed_per', 'speed_per'); 
    set_param(car, 'speed_deadzone', 'speed_deadzone');
%% correction for last car
    lastCarIndex = N-1;
    car = getSimulinkBlockHandle([sys '/Car' num2str(lastCarIndex)]);
    set_param(car, 'last', 1); %is not last (treated separately)
    fromXNamePFull = [sys '/from_xp' num2str(lastCarIndex+1)];
    set_param(fromXNamePFull, 'GoToTag', ['x' num2str(lastCarIndex)])
    rearDistLast = getSimulinkBlockHandle([sys '/from_regr' num2str(lastCarIndex)]);
    set_param(rearDistLast, 'GoToTag', 'dref') %sees the desired distance = zero rear error -> acts as if there is no other car
    
    
%% Subsystem Outputs
%positions
posMuxPos = [100,300,110,300+N*30];
muxPosName = 'MuxPos';
muxPosFull = [sys '/' muxPosName];
mux_pos = add_block('built-in/Mux', muxPosFull , 'Position', posMuxPos);
set_param(mux_pos, 'Inputs', num2str(N));

posOutPos = [130,320,150, 340];
outPosName = 'OutPos';
outPosFull = [sys '/' outPosName];
out_pos = add_block('built-in/Goto', outPosFull , 'Position', posOutPos);
set_param(out_pos, 'GoToTag', 'x');
add_line(sys,[muxPosName, '/1'], [outPosName '/1'], 'autorouting', 'on');

%velocity
posMuxVel = [200,300,210,300+N*30];
muxVelName = 'MuxVel';
muxVelFull = [sys '/' muxVelName];
mux_vel = add_block('built-in/Mux', muxVelFull , 'Position', posMuxVel);
set_param(mux_vel, 'Inputs', num2str(N));

posOutVel = [230,320,250,340];
outVelName = 'OutVel';
outVelFull = [sys '/' outVelName];
out_vel = add_block('built-in/Goto', outVelFull , 'Position', posOutVel);
set_param(out_vel, 'GoToTag', 'v');
add_line(sys,[muxVelName, '/1'], [outVelName '/1'], 'autorouting', 'on');

%distance
posMuxDist = [300,300,310,300+N*30];
muxDistName = 'MuxDist';
muxDistFull = [sys '/' muxDistName];
mux_dist = add_block('built-in/Mux', muxDistFull , 'Position', posMuxDist);
set_param(mux_dist, 'Inputs', num2str(N-1));

posOutDist = [330,320,350,340];
outDistName = 'OutDist';
outDistFull = [sys '/' outDistName];
out_dist = add_block('built-in/Goto', outDistFull , 'Position', posOutDist);
set_param(out_dist, 'GoToTag', 'd');
add_line(sys,[muxDistName, '/1'], [outDistName '/1'], 'autorouting', 'on');

%desired velocity
posMuxVdes = [400,300,410,300+N*30];
muxVdesName = 'MuxVdes';
muxVdesFull = [sys '/' muxVdesName];
mux_vdes = add_block('built-in/Mux', muxVdesFull , 'Position', posMuxVdes);
set_param(mux_vdes, 'Inputs', num2str(N));

posOutVdes = [430,320,450,340];
outVdesName = 'OutVdes';
outVdesFull = [sys '/' outVdesName];
out_vdes = add_block('built-in/Goto', outVdesFull , 'Position', posOutVdes);
set_param(out_vdes, 'GoToTag', 'ce');
add_line(sys,[muxVdesName, '/1'], [outVdesName '/1'], 'autorouting', 'on');

for i=0:N-1
    %positions
    fromXName = ['from_ox' num2str(i)];
    fromXFull = [sys '/' fromXName];
    posXOut = [60, 300+i*30, 80, 320+i*30];   
    add_block('built-in/From', fromXFull , 'Position', posXOut);
    set_param(fromXFull, 'GoToTag', ['x' num2str(i)]);
    add_line(sys, [fromXName '/1'],[muxPosName, '/' num2str(i+1)], 'autorouting', 'on');
    
    %velocities
    fromVName = ['from_ov' num2str(i)];
    fromVFull = [sys '/' fromVName];
    posVOut = [160, 300+i*30, 180, 320+i*30];   
    add_block('built-in/From', fromVFull , 'Position', posVOut);
    set_param(fromVFull, 'GoToTag', ['v' num2str(i)]);
    add_line(sys, [fromVName '/1'],[muxVelName, '/' num2str(i+1)], 'autorouting', 'on');
    
    %desired velocities
    fromVdesName = ['from_ovdes' num2str(i)];
    fromVdesFull = [sys '/' fromVdesName];
    posVdesOut = [360, 300+i*30, 380, 320+i*30];   
    add_block('built-in/From', fromVdesFull , 'Position', posVdesOut) ;
    set_param(fromVdesFull, 'GoToTag', ['ce' num2str(i)]);
    add_line(sys, [fromVdesName '/1'],[muxVdesName, '/' num2str(i+1)], 'autorouting', 'on');
end;
for i=1:N-1
    %distances
    fromDName = ['from_od' num2str(i)];
    fromDFull = [sys '/' fromDName];
    posDOut = [260, 300+i*30, 280, 320+i*30];   
    add_block('built-in/From', fromDFull , 'Position', posDOut);
    set_param(fromDFull, 'GoToTag', ['d' num2str(i)]);
    add_line(sys, [fromDName '/1'],[muxDistName, '/' num2str(i)], 'autorouting', 'on');
end;

add_line(sys, 'fromX/1', 'x/1', 'autorouting', 'on')
add_line(sys, 'fromV/1', 'v/1', 'autorouting', 'on')
add_line(sys, 'fromD/1', 'd/1', 'autorouting', 'on')
add_line(sys, 'fromCe/1', 'v_des/1', 'autorouting', 'on')

disp('Platoon generation was successfull.')



end

