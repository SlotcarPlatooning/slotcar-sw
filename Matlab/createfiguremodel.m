function figure1 = createfiguremodel(velocity, current, bemf, t_lim, v_lim, c_lim, b_lim)
figure1 = figure('InvertHardcopy','off','PaperUnits','centimeters',...
    'PaperSize',[15 15],...
    'Color',[1 1 1],...
    'Renderer','painters');

% Create subplot
subplot1 = subplot(3,1,1,'Parent',figure1);
hold(subplot1,'on');
plot1 = creategraph(velocity.Time, squeeze(velocity.Data), t_lim, v_lim, 'Time (s)', 'Velocity (m/s)');
set(plot1(2),'LineStyle','--','LineWidth',2);
set(plot1(1),'LineWidth',2);
legend({'real', 'sim'},'Interpreter','latex')
box(subplot1,'on');
grid(subplot1,'on');

%%
subplot2 = subplot(3,1,2,'Parent',figure1);
hold(subplot2,'on');
plot2 = creategraph(current.Time, squeeze(current.Data), t_lim, c_lim, 'Time (s)', 'El. current(A)');
set(plot2(2),'LineStyle','--','LineWidth',2);
set(plot2(1),'LineWidth',2);

box(subplot2,'on');
grid(subplot2,'on');

%%

subplot3 = subplot(3,1,3,'Parent',figure1);
hold(subplot3,'on');
plot3 = creategraph(bemf.Time, squeeze(bemf.Data), t_lim, b_lim, 'Time (s)', 'Back EMF(V)');
set(plot3(2),'LineStyle','--','LineWidth',2);
set(plot3(1),'LineWidth',2);
box(subplot3,'on');
grid(subplot3,'on');

