function p = creategraph(x,y,x_lim,y_lim,x_label,y_label)
% Create multiple lines using matrix input to plot
p = plot(x,y, 'LineWidth', 1.4);
% set(plot2(1),...
%     'Color',[0.850980401039124 0.325490206480026 0.0980392172932625]);
% set(plot2(2),...
%     'Color',[0.929411768913269 0.694117665290833 0.125490203499794]);
% set(plot2(3),...
%     'Color',[0.494117647409439 0.184313729405403 0.556862771511078]);

% Create xlabel
xlabel(x_label,'HorizontalAlignment','center',...
    'Interpreter','latex');
xlim(x_lim)
% Create ylabel
ylab = ylabel(y_label,'Interpreter','latex');

 
%  set(ylab,'VerticalAlignment','middle')
 
 
ylim(y_lim)
grid on;
end