package slotcar.clock;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;

public final class CarClock {
	
	private static ProcessBuilder procBuilder = new ProcessBuilder("chronyc", "tracking");
	private static Process proc = null;
	
	public static int getTimestampMilli(){
		LocalDateTime startOfMonth = LocalDate.now().withDayOfMonth(1).atStartOfDay();
	    Duration d = Duration.between(startOfMonth, LocalDateTime.now());

	    return (int) d.toMillis();
	}
	
	public static LocalDateTime getDateFromTimestampMilli(int milli) {
		return LocalDate.now().withDayOfMonth(1).atStartOfDay().plus(Duration.ofMillis(milli));
	}
	
	public static int querryState() throws IOException {
		if (proc != null) proc.destroy();
			proc = procBuilder.start();
		try {
			proc.waitFor();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("querried, returned "+ proc.exitValue());
		return proc.exitValue();
	}
	
	// must be called after querryState()
	public static byte[] logState() {
		if (proc == null) return null;
		java.io.InputStream is = proc.getInputStream();
		BufferedInputStream bis = new BufferedInputStream(is);
		ByteArrayOutputStream buf = new ByteArrayOutputStream();
		int result;
		try {
			result = bis.read();
			while(result != -1) {
			    buf.write((byte) result);
			    result = bis.read();
			}
			buf.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		System.out.println(buf.toString());
		return buf.toByteArray();
	}
	

}
