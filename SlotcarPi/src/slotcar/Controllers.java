package slotcar;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import slotcar.pi.controller.Controller;
import slotcar.pi.controller.ControllerNone;
import slotcar.pi.controller.ControllerPwm;
import slotcar.pi.controller.ControllerSpeed;
import slotcar.pi.controller.SimulinkControllerMISO;
import slotcar.pi.controller.SimulinkControllerSISO;
import slotcar.pi.controller.SimulinkControllerTISO;
import slotcar.pi.controller.UnsupportedControllerException;

/**
 * Contains available controllers. Loads new controllers.
 */
public class Controllers {
	@SuppressWarnings("unused")
	private static final long serialVersionUID = -4795763249206092515L;
	private final Logger log = Logger.getGlobal();

	
	private Map<String, Controller> map = new HashMap<>();
	private final Controller defaultController = new ControllerNone();

	private static final String CONF_TAG_CTRL_FILES = "CONTROLLERS";

	public Controllers() {
		map.put(defaultController.getName(), defaultController);
		// Inserting PWM controller
		ControllerPwm cPwm = new ControllerPwm();
		map.put(cPwm.getName(), cPwm);
		// Inserting SPEED controller
		ControllerSpeed cSpeed = new ControllerSpeed();
		map.put(cSpeed.getName(), cSpeed);
		// Inserting SISO controller generated from Simulink
		SimulinkControllerSISO cSISO = new SimulinkControllerSISO();
		map.put(cSISO.getName(), cSISO);
		// Inserting Two input single output controller generated from Simulink
		SimulinkControllerTISO cTISO = new SimulinkControllerTISO();
		map.put(cTISO.getName(), cTISO);
		// Inserting Multiple input single output controller generated from Simulink
		SimulinkControllerMISO cMISO = new SimulinkControllerMISO();
		map.put(cMISO.getName(), cMISO);
	}

	/**
	 * @see Controller
	 * @return controller with label if it exits.
	 * OR
	 * @return default controller.
	 */
	public Controller get(String label) {
		return map.getOrDefault(label, defaultController);
	}

	/**
	 * @return list of loaded controllers.
	 */
	public Collection<Controller> getValues() {
		return map.values();
	}

	/**
	 * Recursive search in directories for files containing controllers.
	 * @param depth Limit to recursion
	 * @param files list of files/directories in which we search.
	 */
	public void load(int depth, File... files) {
		if (files == null){
			return;
		}
		
		for (File file : files) {
			if (file == null) {
				continue;
			}
			if (depth > 0 && file.isDirectory()) {
				log.info("Looking for controllers in directory:" + file.getAbsolutePath());
				load(depth - 1, file.listFiles());
			} else {
				log.info("Looking for controllers in file:" + file.getAbsolutePath());
				loadControllerClass(file);
			}
		}
	}

	public void clearSavedPaths() {
		Config.getInstance().set(CONF_TAG_CTRL_FILES, "");
	}

	/**
	 * Tries to load a controller from a file. 
	 * @return true if success.
	 * @return false if failed.
	 * @param file File with controller.
	 */
	private boolean loadControllerClass(File file) {
		final ClassLoader parentLoader = Controller.class.getClassLoader();

		if(file == null){
//			log.severe("File is null.");
			return false;
		}
		
		if (!file.exists()) {
//			log.severe("File " + file.getAbsolutePath() + " does not exist.");
			Config.getInstance().remove(CONF_TAG_CTRL_FILES, file.getAbsolutePath());
			return false;
		}

		JarFile jar;
		try {
			jar = new JarFile(file);
		} catch (IOException e) {			
			Config.getInstance().remove(CONF_TAG_CTRL_FILES, file.getAbsolutePath());
//			log.severe("File " + file.getAbsolutePath() + " is not jar.");
			return false;
		}
		
//		log.info("Found jar " + jar.getName());

		try {
			Enumeration<JarEntry> e = jar.entries();
			URLClassLoader loader = new URLClassLoader(new URL[] { file.toURI().toURL() }, parentLoader);

			while (e.hasMoreElements()) {
				JarEntry je = e.nextElement();
				if (je.isDirectory() || !je.getName().endsWith(".class")) {
					continue;
				}

				// -6 because of .class
				String className = je.getName().substring(0, je.getName().length() - 6).replace('/', '.');
				Class<?> c = loader.loadClass(className);

				Controller x = null;

				try {
					// just test if we can make an instance
					// TODO test it in a smarter way
					x = (Controller) c.newInstance();
				} catch (ClassCastException | InstantiationException | IllegalAccessException e1) {
					Logger.getGlobal().finest(c.getName() + " is not a child of Controller");
					continue;
				}

				String label = x.getName();
				if (map.containsKey(label)) {
					String s = String.format("Controller %s is already present. Replacing it!", label);
					Logger.getGlobal().info(s);
				}
				map.put(label, x);

			}
			loader.close();

			Config.getInstance().add(CONF_TAG_CTRL_FILES, file.getAbsolutePath());

			return true;

		} catch (IOException | ClassNotFoundException e) {
			Logger.getGlobal().log(Level.SEVERE, null, e);
		} finally {
			try {
				jar.close();
			} catch (IOException e) {
				Logger.getGlobal().log(Level.SEVERE, null, e);
			}
		}
		return false;
	}

	/**
	 * Stores controller constants to a XML file.
	 */
	public void storeControllerConstants(Controller c) {
		try {
			// check if directory exists or create it
			File dir = Config.getControllersPath();

			String path = String.format("%s/%s.xml", dir.getAbsolutePath(), c.getName());
			File f = new File(path);
			if (!f.exists()) {
				f.createNewFile();
			}
			
			FileOutputStream out = new FileOutputStream(f);
			c.writeTo(out);
		} catch (IOException e) {
			log.log(Level.SEVERE, "Storring controller's constants error", e);
		}
	}

	public Controller readControllerConstantsFrom(InputStream in) throws UnsupportedControllerException {
		Controller c = null;
		try {
			Document document = new SAXBuilder().build(in);
			String name = document.getRootElement().getAttributeValue("name");

			c = get(name);

			if (c != null) {
				c.readFrom(document.getRootElement());
				log.fine(c.toString());
			} else {
				String s = "Controler '" + name
						+ "' is not on list of loaded controllers. Perhaps you did not upload the correct .jar file to this car.";
				log.severe(s);
				throw new UnsupportedControllerException(s, name);
			}
		} catch (JDOMException | IOException e) {
			log.log(Level.SEVERE, "Reading controllers from file error.", e);
		}
		return c;
	}

	/**
	 * Loads the constants of the controllers from the stored xml files.
	 * 
	 * @param dir Directory in which we look for controllers' parameters.
	 */
	public void loadConstants(File dir) {
		log.info("Loading constants in " + dir.getAbsolutePath());
		if (dir != null && dir.isDirectory()) {
			File[] fileList = dir.listFiles();
			for (File file : fileList) {
				log.info("Loading constants from " + file.getAbsolutePath());
				try {
					readControllerConstantsFrom(new FileInputStream(file));
				} catch (FileNotFoundException | UnsupportedControllerException e) {
					Logger.getGlobal().log(Level.SEVERE, null, e);
				}
			}
		}
	}

	public int size() {
		return map.size();
	}

	public Controller getDefault() {
		return defaultController;
	}
}
