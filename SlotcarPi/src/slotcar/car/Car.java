package slotcar.car;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

import slotcar.CarList;
import slotcar.Config;
import slotcar.SlotcarState;
import slotcar.network.CarPacket;
import slotcar.network.ComManager;
import slotcar.network.Communicator;
import slotcar.pi.controller.CarImageInterface;
import slotcar.network.NetworkFaultParameters;
import slotcar.pi.controller.OutputType;

/**
 * Represents general car implementation, which is then meant to be specified by
 * inheritance.
 */
public abstract class Car extends CarImage implements Runnable, NetworkEventListenerCar {

	private static final Logger log = Logger.getLogger(Car.class.getName());

	private static final CarList cars = CarList.getInstance();

	protected Communicator com;
	private StateLogger stateLogger;
	private Experiment experiment;
	private Positioning positioning;
	private Calibration calibration;
	private NetworkProbe netProbe;

	private CarState currentState;
	private Map<SlotcarState,CarState> states = new HashMap<>();
	private final ExecutorService stateExecutor = Executors.newSingleThreadExecutor();

	/**
	 * When doing the communication test, this variable stores the number of
	 * successfully received packets
	 */

	protected int numberOfReceivedTestCommPackets = 0;

	public void init(ComManager comMan) {
		log.info("Initializing car " + getLogAddr());
		cars.addCar(this);

		log.info("Initializing network");
		// waiting for network
		boolean connected = false;

		while(true){
			log.warning("Connecting...");
			connected = comMan.open();
			if(connected){
				break;
			}
			try {
				log.warning("Connecting failed...waiting.");
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// do nothing
			}
		}

		log.warning("Connected!");


		this.com = new CommunicatorCar(this, comMan, getLogAddr());


		stateLogger = new StateLogger(this);
		experiment = new Experiment(this);
		netProbe = new NetworkProbe(this);
		positioning = new Positioning(this) {
			@Override
			public void sendEcho() {
				com.sendSetPosition();
			}
		};
		setState(SlotcarState.READY);

		states.put(SlotcarState.POSITIONING, new CarState(SlotcarState.POSITIONING) {

			@Override
			public void run() {

			}

			@Override
			public void init() {
				positioning.start();
			}

			@Override
			public void end() {
				positioning.stop();
			}
		});

		states.put(SlotcarState.EXPERIMENT, new CarState(SlotcarState.EXPERIMENT) {

			@Override
			public void run() {

			}

			@Override
			public void init() {
				experiment.start();
			}

			@Override
			public void end() {
				experiment.stop();
			}
		});

		states.put(SlotcarState.READY, new CarState(SlotcarState.READY) {

			@Override
			public void run() {

			}

			@Override
			public void init() {

			}

			@Override
			public void end() {

			}
		});

		states.put(SlotcarState.MANUAL, new CarState(SlotcarState.MANUAL) {

			@Override
			public void run() {

			}

			@Override
			public void init() {
				getCarControl().setOutputType(OutputType.SPEED);

			}

			@Override
			public void end() {
				getCarControl().setOutputType(OutputType.STOP);

			}
		});

		setState(SlotcarState.READY);
		currentState = states.get(SlotcarState.READY);

		stateExecutor.submit(this);
		com.connectToServer();
	}

	@Override
	public void run() {
		while(true){
			if(getState() != currentState.getSlotcarState()){
				log.info("State changed from " + currentState.getSlotcarState() + " to " + getState());
				currentState.end();
				currentState = states.get(getState());
				currentState.init();
			}
			currentState.run();
		}
	}

	public Car(CarMeasurement measurement, CarControl control, NetworkFaultParameters netFaultParams) {
		super(Config.getAddr(), measurement, control, netFaultParams);
	}

	public void setCalibration(Calibration calibration) {
		this.calibration = calibration;
	}

	@Override
	public void setState(SlotcarState state) {
		if (state != getState()) {
			super.setState(state);
			// state changed, send it to GUI
			com.sendStatus(Config.ADDR_SERVER, getState(), hashCode(), getTimeOfChange());
		}
	}

	@Override
	public void handleCarLoggingAck(byte sourceAddr) {
		log.info("Connected to network " + sourceAddr);
	}

	@Override
	public void handleSyncCarList(byte[] data) {
		cars.syncPositions(data);
	}

	@Override
	public void handleUploadSettings(byte sourceAddr, byte[] data) {
		try {
			ObjectInputStream s = new ObjectInputStream(new ByteArrayInputStream(data));
			getCarControl().read(s, true);
			log.info("New Settings\n" + this);
			s.close();
		} catch (IOException e1) {
			Logger.getGlobal().log(Level.SEVERE, null, e1);
		}
	}

	@Override
	public void handleImageRequest(byte sourceAddr) {
		com.sendImage(this, sourceAddr, true);
	}

	@Override
	public void handleReconnectRequest() {
		log.info("Reconnect request. Reconnecting."); // it is done on lower
														// layers.
		experiment.stop();
	}

	@Override
	public void handleImage(byte sourceAddr, byte[] data) {

		experiment.communicationstep(sourceAddr);

		CarImage car = cars.get(sourceAddr);

		if (car == null) {
			log.warning(String.format("Handling image from car %d. Car is not on the list", sourceAddr));
			return;
		} else {
			try {
				car.read(new ObjectInputStream(new ByteArrayInputStream(data)), true);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void handleCommunicationTestStart() {
		log.info("Starting a communication test.");
		this.numberOfReceivedTestCommPackets = 0;

	}

	@Override
	public void handleCommunicationTestPacket() {
		log.info("A packet in communication test received");
		this.numberOfReceivedTestCommPackets++;
	}

	@Override
	public void handleCommunicationTestEnd() {
		log.info("Ending the communication test with the number of received packets: "
				+ this.numberOfReceivedTestCommPackets);
		com.sendCommTestResult(this.numberOfReceivedTestCommPackets);
	}

	@Override
	public void handleUnknownService(CarPacket packet) {
		log.info("Received service packet is not implemented. Service number " + packet.getServiceByte());
	}

	@Override
	public void handleControlStart() {
		log.fine("Control start received.");
		getCarControl().setControlled(true);
	}

	@Override
	public void handleControlStop() {
		log.fine("Control stop received.");
		getCarControl().setControlled(false);
	}

	public void shareImage(byte recieverLogAddr) {
		com.sendImage(this, recieverLogAddr);
	}

	public void shareImage() {
		com.sendImage(this);
	}

	@Override
	public void handleCalibrationA(float refDistance) {
		if (refDistance > 0) {
			log.info("Callibrating");
			calibration.calibrate(refDistance);
		} else {
			log.warning("Reference distance is not greater than zero.");
		}
	}

	@Override
	public void handleCalibrationB() {
		// TODO check communication is on and that the data are valid
		calibration.calibrate();
	}

	@Override
	public void handleLoggingStart(byte[] data) {
		log.fine("State logging start received.");
		ByteBuffer bf = ByteBuffer.wrap(data);
		stateLogger.start(bf.getInt(), bf.getInt() > 0);
	}

	@Override
	public void handleStartCommunication() {
		log.fine("Communication start received.");
		experiment.start();
	}

	@Override
	public void handleManualSpeed(float speed){
		if(getState() == SlotcarState.MANUAL)
			getCarControl().setReferenceSpeed(speed);
	}

	@Override
	public void handleStopCommunication() {
		log.fine("Communication stop received.");
		experiment.stop();
	}

	@Override
	public void handleLoggingStop() {
		log.fine("Logging stop received.");
		stateLogger.stop();
	}

	@Override
	public void handleStatusRequest(byte sourceAddr) {
		log.fine("Status request received.");
		com.sendStatus(sourceAddr, getState(), getCarControl().hashCode(), getTimeOfChange());
	}

	public CarList getCars() {
		return cars;
	}

	public String printPosition() {
		return String.format("%d/%d", getPosition(), getCars().size());
	}

	public void sendErrorMessage(String msg) {
		com.sendErrorMessage(msg, Config.ADDR_SERVER);
	}

	@Override
	public void handleStateChange(SlotcarState state) {
		this.setState(state);
	}

	public CarImage getSuccessor(){
		return (CarImage)cars.getSuccessor(this);
	}

	public CarImage getPredecessor(){
		return (CarImage)cars.getPredecessor(this);
	}

	public boolean isOdd(){
		if( (getPosition() % 2) == 0 ){
			return false;
		} else {
			return true;
		}
	}

	@Override
	public void handleStartProbe(int ps, int ipt) {
		netProbe.stop();
		netProbe.setPacketSize(ps);
		netProbe.setInterPacketTime(ipt);
		netProbe.start();

	}

	@Override
	public void handleStopProbe() {
		netProbe.stop();

	}

	@Override
	public void handleProbe(byte[] bogus) {
		return;

	}
}
