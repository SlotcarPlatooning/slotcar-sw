package slotcar.car;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;

import slotcar.Config;
import slotcar.Rolling;
import slotcar.pi.controller.OutputType;
import slotcar.pi.stm.StmMemory;

/**
 * <h1>Positioning procedure</h1>
 * <p>
 * Cars in the platoon determine their position based on information from
 * distance measurement. A car who sees no one in front of him sends an echo to
 * the GUI and switch of his back distance sensor. Then waits for the procedure
 * to be finished (by checking state) for the rest of the platoon.
 */
public abstract class Positioning implements Runnable {

	static final double DISTANCE_LIMIT = 0.3;
	static final int CHECKS_NUM = 10;
	private static final long PHASE2_WAITING_TIME = 500;

	private Thread t;
	private boolean someoneInFront;
	private boolean someoneBehind;
	private CarImage car;

	public Positioning(CarImage car) {
		this.car = car;
	}

	private void checkNeighbours() {
		Rolling frontAvg = new Rolling(CHECKS_NUM);
		Rolling backAvg = new Rolling(CHECKS_NUM);
	
		for (int i = 0; i < CHECKS_NUM; i++) {
			
			float front = car.getCarMeasurement().getDistanceFront();
			float back = car.getCarMeasurement().getDistanceBack();
		
			frontAvg.add(front);
			backAvg.add(back);
			
			try {
				Thread.sleep(Config.SPI_MEMORY_PERIOD_UNIT.toMillis(Config.SPI_MEMORY_PERIOD));
			} catch (InterruptedException e) {
				Logger.getGlobal().log(Level.SEVERE, null, e);
			}
		}
		
		someoneInFront = (frontAvg.getAverage() < DISTANCE_LIMIT) ? true : false;
		someoneBehind = (backAvg.getAverage() < DISTANCE_LIMIT) ? true : false;
		
		Logger.getGlobal().info("Neighbours front=" + frontAvg.getAverage() + "m , back=" + backAvg.getAverage() + "m");
	}

	private AtomicBoolean stopped = new AtomicBoolean(true);

	@Override
	public void run() {

		Logger.getGlobal().info("Positioning Started");

		// stop the car
		car.getCarControl().setControlled(false);
		car.getCarControl().setOutputType(OutputType.STOP);

		// make sure that transmitting is enabled
		StmMemory.getInstance().distBackTransmitEn.set(1);

		// First, we check if there is someone behind and in front of us
		checkNeighbours();
		String neigh = String.format("Neighbours (front=%b, back=%b)",someoneInFront, someoneBehind);
		Logger.getGlobal().fine(neigh);

		// Second, we wait until there will be free before us
		if(someoneInFront){
			Logger.getGlobal().info("Phase 1, waiting for car ahead");	
		}
	
		while (someoneInFront && !stopped.get()) {
			checkNeighbours();
		}

		if(!stopped.get()){
			sendEcho(); // inform the server
		}
		// if the road is free we hide ourselves from others
		Logger.getGlobal().info("Positioning phase 2, switching sensor off!");

		// now we hide ourself from other cars, by disabling back
		// trasmitting and we will wait for some amount of time
		
		try {
			Thread.sleep(PHASE2_WAITING_TIME);
		} catch (InterruptedException e) {

			Logger.getGlobal().log(Level.SEVERE, null, e);
		}

		// set transmitting off
		StmMemory.getInstance().distBackTransmitEn.set(0);
		try {
			Thread.sleep(2*PHASE2_WAITING_TIME);
		} catch (InterruptedException e) {

			Logger.getGlobal().log(Level.SEVERE, null, e);
		}
		
		// set trasmitting on
		StmMemory.getInstance().distBackTransmitEn.set(1);
		Logger.getGlobal().info("Positioning is done");
	}

	public abstract void sendEcho();

	public void start() {
		stopped.set(false);
		if (t == null || !t.isAlive()) {
			t = new Thread(this);
			t.start();
		}
	}

	public void stop() {
		stopped.set(true);
	}
}
