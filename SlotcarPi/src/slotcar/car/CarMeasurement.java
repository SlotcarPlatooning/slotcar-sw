package slotcar.car;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.time.ZonedDateTime;

import slotcar.pi.controller.CarMeasurementInterface;

public class CarMeasurement implements CarMeasurementInterface {
	
	static final long serialVersionUID = 42L;

	// state variables
	private float speed;
	private float distanceFront;
	private float distanceBack;
	private float appliedPwm;
	private float desiredI;
	private float desiredSpeed;
	
	// accelerometer
	private float accX;
	private float accY;
	private float accZ;
	// gyroscope
	private float pitchRate;
	private float rollRate;
	private float yawRate;
	// others
	private float speedKalman;
	private float frictionKalman;
	private float speedIrc;
	private float speedBemf;
	private float motorI;
	private float motorBemf;
	private float railV;
	private float supercapV;
	
	public CarMeasurement() {

	}

	/* Speed */
	@Override
	public float getSpeed() {
		return speed;
	}
	
	public void setSpeed(float speed) {
		this.speed = speed;
	}
	
	/* Distance front */
	@Override
	public float getDistanceFront() {
		return distanceFront;
	}
	
	public void setDistanceFront(float distance) {
		this.distanceFront = distance;
	}

	/* Distance back */
	@Override
	public float getDistanceBack() {
		return distanceBack;
	}

	public void setDistanceBack(float distance) {
		this.distanceBack = distance;
	}
	
	/* Acceleration X */
	@Override
	public float getAccX() {
		return accX;
	}

	public void setAccX(float acceleration) {
		this.accX = acceleration;
	}

	/* Acceleration Y */
	@Override
	public float getAccY() {
		return accY;
	}
	
	public void setAccY(float acceleration) {
		this.accY = acceleration;
	}

	/* Acceleration Z */
	@Override
	public float getAccZ() {
		return accZ;
	}
	
	public void setAccZ(float acceleration) {
		this.accZ = acceleration;
	}

	/* Pitch rate */
	@Override
	public float getPitchRate() {
		return pitchRate;
	}

	public void setPitchRate(float rate) {
		this.pitchRate = rate;
	}
	
	/* Roll rate */
	@Override
	public float getRollRate() {
		return rollRate;
	}

	public void setRollRate(float rate) {
		this.rollRate = rate;
	}
	
	/* Yaw rate */
	@Override
	public float getYawRate() {
		return yawRate;
	}

	public void setYawRate(float rate) {
		this.yawRate = rate;
	}
	
	/* Applied PWM */
	@Override
	public float getAppliedPwm() {
		return appliedPwm;
	}

	public void setAppliedPwm(float pwm) {
		this.appliedPwm = pwm;
	}
	
	/* Speed estimated by kalman filter */
	public float getSpeedKalman() {
		return speedKalman;
	}
	
	public void setSpeedKalman(float speed) {
		speedKalman = speed;
	}

	/* Friction estimated by kalman filter */
	public void setFriction(float friction) {
		frictionKalman = friction;
	}
	
	public float getFriction() {
		return frictionKalman;
	}
	
	/* Speed estimated by IRC */
	public void setSpeedIrc(float speed) {
		speedIrc = speed;
	}
		
	public float getSpeedIrc() {
		return speedIrc;
	}

	/* Speed estimated from BEMF */
	public void setSpeedBemf(float speed) {
		speedBemf = speed;
	}
	
	public float getSpeedBemf() {
		return speedBemf;
	}
	
	/* Friction estimated by kalman filter */
	@Override
	public CarMeasurement clone() {
		CarMeasurement clone = new CarMeasurement();
		clone.speed = this.speed;
		clone.distanceFront = this.distanceFront;
		clone.distanceBack = this.distanceBack;
		clone.accX = this.accX;
		clone.accY = this.accY;
		clone.accZ = this.accZ;
		clone.pitchRate = this.pitchRate;
		clone.rollRate = this.rollRate;
		clone.yawRate = this.yawRate;
		clone.appliedPwm = this.appliedPwm;
		clone.speedIrc = this.speedIrc;
		clone.speedBemf = this.speedBemf;
		clone.speedKalman = this.speedKalman;
		clone.frictionKalman = this.frictionKalman;
		clone.motorI = this.motorI;
		clone.motorBemf = this.motorBemf;
		clone.railV = this.railV;
		clone.supercapV = this.supercapV;
		clone.desiredI = this.desiredI;
		return clone;
	}
	
	
	public void write(ObjectOutput out) throws IOException {
		out.writeFloat(getSpeed());
		out.writeFloat(getDistanceFront());
		out.writeFloat(getDistanceBack());
		out.writeFloat(getAccX());
		out.writeFloat(getAccY());
		out.writeFloat(getAccZ());
		out.writeFloat(getPitchRate());
		out.writeFloat(getRollRate());
		out.writeFloat(getYawRate());
		out.writeFloat(getAppliedPwm());
		out.writeFloat(getSpeedIrc());
		out.writeFloat(getSpeedBemf());
		out.writeFloat(getSpeedKalman());
		out.writeFloat(getFriction());
		out.writeFloat(getMotorI());
		out.writeFloat(getMotorBemf());
		out.writeFloat(getRailV());
		out.writeFloat(getSupercapV());
		out.writeFloat(getDesiredI());
		out.writeFloat(getDesiredSpeed());
	}
	
	public void read(ObjectInput in) throws IOException {
		setSpeed(in.readFloat());
		setDistanceFront(in.readFloat());
		setDistanceBack(in.readFloat());
		setAccX(in.readFloat());
		setAccY(in.readFloat());
		setAccZ(in.readFloat());
		setPitchRate(in.readFloat());
		setRollRate(in.readFloat());
		setYawRate(in.readFloat());
		setAppliedPwm(in.readFloat());
		setSpeedIrc(in.readFloat());
		setSpeedBemf(in.readFloat());
		setSpeedKalman(in.readFloat());
		setFriction(in.readFloat());
		setMotorI(in.readFloat());
		setMotorBemf(in.readFloat());
		setRailV(in.readFloat());
		setSupercapV(in.readFloat());
		setDesiredI(in.readFloat());
		setDesiredSpeed(in.readFloat());
	}

	public String toCSV() {
		StringBuilder s = new StringBuilder();
		s.append(getSpeed()).append(",");
		s.append(getDistanceFront()).append(",");
		s.append(getDistanceBack()).append(",");
		s.append(getAccX()).append(",");
		s.append(getAccY()).append(",");
		s.append(getAccZ()).append(",");
		s.append(getPitchRate()).append(",");
		s.append(getRollRate()).append(",");
		s.append(getYawRate()).append(",");
		s.append(getAppliedPwm()).append(",");
		s.append(getSpeedIrc()).append(",");
		s.append(getSpeedBemf()).append(",");
		s.append(getSpeedKalman()).append(",");
		s.append(getFriction()).append(",");
		return s.toString();
	}

	public String CSVHeader() {
		return "Speed,DistanceF,DistanceB,AccX,AccY,AccZ,Pitch,Roll,Yaw,AppliedPwm,SpeedIRC,SpeedBEMF,SpeedKalman,friction";
	}

	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		ZonedDateTime dateTime = ZonedDateTime.now();
		str.append(dateTime.toString()).append("\r\n");
		str.append("Speed:").append(getSpeed()).append("\n");
		str.append("Distance Front:").append(getDistanceFront()).append("\n");
		str.append("Distance Back:").append(getDistanceBack()).append("\n");
		str.append("Acceleration XYZ:").append(getAccX()).append(",").append(getAccY()).append(",").append(getAccZ())
				.append("\n");
		str.append("Gyro XYZ:").append(getRollRate()).append(",").append(getPitchRate()).append(",")
				.append(getYawRate()).append("\n");
		return str.toString();
	}

	public float getMotorI() {
		return motorI;
	}

	public void setMotorI(float motorI) {
		this.motorI = motorI;
	}

	public float getMotorBemf() {
		return motorBemf;
	}

	public void setMotorBemf(float motorBemf) {
		this.motorBemf = motorBemf;
	}

	public float getRailV() {
		return railV;
	}

	public void setRailV(float railV) {
		this.railV = railV;
	}

	public float getSupercapV() {
		return supercapV;
	}

	public void setSupercapV(float supercapV) {
		this.supercapV = supercapV;
	}
	
	public float getDesiredI() {
		return this.desiredI;
	}
	
	public void setDesiredI(float desiredI){
		this.desiredI = desiredI;
	}
	
	public float getDesiredSpeed() {
		return desiredSpeed;
	}
	
	public void setDesiredSpeed(float desiredSpeed){
		this.desiredSpeed = desiredSpeed;
	}
}
