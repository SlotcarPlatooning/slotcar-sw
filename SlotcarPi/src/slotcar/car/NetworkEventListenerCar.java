package slotcar.car;
import slotcar.SlotcarState;
import slotcar.network.CarPacket;

public interface NetworkEventListenerCar  {
	/**
	 * Event handler for received message confirming that the vehicle successfully joined the network
	 * @param logAddr passed parameters. The parsedObject is null
	 */
    public void handleCarLoggingAck(byte logAddr);
    
    /**
     * Event handler for received message requesting the vehicle to start the positioning procedure.
     */
//    public void handleStartPositioning();
    
    /**
     * Event handler for received message requesting the vehicle to stop the positioning procedure.
     */
//    public void handleStopPositioning();
        
    /**
     * Event handler for received message sending to the car new settings
     * @param payload  passed parameters. The parsedObject is byte array with the settings
     */
    public void handleUploadSettings(byte sourceAddr, byte[] payload);
    
    /**
     * Event handler for received message requesting the car to upload its process data to the sender
     */
    public void handleImageRequest(byte sourceAddr);
    
    /**
     * Event handler for received message requesting the car to reconnect to the network
     */
    public void handleReconnectRequest();
    
    public void handleStatusRequest(byte sourceAddr);
    
    /**
     * Event handler for received message having the updated routing table
     * @param e passed parameters. The parsedObject is an byte array with addresses
     */
    public void handleSyncCarList(byte[] data);
    
//    /**
//     * Event handler for received message requesting the car to start experiment
//     * @param e passed parameters. The parsedObject is integer with the number of experiment
//     */
//    public void handleStartExperiment(NetworkEventParsed e);
//    
//    /**
//     * Event handler for received message requesting the car to stop experiment
//     * @param e passed parameters. The parsedObject is null
//     */
//    public void handleStopExperiment(NetworkEventParsed e);
//    
    /**
     * Event handler for received message containing the process data of some vehicle
     * @param payload passed parameters. The parsedObject is the byte array of the packet payload. Can be processed by readExternal method of carImage
     */
    
    /**
     * Event handler for received message with unknown service number
     * @param packet 
     */
    public void handleUnknownService(CarPacket packet);
    
    /**
     * Event handler for received message requiring the car to start the communication test
     */
    public void handleCommunicationTestStart();
    
    /**
     * Event handler for received message in a communication test
     */
    public void handleCommunicationTestPacket();
    
    /**
     * Event handler for received message requiring the car to end the communication test. The car should reply by sending the number of received packet
     */
    public void handleCommunicationTestEnd();

	public void handleControllerConstants(byte sourceAddr, byte[] data);

	public void handleControllerConstantsRequest(String ctlName, byte sourceAddr);
	
	public void handleControlStart();

	public void handleControlStop();

	public void handleCalibrationA(float refDistance);

	public void handleCalibrationB();

	public void handleLoggingStart(byte[] payload);

	public void handleLoggingStop();

	public void handleStopCommunication();

	public void handleStartCommunication();

	public void handleImage(byte sourceAddr, byte[] payload);

	public void handleStateChange(SlotcarState state);

	public void handleManualSpeed(float speed);
	
	public void handleClockReq(byte sourceAddr);
	
	public void handleNetStatReq(byte sourceAddr);
    
	public void handleStartProbe(int ps, int ipt);
	
	public void handleStopProbe();
	
	public void handleProbe(byte[] bogus);
    
}
