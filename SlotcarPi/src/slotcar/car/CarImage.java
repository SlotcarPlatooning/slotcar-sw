package slotcar.car;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import slotcar.CarList;
import slotcar.Config;
import slotcar.SlotcarState;
import slotcar.network.NetworkFaultParameters;
import slotcar.pi.controller.CarImageInterface;

public class CarImage implements CarImageInterface {
	private CarMeasurement measurements;
	private CarControl control;
	private SlotcarState state = SlotcarState.READY;
	private NetworkFaultParameters netFaultParams;
	private long timeLastSeen;
	private long timeStatusWasSet;
	private int syncStatus;
	// position in the platoon
	private int position;

	public long getTimeLastSeen() {
		return timeLastSeen;
	}

	public void setTimeLastSeen(long timeLastSeen) {
		this.timeLastSeen = timeLastSeen;
	}

	public long getTimeOfChange() {
		return getCarControl().getTimeOfChange();
	}

	private final byte addr; // logical address

	/**
	 * Number of received packets when doing a communication test
	 */
	private int numberOfReceivedCommTestPackets = 0;
	
	public void setControl(CarControl control) {
		this.control = control;
	}
	
	public void setMeasurements(CarMeasurement measurements) {
		this.measurements = measurements;
	}
	
	public void setNetFaultParams(NetworkFaultParameters netFaultParams) {
		this.netFaultParams = netFaultParams;
	}
	
	public CarImage(byte addr, CarMeasurement measurement, CarControl control, NetworkFaultParameters netFaultParams) {
		this.addr = addr;
		this.control = control;
		this.measurements = measurement;
		this.netFaultParams = netFaultParams;
	}

	public CarImage(CarMeasurement measurement, CarControl control, NetworkFaultParameters netFaultParams) {
		this(Config.DEFAULT_ADDR, measurement, control, netFaultParams);

	}

	public CarImage(byte addr) {
		this(addr, new CarMeasurement(), new CarControl(), new NetworkFaultParameters());
	}

	public CarImage() {
		this(Config.DEFAULT_ADDR);
	}

	public SlotcarState getState() {
		return state;
	}

	public void setState(SlotcarState state) {
		this.state = state;
		timeStatusWasSet = System.currentTimeMillis();
	}

	public long getTimeStatusWasSet() {
		return timeStatusWasSet;
	}

	public CarMeasurement getCarMeasurement() {
		return measurements;
	}

	public CarControl getCarControl() {
		return control;
	}
	
	public NetworkFaultParameters getNetFaultParams() {
		return netFaultParams;
		
	}

	public byte getLogAddr() {
		return addr;
	}

	public int getReceivedCommTestPackets() {
		return numberOfReceivedCommTestPackets;
	}

	// public boolean isConnected() {
	// return connected;
	// }
	//
	// public void setConnected(boolean connected) {
	// this.connected = connected;
	// }

	public void setReceivedCommTestPackets(int value) {
		this.numberOfReceivedCommTestPackets = value;
	}

	// public void setControl(CarControl data) {
	// this.control = data;
	// }

	public void read(ObjectInput in, boolean readControl) throws IOException {
		@SuppressWarnings("unused")
		byte addr = in.readByte();

		measurements.read(in);
		this.control.read(in, readControl);
		netFaultParams.read(in);
	}

	public void write(ObjectOutput out) throws IOException {
		out.write(addr);
		measurements.write(out);
		control.write(out);
		netFaultParams.write(out);
	}

	public String toCSV() {
		StringBuilder s = new StringBuilder();
		s.append(control.toCSV()).append(",").append(measurements.toCSV());
		return s.toString();
	}

	public String CSVHeader() {
		StringBuilder s = new StringBuilder();
		s.append(control.CSVHeader()).append(",").append(measurements.CSVHeader());
		return s.toString();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Car ").append(getLogAddr()).append("\n");
		sb.append(getCarMeasurement().toString());
		sb.append(getCarControl().toString());
		sb.append(netFaultParams.toString());
		return sb.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + addr;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CarImage other = (CarImage) obj;
		if (addr != other.addr)
			return false;
		return true;
	}

//	public void ReadFrom(byte[] data) {
//
//		ObjectInputStream in = null;
//		try {
//			in = new ObjectInputStream(new ByteArrayInputStream(data));
//			read(in,true);
//		} catch (IOException | ClassNotFoundException e1) {
//			System.err.println();
//		} finally {
//			if (in != null) {
//				try {
//					in.close();
//				} catch (IOException e1) {
//					e1.printStackTrace();
//				}
//			}
//		}
//	}

	// public void setSynchronized(boolean b) {
	// synced = b;
	// }
	//
	// public boolean isSynchronized() {
	// if (getCarControl().hasChanged()) {
	// return false;
	// } else {
	// return synced;
	// }
	// }

	public void setSyncStatus(int syncStatus) {
		this.syncStatus = syncStatus;
	}

	/**
	 * Synchronization status:
	 * <p>
	 * 1 if (gui > car)
	 * <p>
	 * -1 if (gui < car)
	 * <p>
	 * 0 if (gui = car)
	 * <p>
	 * 
	 * @return synchronizatio status
	 */
	public int getSyncStatus() {
		return syncStatus;
	}

	public void setLastStatusSync() {
		// TODO Auto-generated method stub

	}

	/**
	 * @return who has more recent change
	 */
	public int compareTimeOfChange(CarImage car) {
		return Long.compare(getTimeOfChange(), car.getTimeOfChange());
	}

	// @Override
	// public int compareTo(CarImage car) {
	// if(getPosition() < car.getPosition()){
	// return -1;
	// }else{
	// return 1;
	// }
	// }

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	@Override
	public boolean isFirst() {
		if (CarList.getInstance().getFirst().equals(this)) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isLast() {
		if (CarList.getInstance().getLast().equals(this)) {
			return true;
		}
		return false;
	}
}
