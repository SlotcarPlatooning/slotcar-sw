  package slotcar.car;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

import slotcar.Config;
import slotcar.Rolling;

public class Experiment {

	// false: using just CSMA/CD
	// true: using token ring
	private static final boolean TOKEN_RING = false;

	
	public static final int STATISTICS_AVG_SIZE = 200;

	private int platoonSize;
	private ScheduledExecutorService scheduler;
	private AtomicInteger delayCount = new AtomicInteger();

	private Car car;

	boolean started;

	private static long experimentTimeStart;
	private static long experimentTimeStop;

	public static long experimentTime; // duration of last experiment
	/* statistics */
	public static long timeCom; // time of last communication procedure
	public static Rolling timeComAvg = new Rolling(STATISTICS_AVG_SIZE ); // avarage

	public static int periodStampIndex;
	public static long periodStamp;

	public static long periodCom; // time of last communication period
	public static Rolling periodComAvg = new Rolling(STATISTICS_AVG_SIZE ); // avarage

	public static int counterRegular; // on time
	public static int counterTimeout; // timeout

	public static Map<Byte, Rolling> othersPeriodComAvg = new HashMap<>();
	public static Map<Byte, Long> othersPeriodStamps = new HashMap<>();

	private long lastComTime; // the last time it was communicating;

	public Experiment(Car car) {
		this.car = car;
	}

	/**
	 * Starts the communication thread
	 */
	public void start() {

		if (started) {
			return;
		} else {
			started = true;
		}
		experimentTimeStart = System.currentTimeMillis();

		periodStamp = System.currentTimeMillis();

		this.platoonSize = car.getCars().size();

		if (car.getPosition() == 0) {
			// the leader should kick off the communication
			Logger.getGlobal().info("I am the leader, I start the communication.");
			car.shareImage();
		}

		delayCount.set(platoonSize);

		scheduler = Executors.newSingleThreadScheduledExecutor();

		scheduler.scheduleAtFixedRate(new Runnable() {
			@Override
			public void run() {
				if (TOKEN_RING) {
					if (delayCount.decrementAndGet() == 0) {
						doCommunication();
						Experiment.counterTimeout++;
					}
				} else {
					doCommunication();
					Experiment.counterTimeout++;
				}
			}
		}, 0, Config.EXPERIMENT_PERIOD, Config.EXPERIMENT_PERIOD_UNIT);
	}

	/**
	 * Stops the communication thread
	 */
	public void stop() {
		if (!started) {
			return;
		} else {
			started = false;
		}

		experimentTimeStop = System.currentTimeMillis();
		experimentTime = experimentTimeStop - experimentTimeStart;
		scheduler.shutdownNow();
	}

	public void communicationstep(byte previousCar) {
		if (!started)
			return;

		Long stamp = othersPeriodStamps.get(previousCar);

		if (stamp == null) {
			stamp = System.currentTimeMillis();
			othersPeriodStamps.put(previousCar, stamp);
		}

		Long stampNew = System.currentTimeMillis();

		double period = (double) (stampNew - stamp);

		try {
			othersPeriodComAvg.get(previousCar).add(period);
		} catch (NullPointerException ex) {
			Rolling avg = new Rolling(STATISTICS_AVG_SIZE);
			avg.add(period);
			othersPeriodComAvg.put(previousCar, avg);
			Logger.getGlobal().warning("Car is not on the list.");
		}

		othersPeriodStamps.put(previousCar, stampNew);

		int posDiff = 0;
		posDiff = car.getCars().positionDifference(previousCar, car.getPosition());

		if (TOKEN_RING) {
			if (posDiff == 1) {
				//if (3 * (System.currentTimeMillis() - lastComTime)> EXPERIMENT_COM_PERIOD) {
					doCommunication();
					counterRegular++;
				//}
			} else {
				delayCount.set(posDiff);

			}
		}

	}

	private void doCommunication() {
		long stampNew = System.currentTimeMillis();
		periodCom = stampNew - periodStamp;
		periodStamp = stampNew;
		periodComAvg.add((double) periodCom);

		// communication
		long foo = System.currentTimeMillis();
		car.shareImage();
		this.delayCount.set(this.platoonSize + 1);
		lastComTime = System.currentTimeMillis();
		// communication end
		timeCom = System.currentTimeMillis() - foo;
		timeComAvg.add((double) timeCom);
	}

	public static String getStatistics() {

		int all = counterTimeout + counterRegular;
		String period = String.format("Period:\nlast: %d avg: %s\n", periodCom, periodComAvg);
		String proc = String.format("time of sending:\nlast: %d avg: %s\n", timeCom, timeComAvg);
		String regular = String.format("# Regular:  %d/%d\n", counterRegular, all);
		String timeout = String.format("# Timeout:  %d/%d\n", counterTimeout, all);
		String time = String.format("Time of last experiment:  %d\n", experimentTime);

		StringBuilder b = new StringBuilder();
		b.append(period);
		b.append(proc);
		b.append(regular);
		b.append(timeout);
		b.append(time);

		String s;
		b.append("Cars communication time periods:").append("\n");

		for (Entry<Byte, Rolling> e : othersPeriodComAvg.entrySet()) {
			s = String.format("car%d = %s ms", e.getKey(), e.getValue().toString());
			b.append(s).append("\n");
		}

		return b.toString();
	}

}
