package slotcar.car;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.logging.Logger;

import slotcar.RoutingTable;
import slotcar.SlotcarState;
import slotcar.clock.CarClock;
import slotcar.network.ComManager;
import slotcar.network.Communicator;
import slotcar.network.NetworkRole;
import slotcar.network.NewDataEventHandler;
import slotcar.network.ServiceType;

public class CommunicatorCar extends Communicator {

	protected final NetworkEventListenerCar owner;
	
	public CommunicatorCar(NetworkEventListenerCar owner, ComManager comMan, byte myLogAddr) {
		super(comMan, NetworkRole.CAR, myLogAddr);
		this.owner = owner;

		addHandler(ServiceType.CAR_LOGGING_ACK, (event) -> {
			// this is not needed, there is sync table anyway
			// this.addAddr(event.getPacket().getSourceAddr(),
			// event.getPacket().getPayload()); //add the car the
			// the list of cars and routing table
			this.setConnected(true);
			owner.handleCarLoggingAck(event.getPacket().getSourceAddr());
		});

//		addHandler(ServiceType.POSITIONING_START, (event) -> {
//			owner.handleStartPositioning();
//		});

//		addHandler(ServiceType.POSITIONING_STOP, (event) -> {
//			owner.handleStopPositioning();
//		});
		
		addHandler(ServiceType.STATE_CHANGE, (event) -> {
			ByteBuffer bf = ByteBuffer.wrap(event.getPacket().getPayload());
			SlotcarState state = SlotcarState.getByVal(bf.getInt());
			owner.handleStateChange(state);
		});
		
		addHandler(ServiceType.SYNC_CAR_LIST, (event) -> {
			Logger.getGlobal().fine("Processed position table");
			owner.handleSyncCarList(event.getPacket().getPayload());
		});

		addHandler(ServiceType.UPLOAD_SETTINGS, (event) -> {
			owner.handleUploadSettings(event.getPacket().getSourceAddr(), event.getPacket().getPayload());

		});

		addHandler(ServiceType.IMAGE_REQUEST, (event) -> {
			owner.handleImageRequest(event.getPacket().getSourceAddr());

		});

		addHandler(ServiceType.RECONNECT_REQUEST, (event) -> {
			this.setConnected(false);
			this.connectToServer();
			owner.handleReconnectRequest();
		});

		addHandler(ServiceType.CONTROLLER_CONSTANTS_REQUEST, (event) -> {
			String ctlName = new String(event.getPacket().getPayload());
			Logger.getGlobal().fine("Controller params requested " + ctlName);
			owner.handleControllerConstantsRequest(ctlName, event.getPacket().getSourceAddr());
		});

		addHandler(ServiceType.CONTROLLER_CONSTANTS, (event) -> {
			owner.handleControllerConstants(event.getPacket().getSourceAddr(), event.getPacket().getPayload());
		});

		addHandler(ServiceType.SYNC_ROUTING_TABLE, (event) -> {
			RoutingTable.deserializeFromBytes(event.getPacket().getPayload());
		});

		addHandler(ServiceType.IMAGE, (event) -> {
			owner.handleImage(event.getPacket().getSourceAddr(), event.getPacket().getPayload());
		});

		addHandler(ServiceType.COMMUNICATION_TEST_START, (event) -> {
			owner.handleCommunicationTestStart();
		});

		addHandler(ServiceType.COMMUNICATION_TEST_PACKET, (event) -> {
			owner.handleCommunicationTestPacket();
		});

		addHandler(ServiceType.COMMUNICATION_TEST_END, (event) -> {
			owner.handleCommunicationTestEnd();
		});

		addHandler(ServiceType.CONTROL_START, (event) -> {
			owner.handleControlStart();
		});

		addHandler(ServiceType.CONTROL_STOP, (event) -> {
			owner.handleControlStop();
		});

		addHandler(ServiceType.CALIBRATION_A, (event) -> {
			
			Logger.getGlobal().info("received callibration A packet");
			float refDistance = ByteBuffer.wrap(event.getPacket().getPayload()).getFloat();
			Logger.getGlobal().info("reference distance = " + refDistance);
			owner.handleCalibrationA(refDistance);
		});

		addHandler(ServiceType.CALIBRATION_B, (event) -> {
			owner.handleCalibrationB();
		});

		addHandler(ServiceType.LOGGING_START, (event) -> {
			owner.handleLoggingStart(event.getPacket().getPayload());
		});

		addHandler(ServiceType.LOGGING_STOP, (event) -> {
			owner.handleLoggingStop();
		});

		addHandler(ServiceType.COMMUNICATION_START, (event) -> {
			owner.handleStartCommunication();
		});

		addHandler(ServiceType.COMMUNICATION_STOP, (event) -> {
			owner.handleStopCommunication();
		});

		
		addHandler(ServiceType.STATUS_REQUEST, (event) -> {
			owner.handleStatusRequest(event.getPacket().getSourceAddr());
		});
		
		addHandler(ServiceType.MANUAL_SPEED, (event) -> {
			ByteBuffer bf = ByteBuffer.wrap(event.getPacket().getPayload());
			owner.handleManualSpeed(bf.getFloat());
		});

		// case START_EXPERIMENT:
		// int expNumber =
		// ByteBuffer.wrap(event.getPacket().getPayload()).getInt();
		// event = new NetworkEventParsed(this, event.getPacket(), expNumber);
		// owner.handleStartExperiment(event);
		// break;
		// case STOP_EXPERIMENT:
		// event = new NetworkEventParsed(this, event.getPacket(), null);
		// owner.handleStopExperiment(event);
		// break;
		
		addHandler(ServiceType.NET_CLOCK_LOG_REQ, (event) -> {
			owner.handleClockReq(event.getPacket().getSourceAddr());
		});
		
		addHandler(ServiceType.NET_STAT_LOG_REQ, (event) -> {
			owner.handleNetStatReq(event.getPacket().getSourceAddr());
		});
		
		addHandler(ServiceType.NET_START_PROBE, (event) -> {
			ByteBuffer bb = ByteBuffer.wrap(event.getPacket().getPayload());
			int ps = bb.getInt();
			int ipt = bb.getInt();
			owner.handleStartProbe(ps, ipt);
		});
		
		addHandler(ServiceType.NET_STOP_PROBE, (event) -> {
			owner.handleStopProbe();
		});
		
		addHandler(ServiceType.NET_PROBE, (event) -> {
			owner.handleProbe(event.getPacket().getPayload());
		});

	}
	
	@Override
	public NewDataEventHandler getDefaultHandler() {
		return (event) -> {
			owner.handleUnknownService(event.getPacket());
		};
	}

}
