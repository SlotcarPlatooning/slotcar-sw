package slotcar.car;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.time.ZonedDateTime;

import slotcar.pi.controller.CarControlInterface;
import slotcar.pi.controller.OutputType;

public class CarControl implements CarControlInterface {
	static final long serialVersionUID = 43L;

	private boolean controlled;
	private float refDistance;
	private float refSpeed;
	private float refPwm;
	private float timeHeadway;

	// it says that control settings has changed since the last synchronization
	// so it needs to be synchronized again
	private boolean changed; // this is just for matlab com
	private long timeOfChange;

	private OutputType outputType;
	private String controller = "";

	public CarControl() {
	}

	@Override
	public float getReferencePwm() {
		return this.refPwm;
	}

	@Override
	public float getReferenceSpeed() {
		return this.refSpeed;
	}

	@Override
	public float getReferenceDistance() {
		return this.refDistance;
	}

	@Override
	public float getTimeHeadway() {
		return this.timeHeadway;
	}

	public void setReferencePwm(float pwm) {
		if (getReferencePwm() != pwm) {
			setChanged();
			this.refPwm = pwm;
		}
	}

	public void setReferenceSpeed(float speed) {
		if (getReferenceSpeed() != speed) {
			setChanged();
			this.refSpeed = speed;
		}
	}

	public void setReferenceDistance(float distance) {
		if (getReferenceDistance() != distance) {
			setChanged();
			this.refDistance = distance;
		}
	}

	public void setTimeHeadway(float timeHeadway) {
		if (getTimeHeadway() != timeHeadway) {
			setChanged();
			this.timeHeadway = timeHeadway;
		}
	}

	private void setChanged() {
		this.changed = true;
		timeOfChange = System.currentTimeMillis();
	}

	public long getTimeOfChange() {
		return timeOfChange;
	}

	public boolean hasChanged() {
		return this.changed;
	}

	public void clearUpdated() {
		this.changed = false;
	}

	public void setControlled(boolean controlled) {
		if (isControlled() != controlled) {
			setChanged();
			this.controlled = controlled;
		}
	}

	public boolean isControlled() {
		return this.controlled;
	}

	public void setController(String controller) {
		if(controller == null){
			return;
		}
		if (!getController().equals(controller)) {
			setChanged();
			this.controller = controller;
		}
	}

	@Override
	public String getController() {
		return this.controller;
	}

	public void setOutputType(OutputType out) {
		this.outputType = out;
	}

	@Override
	public OutputType getOutputType() {
		return this.outputType;
	}

	public String toCSV() {
		StringBuilder s = new StringBuilder();
		s.append(getReferencePwm()).append(",");
		s.append(getReferenceSpeed()).append(",");
		s.append(getReferenceDistance()).append(",");
		s.append(getController());
		return s.toString();
	}

	public String CSVHeader() {
		return "refPWM,refSpeed,refDistance,Reg";
	}

	public void read(ObjectInput in, boolean store) throws IOException {

		long timeOfChange = in.readLong();
		String s = in.readUTF();
		boolean controlled = in.readBoolean();
		float refPwm = in.readFloat();
		float refSpeed = in.readFloat();
		float refDistance = in.readFloat();
		float timeHeadway = in.readFloat();
		
		if (store) {
			// Controller must be set before controlled
			setController(s);
			setControlled(controlled);
			setReferencePwm(refPwm);
			setReferenceSpeed(refSpeed);
			setReferenceDistance(refDistance);
			setTimeHeadway(timeHeadway);
			this.timeOfChange = timeOfChange;
			// time of change must be set as last, because previous functions
			// rewrites it
		}
	}

	public void write(ObjectOutput out) throws IOException {
		// Controller must be set before controlled
		out.writeLong(timeOfChange);
		out.writeUTF(getController());
		out.writeBoolean(isControlled());
		out.writeFloat(getReferencePwm());
		out.writeFloat(getReferenceSpeed());
		out.writeFloat(getReferenceDistance());
		out.writeFloat(getTimeHeadway());
	}

	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		ZonedDateTime dateTime = ZonedDateTime.now();
		str.append(dateTime.toString()).append("\r\n");
		str.append("control:").append(isControlled() ? "Enabled" : "Disabled").append("\n");
		str.append("Controller:").append(getController()).append("\n");
		str.append("RefPWM").append(getReferencePwm()).append("\n");
		str.append("RefSpeed").append(getReferenceSpeed()).append("\n");
		str.append("RefDistance").append(getReferenceDistance()).append("\n");
		return str.toString();
	}

	@Override
	public int hashCode() {
		float ret = 0;
		ret += getReferenceDistance();
		ret += getReferenceSpeed();
		ret += getReferencePwm();
		ret += getTimeHeadway();
		ret *= 100.0f;
		// ret +=
		return (int) ret + getController().hashCode();

	}


}
