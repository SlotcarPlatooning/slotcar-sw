package slotcar.car;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import slotcar.Config;

public class StateLogger {
	private final CarImage car;

	private File f;
	private OutputStreamWriter out;
	private ScheduledExecutorService scheduler;
	
	public void start(int id, boolean ram) {
		stop();
		id = id > 0 ? id : -id;
		String dir = ram ? Config.EXP_RAM : Config.EXP_FLASH;
		String fileName = String.format("C%02dE%d", car.getLogAddr(), id);

		this.f = new File(dir + fileName);

		if (f != null) {
			try {
				f.getParentFile().mkdirs();
				f.createNewFile();
			} catch (IOException e) {
				Logger.getGlobal().log(Level.SEVERE, "Could not create directory or file for experiment", e);
				return;
			}

			try {
				out = new OutputStreamWriter(new FileOutputStream(f));
			} catch (FileNotFoundException e) {
				Logger.getGlobal().log(Level.SEVERE, "Could not open experiment output stream.", e);
				return;
			}

			try {
				out.write("Time,");
				out.write(car.CSVHeader());
				out.write("\n");
			} catch (IOException e) {
				Logger.getGlobal().log(Level.SEVERE, "Could not write header to experiment file.", e);
				return;
			}

			scheduler = Executors.newSingleThreadScheduledExecutor();
			scheduler.scheduleAtFixedRate(() -> {
				try {
					out.write(String.valueOf(System.currentTimeMillis()));
					out.write(car.toCSV());
					out.write("\n");
				} catch (IOException e) {
					// If stream is already closed then it is ok.
					Logger.getGlobal().log(Level.SEVERE, "Logging state Error.", e);
				}
			}, 0, Config.LOG_SAMPLE_PERIOD, Config.LOG_SAMPLE_PERIOD_UNIT);
			Logger.getGlobal().info("Data log started");
		}
	}

	public void stop() {

		try {
			scheduler.shutdown();
			scheduler.awaitTermination(100, TimeUnit.MILLISECONDS);
		}catch(NullPointerException e){
			// do nothing
		} catch (InterruptedException e) {
			Logger.getGlobal().log(Level.SEVERE, "", e);
		}

		try {
			out.close();
		} catch (NullPointerException ex) {
			// do nothing
		} catch (IOException e) {
			Logger.getGlobal().log(Level.SEVERE, "", e);
		}
	}

	public StateLogger(CarImage car) {
		this.car = car;
	}

}
