package slotcar.car;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class NetworkProbe {

		private int packetSize;  //[bytes]
		
		private int interPacketTime; // [ms]

		private ScheduledExecutorService scheduler;
		
		private boolean started;

		private Car car;
		
		public NetworkProbe(Car car) {
			this.car = car;
		}
		
		public void start() {

			if (started || interPacketTime <= 0 || packetSize < 1) {
				return;
			} else {
				started = true;
			}

			scheduler = Executors.newSingleThreadScheduledExecutor();

			scheduler.scheduleAtFixedRate(new Runnable() {
				@Override
				public void run() {
						doCommunication();
				}
			}, 0, interPacketTime, TimeUnit.MILLISECONDS);
		}

		public void stop() {
			if (!started) {
				return;
			} else {
				started = false;
			}
			scheduler.shutdownNow();
		}

		private void doCommunication() {
			car.com.sendProbe(packetSize);
		}

		/**
		 * @return the packetSize
		 */
		public int getPacketSize() {
			return packetSize;
		}

		/**
		 * @param packetSize the packetSize to set
		 */
		public void setPacketSize(int packetSize) {
			this.packetSize = packetSize;
		}

		/**
		 * @return the interPacketTime
		 */
		public int getInterPacketTime() {
			return interPacketTime;
		}

		/**
		 * @param interPacketTime the interPacketTime to set
		 */
		public void setInterPacketTime(int interPacketTime) {
			this.interPacketTime = interPacketTime;
		}
}
