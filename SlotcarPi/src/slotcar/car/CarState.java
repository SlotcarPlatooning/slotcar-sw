package slotcar.car;

import slotcar.SlotcarState;

abstract class CarState {
	
	private final SlotcarState state;
	
	public CarState(SlotcarState state){
		this.state = state;
	}
	
	public final SlotcarState getSlotcarState(){
		return state;
	}
	
	public abstract void init();

	public abstract void run();

	public abstract void end();
}