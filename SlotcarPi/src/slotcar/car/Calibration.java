package slotcar.car;

import java.util.logging.Logger;
import slotcar.pi.controller.CarImageInterface;

/**
 * Calibrates car's distance sensors.
 */
public abstract class Calibration {

	static private Logger log = Logger.getLogger(Calibration.class.getName());
	
	private final Car car;
	
	public Calibration(Car car) {
		this.car = car;
	}

	/**
	 * Calibrates front distance sensor
	 */
	public abstract void calibrateFront(float refDist);

	/**
	 * Calibrates rear distance sensor
	 */
	public abstract void calibrateRear(float refDist);

	/**
	 * Calibrates car's distance sensors to reference distance set by user
	 * 
	 * @param referenceDistanceFront
	 *            Reference distance for front distance sensor.
	 * @param referenceDistanceRear
	 *            Reference distance for rear distance sensor.
	 */
	public void calibrate(float referenceDistanceFront, float referenceDistanceRear) {
		String info = String.format("Calibration to reference front %f and rear %f. My position is %s.",
				referenceDistanceFront, referenceDistanceRear, car.printPosition());
		log.info(info);
		
		if (car.getPosition() != 0) {
			// I am first, so I must calibrate front sensor
			float before = car.getCarMeasurement().getDistanceFront();
			calibrateFront(referenceDistanceFront);
			float after = car.getCarMeasurement().getDistanceFront();
			String infoFront = String.format("I am not first, I must calibrate front sensor. Brefore %f and after %f.", before, after);
			log.info(infoFront);
		} else {
			Logger.getGlobal().info("I am first, so I must NOT calibrate front sensor");
		}

		if (car.getPosition() != car.getCars().size() - 1) {
			float before = car.getCarMeasurement().getDistanceBack();
			calibrateRear(referenceDistanceRear);
			float after = car.getCarMeasurement().getDistanceBack();
			String infoFront = String.format("I am not last, I must calibrate front sensor. Brefore %f and after %f.", before, after);
			log.info(infoFront);
		} else {
			Logger.getGlobal().info("I am last, so I must NOT calibrate rear sensor");
		}
	}

	/**
	 * Calibrates car's distance sensors to reference distance set by user
	 * 
	 * @param referenceDistance
	 *            Reference distance for both front and rear distance sensors.
	 */
	public void calibrate(float referenceDistance) {
		calibrate(referenceDistance, referenceDistance);
	}

	/**
	 * Calibrates car's distance sensors to its neighbors. 
	 * The experiment has to be running, because the car needs to know its neighbor's measurements.
	 * TODO needs to be tested
	 */
	public void calibrate() {

		int myPosition = car.getPosition();

		if (myPosition % 2 == 0) {
			log.info("My position is even, I do not calibrate my sensors.");
			return;
		}
		
		log.info("My position is odd, I calibrate my sensors.");
		
		CarImageInterface carFront = car.getCars().getByPos(myPosition + 1);
		CarImageInterface carBack = car.getCars().getByPos(myPosition - 1);

		float refDistBack = carFront.getCarMeasurement().getDistanceBack();
		float refDistFront = carBack.getCarMeasurement().getDistanceFront();

		calibrate(refDistFront, refDistBack);
	}
}
