package slotcar.pi;

import java.io.File;
import java.io.InputStream;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import slotcar.CarList;
import slotcar.Config;
import slotcar.Controllers;
import slotcar.Rolling;
import slotcar.car.Car;
import slotcar.car.CarControl;
import slotcar.pi.MyVariables.MyVariableList;
import slotcar.pi.controller.Controller;
import slotcar.pi.controller.ControllerNone;
import slotcar.pi.controller.OutputType;
import slotcar.pi.controller.UnsupportedControllerException;
import slotcar.pi.stm.StmMemory;

public class PiControl extends CarControl {

	private static final Logger log = Logger.getLogger(CarControl.class.getName());

	// selected controller
	private Controller ctrler = new ControllerNone();

	// list of controllers
	private final Controllers list = new Controllers();

	// controller executor service
	private ScheduledExecutorService controllerRunner = Executors.newSingleThreadScheduledExecutor();

	private final StmMemory stmMemory = StmMemory.getInstance();

	MyVariableList varList;

	/* Statistics */
	long periodStamp = 0L;
	long period = 0L;
	long stepTime = 0L;
	Rolling periodAvg = new Rolling(100);
	Rolling stepTimeAvg = new Rolling(100);

	private final Car car;
	private final CarList cars;

	public PiControl(PiCar car) {
		super();
		this.car = car;
		this.cars = car.getCars();

		ctrler = list.getDefault();
		setController(list.getDefault().getName());
	}

	public Controllers getControllers() {
		return list;
	}

	@Override
	public void setReferencePwm(float pwm) {
		super.setReferencePwm(pwm);
		stmMemory.referencePWM.set(pwm);
	}

	@Override
	public void setReferenceSpeed(float speed) {
		super.setReferenceSpeed(speed);
		stmMemory.referenceSpeed.set(speed);
	}

	@Override
	public void setOutputType(OutputType out) {
		super.setOutputType(out);
		stmMemory.reg.set(out.getVal());
	}

	@Override
	public OutputType getOutputType() {
		return OutputType.getByVal(stmMemory.reg.get());
	}

	private void setOutput(float f) {
		if (isControlled()) {

			OutputType out = ctrler.getOutputType();

			if (out == OutputType.PWM) {
//				stmMemory.referencePWM.set(f);
				setReferencePwm(f);
			} else if (out == OutputType.SPEED) {
				setReferenceSpeed(f);
//				stmMemory.referenceSpeed.set(f);
			} else {
				setOutputType(OutputType.STOP);
			}

		}
	}

	private void startControl() {
		stopControl();
		setOutputType(ctrler.getOutputType());
		controllerRunner = Executors.newSingleThreadScheduledExecutor();

		periodStamp = System.currentTimeMillis();

		controllerRunner.scheduleAtFixedRate(new Runnable() {
			@Override
			public void run() {
				try {
					long last = System.currentTimeMillis();
					/* control start */
					setOutput(ctrler.step(car, cars));
					/* control end */
					stepTime = System.currentTimeMillis() - last;
					stepTimeAvg.add((double) stepTime);

					long stampNew = System.currentTimeMillis();
					period = stampNew - periodStamp;
					periodStamp = stampNew;
					periodAvg.add((double) period);

				} catch (Exception e) {
					Logger.getGlobal().log(Level.SEVERE, "Controller Step Exception", e);
					PiControl.this.car.sendErrorMessage("Controller Step Exception. Stopping controller " + ctrler.getName());
					PiControl.this.stopControl();
				}
			}
		}, 0, ctrler.getMillisPeriod(), TimeUnit.MILLISECONDS);
	}

	private void stopControl() {
		try {
			controllerRunner.shutdown();
			controllerRunner.awaitTermination(ctrler.getMillisPeriod(), TimeUnit.MILLISECONDS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		setOutputType(OutputType.STOP);
	}

	public String getStatistics() {
		return String.format(
				"Controller statistics: %s\nPeriod: %d\nPeriodAvg: %s\nRef period: %d\nStep time: %d\nStep time avg: %s\n",
				ctrler.getName(), period, periodAvg, ctrler.getMillisPeriod(), stepTime,
				stepTimeAvg);
	}

	@Override
	public synchronized void setController(String name) {

		// if controller is already set, then skip
		if (super.getController().equals(name)) {
			// String s = String.format("Controller %s is already set. No
			// change.", name);
			// Logger.getGlobal().fine(s);
			return;
		}

		log.info("Reading controllers");
		// if controller is not set
		// (1) load all controllers with their constants
		File ctrlDir = Config.getControllersPath();
		list.load(1, ctrlDir);
		list.loadConstants(ctrlDir);
		// (2) Search the list for controller 'name', otherwise Default is set.
		ctrler = list.get(name);
		super.setController(ctrler.getName());
		if (ctrler.getName().equals(name)) {
			// Reset statistics.
			period = 0L;
			stepTime = 0L;
			periodAvg = new Rolling(100);
			stepTimeAvg = new Rolling(100);
			// Stop control.
			stopControl();
			setControlled(false);
		} else {
			// (3) if it is not on the list we set it to Default
			// and send an error message
			car.sendErrorMessage("Controller with \"name\"set to " + name + " was not found. Did you upload the .jar?");
		}
	}

	/**
	 * Tests if the controller with a given name is loaded from a jar file. For
	 * a loaded controller, the constants can be stored.
	 * 
	 * @param contName
	 *            The name of the controller
	 * @return true if the controller was loaded
	 */
	public boolean isControllerLoaded(String contName) {
		Controller c = list.get(contName);

		if (!c.getName().equals(contName)) {
			return false;
		}

		return true;
	}

	@Override
	public synchronized void setControlled(boolean controlled) {

		if (this.ctrler == null) {
			Logger.getGlobal().warning("Controller is Null.");

		} else {
			// watch the order of commands !
			boolean wasControlled = isControlled();
			super.setControlled(controlled);
			// logic after controlled being set
			if (!wasControlled & isControlled()) {
				Logger.getGlobal().info("Control has started.");
				ctrler.init();
				startControl();
			} else if (wasControlled & !isControlled()) {
				Logger.getGlobal().info("Control has stopped.");
				stopControl();
			} else {
				Logger.getGlobal().info("No change in control.");
			}
		}
	}

	public synchronized Controller writeConstantsToController(InputStream in) {
		try {
			return list.readControllerConstantsFrom(in);
		} catch (UnsupportedControllerException e) {
			car.sendErrorMessage(e.getMessage());
		}
		return null;
	}

}
