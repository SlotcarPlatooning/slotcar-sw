package slotcar.pi.peri;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinState;

public class LED {

	// yellow led is used to OS purpose
	private static final Pin PIN_LED6 = Pins.createDigitalPin(22, "GPIO 22");
	//private static final Pin PIN_LED7 = Pins.createDigitalPin(23, "GPIO 23");
	private static final Pin PIN_LED8 = Pins.createDigitalPin(24, "GPIO 24");

	private static final GpioController GPIO = GpioFactory.getInstance();

	public static final GpioPinDigitalOutput BLUE = GPIO.provisionDigitalOutputPin(PIN_LED6, PinState.LOW);
	//public static final GpioPinDigitalOutput YELLOW = GPIO.provisionDigitalOutputPin(PIN_LED7, PinState.LOW);
	public static final GpioPinDigitalOutput RED = GPIO.provisionDigitalOutputPin(PIN_LED8, PinState.LOW);
}
