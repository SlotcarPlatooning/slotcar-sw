package slotcar.pi.peri;

import java.util.EnumSet;

import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinMode;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.RaspiGpioProvider;
import com.pi4j.io.gpio.impl.PinImpl;

public class Pins {
	
	public static Pin createDigitalPin(int address, String name) {
		return new PinImpl(RaspiGpioProvider.NAME, address, name,
				EnumSet.of(PinMode.DIGITAL_INPUT, PinMode.DIGITAL_OUTPUT), PinPullResistance.all());
	}
}
