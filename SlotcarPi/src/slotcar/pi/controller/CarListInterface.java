package slotcar.pi.controller;

public interface CarListInterface {
	
	public CarImageInterface getByPos(int position);
	public CarImageInterface getByAddr(byte addr);
	public CarImageInterface getFirst();
	public CarImageInterface getLast();
	public int size();
}
