package slotcar.pi.controller;

import slotcar.pi.controller.CarImageInterface;
import slotcar.pi.controller.CarListInterface;
import slotcar.pi.controller.ClassInfo;
import slotcar.pi.controller.Controller;
import slotcar.pi.controller.FieldInfo;
import slotcar.pi.controller.OutputType;

@ClassInfo(label = "Speed Controller", description = "Example of a speed controller, using integrated speed controller, so it does nothing", author="none", version="0")
public class ControllerSpeed extends Controller {
	
	@FieldInfo(label="output multiplication", description="")
	public float mult = 1.0f;

	public ControllerSpeed() {
		super(OutputType.SPEED, 100);
	}
	
	@Override
	public void afterWritingConstants() {
		// do nothing
	}

	@Override
	public void init() {
		// do nothing
	}

	@Override
	public float step(CarImageInterface me, CarListInterface cars) {
		float u = mult * me.getCarControl().getReferenceSpeed();
		return u;
	}

}
