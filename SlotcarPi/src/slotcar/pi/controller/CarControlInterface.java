package slotcar.pi.controller;

public interface CarControlInterface {
	public abstract OutputType getOutputType();
	public abstract float getReferencePwm();
	public abstract float getReferenceSpeed();
	public abstract float getReferenceDistance();
	public abstract float getTimeHeadway();
	public abstract String getController();
}
