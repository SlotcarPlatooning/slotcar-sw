package slotcar.pi.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.logging.Logger;

import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.XMLOutputter;

@ClassInfo(label = "ControllerTemplate", description = "Basic template for completing your own controller", version = "1.0", author = "anonymous")
public abstract class Controller {
	Logger log = Logger.getLogger(Controller.class.getName());

	private final OutputType outputType;
	private final long millisPeriod;
	
	public abstract float step(CarImageInterface me, CarListInterface cars);

	public abstract void init();

	public abstract void afterWritingConstants();
	
	public Controller(OutputType outputType, long millisPeriod) {
		this.outputType = outputType;
		this.millisPeriod = millisPeriod;
	}

	public OutputType getOutputType() {
		return outputType;
	}
	
	public long getMillisPeriod(){
		return millisPeriod;
	}

	public final String getLabel() {
		Annotation annotation = this.getClass().getAnnotation(ClassInfo.class);
		if (annotation instanceof ClassInfo) {
			ClassInfo myAnnotation = (ClassInfo) annotation;
			return myAnnotation.label();
		} else {
			return "";
		}
	}

	public final String getName() {
		return getClass().getName();
	}

	public final String getDescription() {
		Annotation annotation = this.getClass().getAnnotation(ClassInfo.class);
		if (annotation instanceof ClassInfo) {
			ClassInfo myAnnotation = (ClassInfo) annotation;
			return myAnnotation.description();
		}
		return "";
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Controller) {
			Controller x = (Controller) obj;
			if (this.getName().equals(x.getName())) {
				return true;
			}
		}
		return false;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getName()).append("\n");
		sb.append(getDescription()).append("\n");
		sb.append("Time period: ").append(getMillisPeriod()).append("\n");
		sb.append("==Constants==").append("\n");
		Field[] fields = this.getClass().getFields();
		for (Field field : fields) {
			try {
				sb.append("[name=\"").append(field.getName()).append("\" value=").append(field.getFloat(this))
						.append("]\n");
			} catch (IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}

		return sb.toString();

	}

	public final Element writeToElement() {
		Element root = new Element("controller");
		root.setAttribute(new Attribute("name", getName()));
		Element data = new Element("constants");
		root.addContent(data);

		for (Field field : this.getClass().getFields()) {
			data.addContent(field2Element(field));
		}
		return root;
	}

	public final static String getFieldLabel(Field field) {
		String s = null;
		Annotation annotation = field.getAnnotation(FieldInfo.class);
		if (annotation instanceof FieldInfo) {
			FieldInfo myAnnotation = (FieldInfo) annotation;
			s = myAnnotation.label();
		}
		if (s == null || s == "") {
			return field.getName();
		} else {
			return s;
		}

	}

	public final static String getFieldDescription(Field field) {

		Annotation annotation = field.getAnnotation(FieldInfo.class);
		if (annotation instanceof FieldInfo) {
			FieldInfo myAnnotation = (FieldInfo) annotation;
			return myAnnotation.description();
		}

		return "";

	}

	private final Element field2Element(Field field) {
		try {
			Element element = new Element("const");
			element.setAttribute("name", field.getName());
			element.setAttribute("type", field.getType().toString());
			element.setAttribute("value", field2String(field));
			return element;
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	}

	public final String field2String(Field field) throws IllegalArgumentException, IllegalAccessException {
		if (field.getType() == float.class) {
			return Float.toString(field.getFloat(this));
		} else if (field.getType() == float[].class) {
			return MatlabArray.toString((float[]) field.get(this));
		} else if (field.getType() == float[][].class) {
			return MatlabArray.toString((float[][]) field.get(this));
		} else if (field.getType() == String.class) {
			return (String) field.get(this);
		} else {
			return null;
		}
	}

	public final void readFrom(Element root) {
		for (Element element : root.getChild("constants").getChildren()) {
			element2Field(element);
		}

		afterWritingConstants();
	}

	private final void element2Field(Element element) {
		String name = element.getAttributeValue("name");
		String value = element.getAttributeValue("value");
		String type = element.getAttributeValue("type");

		try {
			Field field = this.getClass().getField(name);

			if (field.getType().toString().equals(type)) {
				string2field(field, value);
			}

		}catch(NoSuchFieldException e){
			log.severe("No such field " + name);
		}
		catch (IllegalArgumentException | IllegalAccessException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public final void string2field(Field field, String value)
			throws NumberFormatException, IllegalArgumentException, IllegalAccessException {
		if (field.getType() == float.class) {
			field.setFloat(this, Float.parseFloat(value));
		} else if (field.getType() == float[].class) {
			field.set(this, MatlabArray.d1fromString(value));
		} else if (field.getType() == float[][].class) {
			field.set(this, MatlabArray.d2fromString(value));
		} else if (field.getType() == String.class) {
			field.set(this, value);
		}
	}

	public final void writeTo(OutputStream out) {
		try {
			Document doc = new Document();
			doc.setRootElement(writeToElement());
			try {
				XMLOutputter serializer = new XMLOutputter();
				serializer.output(doc, out);
			} catch (IOException e) {
				System.err.println(e);
			}

		} catch (NumberFormatException e) {
			e.printStackTrace(); // TODO nejak lepe odchytavat
		}
	}

}
