package slotcar.pi.controller;

public interface CarMeasurementInterface {
	public float getSpeed();
	public float getDistanceFront();
	public float getDistanceBack();
	public float getAccX();
	public float getAccY();
	public float getAccZ();
	public float getPitchRate();
	public float getRollRate();
	public float getYawRate();
	public float getAppliedPwm();
	public float getFriction();
	public float getSpeedKalman();
}
