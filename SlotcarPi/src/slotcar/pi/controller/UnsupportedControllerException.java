package slotcar.pi.controller;

public class UnsupportedControllerException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 114546545689614L;

	private String controllerName = "Basic Controller";
	public UnsupportedControllerException()
	{
		super("The controller, for which the operation was done, is not supported or installed");
	}
	
	public UnsupportedControllerException(String message)
	{
		super(message);
	}
	
	public UnsupportedControllerException(String message, String controllerName)
	{
		super(message);
		this.setControllerName(controllerName);
	}

	private String getControllerName() {
		return controllerName;
	}

	private void setControllerName(String controllerName) {
		this.controllerName = controllerName;
	}
}
