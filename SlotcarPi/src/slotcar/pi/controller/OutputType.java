package slotcar.pi.controller;

public enum OutputType {
	// !!!!
	// The numbers are set according the settings in STM.
	// They must stay this way because we set them directly to STM.
	STOP(0), PWM(4), SPEED(1) ;

	private final int val;
	
	private OutputType(int val) {
		this.val = val;
	}
	
	public int getVal() {
		return val;
	}
	
	public static OutputType getByVal(int val){
		for (OutputType type: OutputType.values()) {
			if(type.val == val) return type;
		}
		return null;
	}
}