package slotcar.pi.controller;

class MatlabArray {

	private static String removeBrackets(String data) {
		String s = data.trim();
		s = s.substring(1, s.length() - 1);
		return s;
	}

	private static String addBrackets(String data) {
		StringBuilder sb = new StringBuilder();
		sb.append('[').append(data).append(']');
		return sb.toString();
	}

	public static String toString(float[] a) {
		StringBuilder sb = new StringBuilder();
		for (float f : a) {
			sb.append(f).append(',');
		}
		sb.deleteCharAt(sb.length() - 1);
		return addBrackets(sb.toString());
	}

	public static float[] d1fromString(String data) {
		String s = removeBrackets(data);
		String[] numbers = s.split(",");
		float[] arr = new float[numbers.length];

		for (int i = 0; i < arr.length; i++) {
			System.out.println(numbers[i]);
			arr[i] = Float.parseFloat(numbers[i]);
		}
		return arr;
	}

	public static String toString(float[][] a) {
		StringBuilder sb = new StringBuilder();
		for (float[] f : a) {
			for (float g : f) {
				sb.append(g).append(',');
			}
			sb.deleteCharAt(sb.length() - 1);
			sb.append(';');
		}
		sb.deleteCharAt(sb.length() - 1);

		return addBrackets(sb.toString());
	}

	public static float[][] d2fromString(String data) {
		String s = removeBrackets(data);

		String[] rows = s.split(";");
		String[] row = null;
		float[][] a = new float[rows.length][];

		for (int j = 0; j < rows.length; j++) {
			row = rows[j].split(",");
			float[] b = new float[row.length];
			for (int i = 0; i < row.length; i++) {
				b[i] = Float.parseFloat(row[i]);
			}
			a[j] = b;
		}

		// TODO fix exceptions

		return a;
	}

}