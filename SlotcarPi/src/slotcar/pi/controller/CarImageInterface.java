package slotcar.pi.controller;

public interface CarImageInterface {
    public CarMeasurementInterface getCarMeasurement();
    public CarControlInterface getCarControl();
    public byte getLogAddr();
    public int getPosition();
    public boolean isFirst();
    public boolean isLast();
}
