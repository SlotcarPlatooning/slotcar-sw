package slotcar.pi.controller;

import slotcar.pi.controller.CarImageInterface;
import slotcar.pi.controller.CarListInterface;
import slotcar.pi.controller.ClassInfo;
import slotcar.pi.controller.Controller;
import slotcar.pi.controller.OutputType;

@ClassInfo(label = "None", description = "Not a controller", author="slotcar", version="1.0")
public class ControllerNone extends Controller {
	
	public ControllerNone() {
		super(OutputType.STOP, Long.MAX_VALUE);
	}

	@Override
	public void afterWritingConstants() {
		// do nothing
	}

	@Override
	public void init() {
		// do nothing
	}

	@Override
	public float step(CarImageInterface me, CarListInterface cars) {
		// do nothing
		return 0.0f;
	}

}
