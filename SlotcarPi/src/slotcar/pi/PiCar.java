package slotcar.pi;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import slotcar.Config;
import slotcar.Debugging;
import slotcar.car.Car;
import slotcar.car.CarControl;
import slotcar.car.CarMeasurement;
import slotcar.car.Experiment;
import slotcar.clock.CarClock;
import slotcar.network.ComManager;
import slotcar.network.NetworkFaultParameters;
import slotcar.network.ServiceType;
import slotcar.pi.controller.Controller;
import slotcar.pi.peri.LED;
import slotcar.pi.stm.StmMemory;
import slotcar.pi.stm.StmSpi;
import slotcar.wifi.WiFiComManager;

public class PiCar extends Car {

	private static final Logger log = Logger.getGlobal();

	private PiControl control;
	private PiMeasurement measurement;

	public PiControl getPiControl() {
		return control;
	}

	public PiMeasurement getPiMeasurement() {
		return measurement;
	}

	@Override
	public CarControl getCarControl() {
		return getPiControl();
	}

	@Override
	public CarMeasurement getCarMeasurement() {
		return getPiMeasurement();
	}

	/**
	 * Once it is set cannot be changed.
	 */
	@Override
	public void setMeasurements(CarMeasurement measurements) {
		if (getPiMeasurement() == null && measurements instanceof PiMeasurement) {
			super.setMeasurements(measurements);
			this.measurement = (PiMeasurement) measurements;
		}
	}

	/**
	 * Once it is set cannot be changed.
	 */
	@Override
	public void setControl(CarControl control) {
		if (super.getCarControl() == null && control instanceof PiControl) {
			super.setControl(control);
			this.control = (PiControl) control;
		}
	}

	public PiCar() {
		super(null, null, null);
		setControl(new PiControl(this));
		setMeasurements(new PiMeasurement(this));
		setNetFaultParams(new NetworkFaultParameters());
		setCalibration(new PiCalibration(this));

		StmMemory.getInstance().distBackTransmitEn.set(1);

		log.info("Starting LED");
		Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(() -> {
			LED.BLUE.toggle();
		}, 0, 1, TimeUnit.SECONDS);
		
		// waiting for interface
		NetworkInterface iface = null;
		String ifaceName = "wlan0";
		while (iface == null) {
			try {
				log.warning("Searching for interface " + ifaceName);
				iface = NetworkInterface.getByName(ifaceName);
			} catch (SocketException e) {
				log.warning("No interface " + ifaceName+", waiting...");
				try {
					Thread.sleep(500);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
			}
		}
		log.warning("Interface " + ifaceName+" is ON.");
		
	
		ComManager comMan = new WiFiComManager(iface, Config.APP_COM_PORT, Config.APP_COM_PORT);

		init(comMan);
	}

	public static void main(String[] args) {
		/* INIT */
		Debugging.initCAR();

		Config.getInstance().load(Config.CAR_CONF);

		PiCar car = new PiCar();

		if (args.length > 0 && args[0].equals("console")) {
			PiControl control = car.getPiControl();
			BufferedReader br = null;
			try {
				br = new BufferedReader(new InputStreamReader(System.in));
				while (true) {

					System.out.print("Enter pwm or nothing: ");
					String input = br.readLine();

					try {
						float val = Float.valueOf(input);
						StmMemory.getInstance().referencePWM.set(val);
					} catch (NumberFormatException ex) {

					}

					StringBuilder sb = new StringBuilder();
					sb.append("STM").append("\n");
					sb.append(StmSpi.printStats());
					sb.append("Period " + StmMemory.getInstance().period).append("\n");
					sb.append("Duration " + StmMemory.getInstance().lenghtOfUpdate).append("\n");
					sb.append(StmMemory.getInstance().outputs.toString()).append("\n");
					sb.append(StmMemory.getInstance().inputs.toString()).append("\n");
					sb.append(" ++++++++++++++++++++++++++++\n");
					sb.append(car);
					sb.append("Front distance coef = "+ car.getPiMeasurement().getDistFrontCoef());
					sb.append("Back distance coef = " + car.getPiMeasurement().getDistBackCoef());
					System.out.println(sb.toString());

					System.out.println(Experiment.getStatistics());
					System.out.println(control.getStatistics());

				}

			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	/**
	 * Sends a controller's constants to a source address or sends an error
	 * message to the GUI.
	 */
	@Override
	public void handleControllerConstantsRequest(String name, byte sourceAddr) {
		Controller c = control.getControllers().get(name);
		if (!c.getName().equals(name)) {
			String error = String.format("Car %d cannot find controller %s.", getLogAddr(), name);
			sendErrorMessage(error);
		} else {
			com.sendControllerConstants(c, sourceAddr);
		}
	}

	/**
	 * Receives controller constants, sets it to controller and stores it.
	 */
	@Override
	public void handleControllerConstants(byte sourceAddr, byte[] data) {
		ByteArrayInputStream s = new ByteArrayInputStream(data);
		Controller c = control.writeConstantsToController(s);
		control.getControllers().storeControllerConstants(c);
	}

	@Override
	public void handleClockReq(byte sourceAddr) {
		try {
			CarClock.querryState();
		} catch (IOException e) {
			com.sendErrorMessage(e.getMessage(), sourceAddr);
			return;
		}
		com.sendClockLog(sourceAddr, CarClock.logState());
		
	}

	@Override
	public void handleNetStatReq(byte sourceAddr) {
		com.initNetStatLog();
		System.out.println("initializing network logging.");
	}
}
