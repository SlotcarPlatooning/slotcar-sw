package slotcar.pi.MyVariables;

import java.nio.ByteBuffer;

public abstract class MyVariable<T> {
	private final MyVariableManager owner;
	private final int address;
	
	private MyVariableInfo info;
	private volatile long lastSyncTime;
	private volatile T value;

	public MyVariable(T value, int address, MyVariableManager owner) {
		assert address >= 0 ;
		this.address = address;
		this.owner = owner;
		this.value = value;
	}
	
	public void setInfo(MyVariableInfo info) {
		this.info = info;
	}
	
	public String getName() {
		if(this.info == null){
			return "";
		}else{
			return this.info.label();
		}
	}

	public String getUnit() {
		if(this.info == null){
			return "";
		}else{
			return this.info.unit();
		}
	}

	public int getAddress() {
		return this.address;
	}

	@Override
	public String toString() {
		return "[" + getName() + ":" + get() + getUnit() + "]";
	}

	/**
	 * Returns the value of this variable. If periodic update is used or the
	 * last query to the STM was too long ago, it refreshes the value. Otherwise
	 * returns the locally stored value.
	 * 
	 * @return The value of this variable.
	 */
	public synchronized T get() {
		return this.value;
	}

	public synchronized void set(T value) {
		this.value = value;
	}
	
	public synchronized void set(ByteBuffer bf) {
		readFrom(bf);
	}
	
	public long getLastSyncTime() {
		return this.lastSyncTime;
	}

	public void setLastSyncTime() {
		this.lastSyncTime = System.currentTimeMillis();
	}

	@SuppressWarnings("unused")
	private void read() {
		this.owner.MyVariableRead(this);
	}

	@SuppressWarnings("unused")
	private void write() {
		this.owner.MyVariableWrite(this);
	}

	public abstract void readFrom(ByteBuffer b);
	
	public abstract void writeTo(ByteBuffer b);

	public abstract int getSize();
}