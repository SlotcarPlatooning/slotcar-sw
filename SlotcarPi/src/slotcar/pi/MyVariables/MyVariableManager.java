package slotcar.pi.MyVariables;

public interface MyVariableManager {
	void MyVariableWrite(MyVariable<?> var);
	void MyVariableRead(MyVariable<?> var);
	void MyVariableWriteList(MyVariableList varlist);
	void MyVariableReadList(MyVariableList varlist);
//	void MyVariableReadWriteList(MyVariableList list2read, MyVariableList list2write);
}
