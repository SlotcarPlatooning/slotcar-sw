package slotcar.pi.MyVariables;

import java.nio.ByteBuffer;

public class MyVariableFloat extends MyVariable<Float> {

	public MyVariableFloat(int address, MyVariableManager owner) {
		super(new Float(0.0f), address, owner);
	}

	@Override
	public int getSize() {
		return 4;
	}

	@SuppressWarnings("boxing")
	@Override
	public void readFrom(ByteBuffer b) {
		set(b.getFloat());
	}

	@Override
	public void writeTo(ByteBuffer b) {
		b.putFloat(get().floatValue());
	}

}
