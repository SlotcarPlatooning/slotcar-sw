package slotcar.pi.MyVariables;

import java.nio.ByteBuffer;

public class MyVariableInt extends MyVariable<Integer> {

	public MyVariableInt(int address, MyVariableManager owner) {
		super(new Integer(0), address,  owner);
	}

	@Override
	public int getSize() {
		return 4;
	}

	@SuppressWarnings("boxing")
	@Override
	public void readFrom(ByteBuffer b) {
		set(b.getInt());
	}

	@Override
	public void writeTo(ByteBuffer b) {
		b.putInt(get().intValue());
	}

}
