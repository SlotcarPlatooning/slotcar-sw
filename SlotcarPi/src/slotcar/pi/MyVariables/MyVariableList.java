package slotcar.pi.MyVariables;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/*
 *  tato trida slouzi pro hromadnou synchronizaci promennych
 */
public class MyVariableList implements Comparator<MyVariable<?>> {

	public ArrayList<MyVariable<?>> list;

	public List<MyVariableListBlock> blocks = new ArrayList<>();

	// public ArrayList<Integer> addr = new ArrayList<>();
	// public ArrayList<Integer> length = new ArrayList<>();
	// public ArrayList<Integer> position = new ArrayList<>();

	private final MyVariableManager memManager;

	@Override
	public int compare(MyVariable<?> o1, MyVariable<?> o2) {
		if (o1.getAddress() > o2.getAddress()) {
			return 1;
		} else if (o1.getAddress() < o2.getAddress()) {
			return -1;
		} else
			return 0;
	}

	@SuppressWarnings("boxing")
	public <T> MyVariableList(MyVariableManager owner, ArrayList<MyVariable<?>> ll) {
		this.memManager = owner;

		ll.removeIf(Objects::isNull); // remove null objects

		// remove duplicates
		Set<MyVariable<?>> hs = new HashSet<>(ll);

		// copy to our list
		this.list = new ArrayList<>();
		this.list.addAll(hs);

		// sort the list from lower to higher address
		this.list.sort(this);

		// step 0 pos
		MyVariableListBlock block = new MyVariableListBlock();
		block.add(this.list.get(0));

		// step i pos
		for (int pos = 1; pos < this.list.size(); pos++) {

			MyVariable<?> var = this.list.get(pos);

			int address = var.getAddress();

			if (address - this.list.get(pos - 1).getAddress() != 1) {
				this.blocks.add(block);
				block = new MyVariableListBlock();
			}
			block.add(var);
		}

		this.blocks.add(block);

		System.out.println("Block list");
		for (MyVariableListBlock b : this.blocks) {
			System.out.print(String.format("(%d, %d, %d)\n", b.getFistAddr(), b.getLastAddr(), b.getLength()));
		}

		System.out.println();
	}

	public void downloadAll() {
		this.memManager.MyVariableReadList(this);
	}

	public void uploadALL() {
		this.memManager.MyVariableWriteList(this);
	}
	

	@Override
	public String toString() {
		StringBuilder strb = new StringBuilder();
		strb.append("size: ").append(this.list.size() * 4).append("Bytes\n");
		strb.append("size: ").append(this.blocks.size()).append("Blocks\n");
		for (MyVariable<?> var : this.list) {
			strb.append(var.toString()).append("\n");
		}
		return strb.toString();
	}
}
