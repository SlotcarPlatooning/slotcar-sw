package slotcar.pi.MyVariables;

import java.util.ArrayList;
import java.util.List;

public class MyVariableListBlock {

	private int len; // length of block in bytes

	List<MyVariable<?>> list;

	public MyVariableListBlock() {
		this.list = new ArrayList<>();
	}

	void add(MyVariable<?> var) {
		this.list.add(var);
		this.len += var.getSize();
	}

	public int getLength() {
		return this.len;
	}

	public int getFistAddr() {
		return this.list.get(0).getAddress();
	}

	public int getLastAddr() {
		return this.list.get(this.list.size() - 1).getAddress();
	}

	public List<MyVariable<?>> getList() {
		return this.list;
	}

}