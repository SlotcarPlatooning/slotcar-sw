package slotcar.pi;

import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import slotcar.CarList;
import slotcar.Config;
import slotcar.car.CarImage;
import slotcar.car.CarMeasurement;
import slotcar.pi.stm.StmMemory;

public class PiMeasurement extends CarMeasurement {
	private static PiMeasurement measurement = null;
	
	private static final Logger log = Logger.getGlobal();

	StmMemory stmMemory;

	private float distFrontCoef;
	private float distBackCoef;

	private float lastValueDistanceFrontRaw = 0;
	private float lastValueDistanceBackRaw = 0;

	private float distanceFront = 0;
	private float distanceBack = 0;
	
	PiCar owner;

	public PiMeasurement(PiCar owner) {
		this.owner = owner;
		this.stmMemory = StmMemory.getInstance();

		setDistFrontCoef(Config.getInstance().getFloat("CoefDistFront", Config.COEF_DIST_FRONT_DEFAULT));
		setDistBackCoef(Config.getInstance().getFloat("CoefDistFront", Config.COEF_DIST_BACK_DEFAULT));

		log.config("Front distance coefficient=" + getDistFrontCoef());
		log.config("Back distance coefficient=" + getDistFrontCoef());
	}

	public void setDistFrontCoef(float c) {
		distFrontCoef = c;
	}

	public void setDistBackCoef(float c) {
		distBackCoef = c;
	}

	public float getDistFrontCoef() {
		return distFrontCoef;
	}

	public float getDistBackCoef() {
		return distBackCoef;
	}

	@Override
	public float getSpeed() {
		return stmMemory.speed.get();
	}
	
	static float rawDistanceConversion(float raw, float coef){
		float d = (float) Math.sqrt(coef / raw);
		
		if( Float.isFinite(d) ) {
			if (d < Config.DISTANCE_MIN) { 
				return Config.DISTANCE_MIN;
			} else if ( d > Config.DISTANCE_MAX ){
				return Config.DISTANCE_MAX;
			}else {
				return d;
			}
		}else {
			return -1.0f;
		}
	}
	
	static boolean checkTime(long t){
		//return (System.currentTimeMillis() - t) < 200l;
		return true;
	}

	@Override
	public float getDistanceFront() {
		float raw = stmMemory.distanceFrontRaw.get();
		if (raw != lastValueDistanceFrontRaw){
			lastValueDistanceFrontRaw = raw;
			float d1 = rawDistanceConversion(raw, distFrontCoef);
			
//			CarImage c = owner.getPredecessor();
//			if(owner.isOdd() && c != null && checkTime(c.getTimeOfChange()) ){
//				float d2 = c.getCarMeasurement().getDistanceBack();
//				System.out.println("predeccessor is " + c.getLogAddr() +" " + d2);
////				distanceFront = ( d1 + d2 )/2;
//				distanceFront = d2;
//			}else{
				distanceFront = d1;
//			}
		}
		return distanceFront;
	}

	@Override
	public float getDistanceBack() {
		float raw = stmMemory.distanceBackRaw.get();
		if (raw != lastValueDistanceBackRaw){
			lastValueDistanceBackRaw = raw;
			float d1 = rawDistanceConversion(raw, distBackCoef);
			
			//CarImage c = owner.getSuccessor();
			//if(c != null && checkTime(c.getTimeOfChange()) ){
			//	float d2 = c.getCarMeasurement().getDistanceFront();
//				System.out.println("successor is " + c.getLogAddr() +" " + d2);
//				distanceBack = ( d1 + d2 ) /2;
			//	distanceBack = d2;
			//}else{
				distanceBack = d1;
			//}
		}
		return distanceBack;
	}

	@Override
	public float getAccX() {
		return stmMemory.accX.get();
	}

	@Override
	public float getAccY() {
		return stmMemory.accY.get();
	}

	@Override
	public float getAccZ() {
		return stmMemory.accZ.get();
	}

	@Override
	public float getPitchRate() {
		return stmMemory.pitchRate.get();
	}

	@Override
	public float getRollRate() {
		return stmMemory.rollRate.get();
	}

	@Override
	public float getYawRate() {
		return stmMemory.yawRate.get();
	}

	public float getDistanceFrontRaw() {
		return stmMemory.distanceFrontRaw.get();
	}

	public float getDistanceBackRaw() {
		return stmMemory.distanceBackRaw.get();
	}

	@Override
	public float getAppliedPwm() {
		return stmMemory.appliedPwm.get();
	}

	@Override
	public float getSpeedIrc() {
		return stmMemory.speedIrc.get();
	}

	@Override
	public float getSpeedBemf() {
		return stmMemory.speedBemf.get();
	}

	@Override
	public float getSpeedKalman() {
		return stmMemory.speedKalman.get();
	}

	@Override
	public float getFriction() {
		return stmMemory.friction.get();
	}
	
	@Override
	public float getMotorBemf() {
		return stmMemory.voltageBEMF.get();
	}
	
	@Override
	public float getMotorI() {
		return stmMemory.currentMotor.get();
	}
	
	@Override
	public float getRailV() {
		return stmMemory.voltageRail.get();
	}
	
	@Override
	public float getSupercapV() {
		return stmMemory.voltageSC.get();
	}
	
	@Override
	public float getDesiredI() {
		return stmMemory.desiredI.get();
	}
	
	@Override
	public float getDesiredSpeed() {
		return stmMemory.desiredSpeed.get();
	}
	

//	void update() {
//		stmMemory.outputs.downloadAll();
//	}

	@Override
	@Deprecated
	public void setSpeed(float speed) {
		log.warning("You can NOT set speed");
	}

	@Override
	@Deprecated
	public void setDistanceFront(float distance) {
		log.warning("You can NOT set front distance");
	}

	@Override
	@Deprecated
	public void setDistanceBack(float distance) {
		log.warning("You can NOT set back dinstace");
	}

	@Override
	@Deprecated
	public void setAccX(float acceleration) {
		log.warning("You can NOT set acceleration X");
	}

	@Override
	@Deprecated
	public void setAccY(float acceleration) {
		log.warning("You can NOT set acceleration Y");
	}

	@Override
	@Deprecated
	public void setAccZ(float acceleration) {
		log.warning("You can NOT set acceleration Z");
	}

	@Override
	@Deprecated
	public void setPitchRate(float rate) {
		log.warning("You can NOT set pitch rate");
	}

	@Override
	@Deprecated
	public void setRollRate(float rate) {
		log.warning("You can NOT set roll rate");
	}

	@Override
	@Deprecated
	public void setYawRate(float rate) {
		log.warning("You can NOT set measured yaw rate");
	}

	@Override
	@Deprecated
	public void setAppliedPwm(float rate) {
		log.warning("You can NOT set applied PWM. Set reference pwm in car setings instead.");
	}

	@Override
	@Deprecated
	public void setSpeedKalman(float speed) {
		log.warning("You can NOT set estimated value");
	}

	@Override
	@Deprecated
	public void setSpeedIrc(float speed) {
		log.warning("You can NOT set measured irc speed");
	}

	@Override
	@Deprecated
	public void setSpeedBemf(float speed) {
		log.warning("You can NOT set measured bemf speed");
	}

	@Override
	@Deprecated
	public void setFriction(float speed) {
		log.warning("You can NOT set estimated friction value");
	}
		
}
