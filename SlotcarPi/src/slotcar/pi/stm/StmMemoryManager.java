package slotcar.pi.stm;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.logging.Logger;

import slotcar.pi.MyVariables.MyVariable;
import slotcar.pi.MyVariables.MyVariableList;
import slotcar.pi.MyVariables.MyVariableListBlock;
import slotcar.pi.MyVariables.MyVariableManager;
import slotcar.pi.peri.LED;

class StmMemoryManager implements MyVariableManager {

	private final StmSpi spi;

	public StmMemoryManager(StmSpi spi) {
		Logger.getGlobal().info("Launching STM");
		this.spi = spi;
		Stm.reset();
	}

	@Override
	public void MyVariableWrite(MyVariable<?> var) {
		try {
			ByteBuffer bf = ByteBuffer.allocate(var.getSize());
			bf.order(ByteOrder.LITTLE_ENDIAN);
			var.writeTo(bf);
			this.spi.write(var.getAddress(), false, bf.array());
			var.setLastSyncTime();
		} catch (StmCommunicationException e) {
			ProcessLostCommunication(e);
		}
	}

	@Override
	public void MyVariableRead(MyVariable<?> var) {
		try {
			ByteBuffer bf = this.spi.read(var.getAddress(), true, var.getSize());
			bf.order(ByteOrder.LITTLE_ENDIAN);
			var.set(bf);
			var.setLastSyncTime();
		} catch (StmCommunicationException ex) {
			ProcessLostCommunication(ex);
		}
	}

	@Override
	public void MyVariableWriteList(MyVariableList varlist) {

		for (MyVariableListBlock b : varlist.blocks) {
			ByteBuffer bf = ByteBuffer.allocate(b.getLength());
			bf.order(ByteOrder.LITTLE_ENDIAN);
			for (MyVariable<?> var : b.getList()) {
				var.writeTo(bf);
			}

			try {
				byte[] array = bf.array();

				this.spi.write(b.getFistAddr(), true, array);

			} catch (StmCommunicationException e) {
				ProcessLostCommunication(e);
			}
		}
	}

	@Override
	public void MyVariableReadList(MyVariableList varlist) {
		for (MyVariableListBlock b : varlist.blocks) {
			try {
				ByteBuffer bf;
				bf = this.spi.read(b.getFistAddr(), true, b.getLength());
				if (bf == null) {
					continue;
				}

				for (MyVariable<?> var : b.getList()) {
					var.set(bf);
					var.setLastSyncTime();
				}
			} catch (StmCommunicationException ex) {
				ProcessLostCommunication(ex);
			}
		}
	}

	/**
	 * Processes lost communication
	 */
	private static void ProcessLostCommunication(StmCommunicationException ex) {
		// TODO What to do when communication is lost
		System.err.println("Communication with STM lost. The reason is: " + ex.getReason());
		LED.RED.setState(true);
	}

}

