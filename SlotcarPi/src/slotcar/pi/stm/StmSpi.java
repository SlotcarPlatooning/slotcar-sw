package slotcar.pi.stm;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pi4j.io.spi.SpiChannel;
import com.pi4j.io.spi.SpiDevice;
import com.pi4j.io.spi.SpiFactory;

public class StmSpi {
	private static final byte READ = 0x02;
	private static final byte WRITE = 0x01;
	private static final byte AUTOINC = 0x40;
	private static final byte AUTOINC_READ = AUTOINC | READ;
	private static final byte AUTOINC_WRITE = AUTOINC | WRITE;
	private static final byte SYNC = 0x16;
	private static final byte RESP_OK = 0x01;

	private static final boolean DEBUG = false;
	private static final int MAX_REPETITION = 3;

	/* Statistics */
	// read
	public static int rCount;
	public static int rFailedCount;
	public static int rFailedStmCount;
	// write
	public static int wCount;
	public static int wFailedCount;


	private static StmSpi stmSpi = null;
	private SpiDevice spi;

	private StmSpi() {
		try {
			this.spi = SpiFactory.getInstance(SpiChannel.CS1, 500000, SpiDevice.DEFAULT_SPI_MODE);
		} catch (Exception e) {
			Logger.getGlobal().log(Level.SEVERE, "STM-SPI inicialization problem", e);
			System.exit(-1);
		}
	}

	public static StmSpi getInstance() {
		if (stmSpi == null) {
			stmSpi = new StmSpi();
		}
		return stmSpi;
	}

	private static byte[] read_frame(int address, boolean autoinc, int length) {
		byte[] foo = new byte[length + 6]; // length + 7 bytes header + crc
		foo[0] = SYNC;
		foo[1] = (byte) length; // zero when reading, the read data length is
								// determined from the received packet
		foo[2] = (byte) address;
		foo[3] = autoinc ? AUTOINC_READ : READ;
		foo[4] = 0; // wait for response status
		return foo;

		// ByteBuffer foo = ByteBuffer.allocate(length + 6);
		// foo.put(SYN_BYTE);
		// foo.put((byte) length);
		// foo.put((byte) address);
		// foo.put(autoinc ? AUTOINC_READ : READ);
		// foo.put(0);
		// return foo;
	}

	private static int check_read_frame(byte[] sent, byte[] recv, int length) throws StmCommunicationException {

		if (recv == null) {
			return -3;
		}

		if (recv[4] != RESP_OK) {
			rFailedStmCount++;
			return -1;
		}

		byte lrc_rec = recv[5 + length];
		byte calc_lrc = calculateLrcRec(recv);

		if (lrc_rec != calc_lrc) {
			if (DEBUG) {
				System.out.println("CRC does not match. Calculated: " + calc_lrc + ", received: " + lrc_rec);
			}
			return -2;
		}

		return 0;
	}

	public ByteBuffer read(int address, boolean autoinc, int length) throws StmCommunicationException {
		byte[] sent = read_frame(address, autoinc, length);
		byte[] recv = new byte[1];

		int ret = 0;
		for (int i = 0; i < MAX_REPETITION; i++) {
			recv = rw(sent);

			ret = check_read_frame(sent, recv, length);

//			if (recv == null) {
//				ret = -4;
//			} else if (ret == 0) {
				
			if (ret == 0) {
				ByteBuffer bf = ByteBuffer.wrap(recv);
				bf.position(5);
				rCount++;
				return bf.slice().order(ByteOrder.LITTLE_ENDIAN);
			} 
			
			rFailedCount++;
			
			if (DEBUG) {
				System.out.println("====READ ===\n" + getBytesAsString(recv, sent));

				switch (ret) {
				case 0:
					System.out.println("COM OK");
					break;
				case -1:
					System.out.println("STM COM_ERR");
					break;
				case -2:
					System.out.println("CRC error");
					break;
				case -3:
					System.out.println("Nothing received");
					break;
				case -4:
					System.out.println("Parsing error");
					break;
				default:
					System.out.println("uknown Communication error");
					break;
				}
			}

		}

		switch (ret) {
		case -1:
			processRecError(recv, sent, "STM COM_ERR");
			break;
		case -2:
			processRecError(recv, sent, "CRC error");
			break;
		case -3:
			processRecError(recv, sent, "Nothing received");
			break;
		case -4:
			processRecError(recv, sent, "Parsing error");
			break;
		default:
			processRecError(recv, sent, "Error in communication.");
			break;
		}

		return null;
	}

	public void write(int address, boolean autoinc, byte[] data) throws StmCommunicationException {

		byte[] foo2 = create_write_frame(address, autoinc, data);
		byte[] rec_bytes = new byte[1];

		for (int i = 0; i < MAX_REPETITION; i++) {
			rec_bytes = rw(foo2);

			try {

				if (check_written_frame(rec_bytes, rec_bytes.length)) {
					wCount++;
					return;
				} else {
					wFailedCount++;
				}
				if (DEBUG)
					System.out.println("Bad response:\n" + getBytesAsString(rec_bytes, foo2));
			} catch (NullPointerException e) {
				// just repeat
			}
		}

		processRecError(rec_bytes, foo2, "Error in the response when writing");

	}

	private static void processRecError(byte[] rec_bytes, byte[] sent_bytes, String message) throws StmCommunicationException {
		String str = getBytesAsString(rec_bytes, sent_bytes);
		throw new StmCommunicationException(message + "\n" + str);
	}

	@SuppressWarnings("boxing")
	private static String getBytesAsString(byte[] rec_bytes, byte[] sent_bytes) {
		StringBuilder bl = new StringBuilder();
		bl.append("Send: ");
		for (byte b : sent_bytes) {
			bl.append(String.format("0x%02X", b & 0xFF)).append(",");
		}
		bl.append("\n");
		bl.append("Recv: ");
		for (byte b : rec_bytes) {
			bl.append(String.format("0x%02X", b & 0xFF)).append(",");
		}
		return bl.toString();
	}

	private static byte calculateLrcRec(byte[] rec_bytes) {
		byte lrc = 0;
		for (int i = 4; i < rec_bytes.length - 1; i++)
			lrc ^= rec_bytes[i];
		return lrc;
	}

	private static byte calculateLrcSend(byte[] rec_bytes) {
		byte lrc = 0;
		for (int i = 0; i < rec_bytes.length - 1; i++)
			lrc ^= rec_bytes[i];
		return lrc;
	}

	private static byte[] create_write_frame(int address, boolean autoinc, byte[] data) {
		byte[] f = new byte[data.length + 6];
		f[0] = SYNC;
		f[1] = (byte) data.length;
		f[2] = (byte) address;
		f[3] = autoinc ? AUTOINC_WRITE : WRITE;
		System.arraycopy(data, 0, f, 4, data.length);
		f[4 + data.length] = calculateLrcSend(f);
		return f;
	}

	static boolean check_written_frame(byte[] rec_bytes, int length) {
		return rec_bytes[length - 1] == RESP_OK;
	}

	public byte[] rw(byte... data) throws StmCommunicationException {
		try {
			return this.spi.write(data);
		} catch (IOException e) {
			throw new StmCommunicationException("SPI exception");
		} 
	}

	public static StringBuffer printStats() {
		StringBuffer sb = new StringBuffer();
		sb.append("reads: ").append(rCount).append("\n");
		sb.append("reads total failed: ").append(rFailedCount).append("\n");
		sb.append("reads stm failed: ").append(rFailedStmCount).append("\n");
		sb.append("writes: ").append(wCount).append("\n");
		sb.append("writes failed: ").append(wFailedCount).append("\n");
		return sb;
	}

}
