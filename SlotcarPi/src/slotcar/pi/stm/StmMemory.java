package slotcar.pi.stm;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import slotcar.Config;
import slotcar.Rolling;
import slotcar.pi.MyVariables.MyVariable;
import slotcar.pi.MyVariables.MyVariableFloat;
import slotcar.pi.MyVariables.MyVariableInfo;
import slotcar.pi.MyVariables.MyVariableInt;
import slotcar.pi.MyVariables.MyVariableList;
import slotcar.pi.MyVariables.MyVariableManager;

public class StmMemory {

	private static StmMemory stmMemory = null;
	private MyVariableManager man = new StmMemoryManager(StmSpi.getInstance());
	
	/*
	 * !!! CHECK ADDRESSES CAREFULLY 
	 */
	
	/* MEASUREMENTS */
	@MyVariableInfo(label="Speed", description = "", unit = "m/s")
	public final MyVariableFloat speed = new MyVariableFloat(0, this.man);
	
	@MyVariableInfo(label="Front Distance", description = "", unit = "m")
	public final MyVariableFloat distanceFront = new MyVariableFloat(1, this.man);
	
	@MyVariableInfo(label="Rear Distance", description = "", unit = "m")
	public final MyVariableFloat distanceBack = new MyVariableFloat(2, this.man);
	
	@MyVariableInfo(label="Acceleration X", description = "", unit = "m/s^2")
	public final MyVariableFloat accX = new MyVariableFloat(3, this.man);
	
	@MyVariableInfo(label="Acceleration Y", description = "", unit = "m/s^2")
	public final MyVariableFloat accY = new MyVariableFloat(4, this.man);
	
	@MyVariableInfo(label="Acceleration Z", description = "", unit = "m/s^2")
	public final MyVariableFloat accZ = new MyVariableFloat(5, this.man);
	
	@MyVariableInfo(label="Gyro pitch rate", description = "", unit = "rad/s")
	public final MyVariableFloat pitchRate = new MyVariableFloat(6, this.man);
	
	@MyVariableInfo(label="Gyro roll rate", description = "", unit = "rad/s")
	public final MyVariableFloat rollRate = new MyVariableFloat(7, this.man);
	
	@MyVariableInfo(label="Gyro yaw rate", description = "", unit = "rad/s")
	public final MyVariableFloat yawRate = new MyVariableFloat(8, this.man);
	
	@MyVariableInfo(label="Motor el. current", description = "", unit = "A")
	public final MyVariableFloat currentMotor = new MyVariableFloat(9, this.man);
	
	@MyVariableInfo(label="Motor Back EMF", description = "", unit = "V")
	public final MyVariableFloat voltageBEMF = new MyVariableFloat(10, this.man);
	
	@MyVariableInfo(label="Rail voltage", description = "", unit = "V")
	public final MyVariableFloat voltageRail = new MyVariableFloat(11, this.man);
	
	@MyVariableInfo(label="Supercapacitor voltage", description = "", unit = "V")
	public final MyVariableFloat voltageSC = new MyVariableFloat(12, this.man);
	
	@MyVariableInfo(label="Front distance RAW", description = "", unit = "-")
	public final MyVariableFloat distanceFrontRaw = new MyVariableFloat(13, this.man);
	
	@MyVariableInfo(label="Rear distance RAW", description = "", unit = "-")
	public final MyVariableFloat distanceBackRaw = new MyVariableFloat(14, this.man);
	
	@MyVariableInfo(label="Speed (BEMF)", description = "", unit = "m/s")
	public final MyVariableFloat speedBemf = new MyVariableFloat(15, this.man);
	
	@MyVariableInfo(label="Speed (IRC)", description = "", unit = "m/s")
	public final MyVariableFloat speedIrc = new MyVariableFloat(16, this.man);
	
	@MyVariableInfo(label="Speed (kalman)", description = "", unit = "m/s")
	public final MyVariableFloat speedKalman = new MyVariableFloat(17, this.man);
	
	@MyVariableInfo(label="Estimated friction", description = "", unit = "-")
	public final MyVariableFloat friction = new MyVariableFloat(18, this.man);

	@MyVariableInfo(label="Applied PWM", description = "", unit = "%")
	public final MyVariableFloat appliedPwm = new MyVariableFloat(19, this.man);
	
	@MyVariableInfo(label="Desired motor el. current", description = "", unit = "A")
	public final MyVariableFloat desiredI = new MyVariableFloat(20, this.man);
	
	@MyVariableInfo(label="Desired speed", description = "", unit = "m/s")
	public final MyVariableFloat desiredSpeed = new MyVariableFloat(21, this.man);
	
	/* CONTROLS */
	@MyVariableInfo(label="Reference PWM", description = "", unit = "%")
	public final MyVariableFloat referencePWM = new MyVariableFloat(30, this.man);
	
	@MyVariableInfo(label="Reference speed", description = "", unit = "m/s")
	public final MyVariableFloat referenceSpeed = new MyVariableFloat(31, this.man);
	
	@MyVariableInfo(label="Reference distance", description = "", unit = "m")
	public final MyVariableFloat referenceDistance = new MyVariableFloat(32, this.man);
	
	@MyVariableInfo(label="Regulator output type", description = "", unit = "")
	public final MyVariableInt reg = new MyVariableInt(33, this.man);
	
	@MyVariableInfo(label="Rear distance sensor enable", description = "", unit = "%")
	public final MyVariableInt distBackTransmitEn = new MyVariableInt(34, this.man);
	
	@MyVariableInfo(label="Who is", description = "", unit = "-")
	public final MyVariableInt whois = new MyVariableInt(35, this.man);

	public MyVariableList inputs;
	public MyVariableList outputs;

	public volatile Rolling lenghtOfUpdate = new Rolling(100); // tells how much time process took
	public volatile Rolling period = new Rolling(100);
	private long timestamp;
	
	private ScheduledExecutorService periodicUpdate = Executors.newSingleThreadScheduledExecutor();

	public static StmMemory getInstance() {
		if (stmMemory == null) {
			stmMemory = new StmMemory();
		}
		return stmMemory;
	}
		
	private StmMemory() {
		ArrayList<MyVariable<?>> inputList = new ArrayList<>();
		ArrayList<MyVariable<?>> outputList = new ArrayList<>();
		
		// TODO Sanity check: each variable should have unique address.
		// Setting info for each MyVariable
		for (Field f : this.getClass().getFields()) {
			MyVariableInfo info = f.getAnnotation(MyVariableInfo.class);
			if(info != null){
				f.setAccessible(true);
				Object x;
				try {
					x = f.get(this);
					if(x instanceof MyVariable<?>){
						((MyVariable<?>)x).setInfo(info);
					}
				} catch (IllegalArgumentException | IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}
		
		outputList.add(this.speed);
		outputList.add(this.distanceFront);
		outputList.add(this.distanceBack);
		outputList.add(this.accX);
		outputList.add(this.accY);
		outputList.add(this.accZ);
		outputList.add(this.pitchRate);
		outputList.add(this.rollRate);
		outputList.add(this.yawRate);
		outputList.add(this.currentMotor);
		outputList.add(this.voltageBEMF);
		outputList.add(this.voltageRail);
		outputList.add(this.voltageSC);
		outputList.add(this.distanceFrontRaw);
		outputList.add(this.distanceBackRaw);
		outputList.add(this.speedBemf);
		outputList.add(this.speedIrc);
		outputList.add(this.speedKalman);
		outputList.add(this.friction);
		outputList.add(this.appliedPwm);
		outputList.add(this.desiredI);
		outputList.add(this.desiredSpeed);
		
		inputList.add(this.referencePWM);
		inputList.add(this.referenceSpeed);
		inputList.add(this.referenceDistance);
		inputList.add(this.referenceDistance);
		inputList.add(this.referenceDistance);
		inputList.add(this.reg);
		inputList.add(this.distBackTransmitEn);

		this.outputs = new MyVariableList(this.man, outputList);
		this.inputs = new MyVariableList(this.man, inputList);
		
		if (Config.STM_ENABLE) {
			this.periodicUpdate.scheduleAtFixedRate(() -> {
				updateStep();
			}, 0, Config.SPI_MEMORY_PERIOD, Config.SPI_MEMORY_PERIOD_UNIT);
		}else{
			Logger.getGlobal().warning("STM IS DISABLED");
		}
	}

	private void updateStep() {
		long foo = System.currentTimeMillis();
		this.period.add((double)foo - this.timestamp);
		this.timestamp = foo;
		stmMemory.inputs.uploadALL();
		stmMemory.outputs.downloadAll();
		this.lenghtOfUpdate.add(System.currentTimeMillis() - foo);
	}
}