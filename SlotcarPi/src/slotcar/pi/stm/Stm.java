package slotcar.pi.stm;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RCMPin;
import com.pi4j.io.gpio.RaspiGpioProvider;
import com.pi4j.io.gpio.RaspiPinNumberingScheme;

public class Stm {

	static {
		GpioFactory.setDefaultProvider(new RaspiGpioProvider(RaspiPinNumberingScheme.BROADCOM_PIN_NUMBERING));
	}
	
	private static final GpioController PI_GPIO = GpioFactory.getInstance();

	private static Pin RESET_PIN = RCMPin.GPIO_00;
	private static Pin BOOT_PIN = RCMPin.GPIO_37;
	
	private static final GpioPinDigitalOutput RESET = PI_GPIO.provisionDigitalOutputPin(RESET_PIN, PinState.HIGH);
	private static final GpioPinDigitalOutput BOOT = PI_GPIO.provisionDigitalOutputPin(BOOT_PIN, PinState.LOW);

	public static void reset() {
		BOOT.setState(PinState.LOW);
		RESET.setState(PinState.LOW);
		RESET.setState(PinState.HIGH);
	}
}
