package slotcar.pi.stm;

public class StmCommunicationException extends Exception {

	private static final long serialVersionUID = -1168198242467004347L;

	private final String reason;
	
	public StmCommunicationException(String reason) {
		this.reason = reason;
	}
	
	public String getReason() {
		return this.reason;
	}
}

