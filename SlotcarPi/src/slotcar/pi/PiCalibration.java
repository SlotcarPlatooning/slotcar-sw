package slotcar.pi;

import slotcar.car.Calibration;

/**
 * Implementation of Calibration for PiCar
 * @see Calibration 
 */
public class PiCalibration extends Calibration {
	private final PiMeasurement m;

	public PiCalibration(PiCar car) {
		super(car);
		m = car.getPiMeasurement();
	}

	public void calibrateFront(float ref) {
		float c = m.getDistanceFrontRaw() * ref * ref;
		System.out.println("Front distance coef = " + c);
		m.setDistFrontCoef(c);		
	}

	public void calibrateRear(float ref) {
			float c = m.getDistanceBackRaw() * ref * ref;
			System.out.println("Front distance coef = " + c);
			m.setDistBackCoef(c);
	}
}
