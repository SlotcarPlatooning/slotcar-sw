package slotcar;

import slotcar.network.NetworkAddress;

public interface RoutingTableListener {

	public void added(byte addr, NetworkAddress nAddr);
	
	public void changed(byte addr, NetworkAddress nAddr);

	public void deleted(byte addr);
	
	public void cleared();


}
