package slotcar;

import java.math.BigDecimal;

// TODO maybe convert all to BigDecimal
public class Rolling {

	public static final int MAX_SIZE = 1000;
	private final double varA;
	private final double avgA;
	private final int size;

	private int index = 0;

	private double avg = 0d;
	private double var = 0d;
	private double avgSamples[];
	private double varSamples[];
	
	private double max;
	private double min;

	private boolean expanded = false;

	public Rolling(int size) {
		if (size > MAX_SIZE) {
			System.err.println("size is big, setting size = " + MAX_SIZE);
			size = MAX_SIZE;
		}

		this.size = size;
		avgSamples = new double[this.size];
		varSamples = new double[this.size];

		varA = 1.0 / (size - 1);
		avgA = 1.0 / size;

		for (int i = 0; i < this.size; i++) {
			avgSamples[i] = 0d;
			varSamples[i] = 0d;
		}
	}

	public void add(double x) {
		
		if(x > max){
			max = x;
		}else if(x < min){
			min = x;
		}

		avg -= avgSamples[index];
		double avgfoo = x;
		avgSamples[index] = avgfoo;
		avg += avgfoo;

		if (expanded) {
			double bar = x - getAverage();
			var -= varSamples[index];
			double varfoo = varA * bar * bar;
			varSamples[index] = varfoo;
			var += varfoo;
		}
		
		index++;
		if (index == size) {
			min = Double.MAX_VALUE;
			max = Double.MIN_VALUE;
			expanded = true;
			index = 0;
		}
	}

	public double getAverage() {
		return avgA * avg;
	}

	public double getVariance() {
		return Math.sqrt(var);
	}

	@Override
	public String toString() {
		BigDecimal avg = new BigDecimal(getAverage());
		BigDecimal var = new BigDecimal(getVariance());
		
		
		BigDecimal max = new BigDecimal(this.max);
		BigDecimal min = new BigDecimal(this.min);
		
		
		// var = var.round(new MathContext(4));
		var = var.setScale(4, BigDecimal.ROUND_HALF_UP);
		avg = avg.setScale(var.scale(), BigDecimal.ROUND_HALF_UP);
		min = min.setScale(var.scale(), BigDecimal.ROUND_HALF_UP);
		max = max.setScale(var.scale(), BigDecimal.ROUND_HALF_UP);
		
		return String.format("%f � %f (%f,%f)", 
				avg.doubleValue(), 
				var.doubleValue(), 
				min.doubleValue(), 
				max.doubleValue());
	}

	/**
	 * Test of Rolling class
	 */
	public static void main(String[] args) {

		Rolling r = new Rolling(1000);

		for (long i = 1; i <= 1000; i++) {
				System.out.println(i);
				r.add((double) i);
		}
	
		// average should be closed to 0.5
		System.out.println(r);
	}
}
