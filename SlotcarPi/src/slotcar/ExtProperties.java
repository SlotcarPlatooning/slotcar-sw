package slotcar;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;
import java.util.logging.Logger;

public class ExtProperties extends Properties {

	/*
	 * Checks if key exists. if it does not exist => it creates the key in
	 * config file and save the value if it does exist => It looks if the
	 * current saved value contains new value if it does then do nothing and
	 * exit it it does not then add it at the end of string
	 */
	public void add(String key, String value) {
		String s = getProperty(key);
		if (s == null || s.isEmpty()) {
			// just set the value
			setProperty(key, value);
		} else if (!s.contains(value)) {
			// add the value at the end
			setProperty(key, s + ";" + value);
		} else {
			// do nothing
			return;
		}
	}

	@Override
	public String getProperty(String key) {
		String s = super.getProperty(key);
		if (s == null) {
			String warn = String.format("No value for key \"%s\".", key);
			Logger.getGlobal().warning(warn);
		}
		return s;
	}

	public String get(String key) {
		return getProperty(key);
	}

	public String get(String key, String defaultValue) {
		return getProperty(key, defaultValue);
	}

	public void set(String key, String value) {
		setProperty(key, value);
	}

	public Byte getByte(String key) {
		String s = getProperty(key);
		return s == null ? null : (byte) (Integer.parseInt(s) & 0xFF);
	}
	
	public Byte getByte(String key, Byte defaultValue) {
		String s = getProperty(key);
		return s == null ? defaultValue : (byte) (Integer.parseInt(s) & 0xFF);
	}

	public void setByte(String key, byte value) {
		setProperty(key, Integer.toString(value & 0xFF));
	}

	public float getFloat(String key) {
		String s = getProperty(key);
		return s == null ? null : (float) Float.parseFloat(s);
	}
	
	public float getFloat(String key, float defaultValue) {
		String s = getProperty(key);
		
		float ret; 
		if(s == null){
			ret = defaultValue;
			Logger.getGlobal().warning(key + "was not found, returning default " + defaultValue);
		}else{
			ret = Float.parseFloat(s);
		}
		
		return ret;
	}

	public File getFile(String key) {
		try {
			File f = new File(getProperty(key));
			if (f.exists() && (f.canWrite() || f.canRead())) {
				return f;
			}
		} catch (NullPointerException e) {
			Logger.getGlobal().warning("File was not found in conf file");
		}
		return null;
	}

	public void setFloat(String key, float value) {
		setProperty(key, Float.toString(value));
	}

	public void remove(String key, String value) {
		String s = getProperty(key);
		if (s == null)
			return;
		s = s.replace(value, "");
		s = s.replace(";;", ";");
		return;
	}

	public void load(File file) {
		Logger.getGlobal().info("Loading config file " + file.getAbsolutePath());
		try {
			if (!file.exists()) {
				return;
			}
			InputStream in = new FileInputStream(file);
			load(in);
			in.close();
		} catch (IOException ex) {
			ex.printStackTrace();
			// Logger.getGlobal().log(Level.SEVERE, null, ex);
		}
	}

	public void save(File file) {
		try {
			OutputStream writer = new FileOutputStream(file);
			store(writer, null);
			writer.close();
		} catch (IOException ex) {
			ex.printStackTrace();
			// Logger.getGlobal().log(Level.SEVERE, null, ex);
		}
	}
	
	public void load(String conf) {
		load(new File(conf));
	}

	public void save(String conf) {
		save(new File(conf));
	}

}
