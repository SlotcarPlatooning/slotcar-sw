package slotcar.wifi;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;

import slotcar.network.NetworkAddress;
import slotcar.network.NetworkAddressType;

/**
 * Wrapper for InetAddress
 */
public class WiFiNetworkAddress implements NetworkAddress {

	@Override
	public NetworkAddressType getType() {
		return NetworkAddressType.WIFI;
	}

	private InetAddress ip;

	public WiFiNetworkAddress(ByteBuffer b) {
		if (b.remaining() < 4) {
			return;
		}

		byte[] data = new byte[4];
		b.get(data, 0, 4);
		try {
			ip = InetAddress.getByAddress(data);
		} catch (UnknownHostException e) {
			Logger.getGlobal().log(Level.SEVERE, null, e);
		}
	}

	public WiFiNetworkAddress( InetAddress ip) {
		this.ip = ip;
	}

	public WiFiNetworkAddress(String string) throws UnknownHostException {
		this(InetAddress.getByName(string));
	}

	public InetAddress getIp() {
		return ip;
	}

	@Override
	public byte[] toBytes() {
		return ip.getAddress();
	}
	
	@Override
	public String toString() {
		return ip.getHostAddress();
	}

}
