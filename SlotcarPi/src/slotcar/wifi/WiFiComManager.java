package slotcar.wifi;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import slotcar.network.ComManager;
import slotcar.network.NetworkAddress;
import slotcar.network.NewDataEvent;
import slotcar.network.NewDataEventListener;
import slotcar.network.TypeMismatchNetworkAddressException;

/**
 * <H1>Wi-Fi Communication Manager</H1> This manager sends and receives packets
 * on UDP. It also allows you to send acknowledged packets. Acknowledgement is
 * done manually by this class.
 * <p>
 * Output messages are proceeded via a output queue, just for thread safe
 * behavior. Input messages are proceeded directly, because the OS has a system
 * queue of packets. Since the packets are UDP the maximum length of a packet is
 * fixed to 576 bytes.
 * <p>
 * TODO TCP-IP should be used and destination port should not be fixed as it is
 * now. Remove output queue.
 */
public class WiFiComManager extends ComManager {

	private static Logger log = Logger.getLogger(WiFiComManager.class.getName());

	private static final int UDP_BUFFER_LENGTH_MAX = 576; // [Bytes] fixed
	private static final short TTL = 10;
	private static final long RESENDING_PERIOD = 100l;
	private static final TimeUnit RESENDING_PERIOD_UNIT = TimeUnit.MILLISECONDS;

	// Parameters
	private final int sPort; // source port
	private final int dPort; // destination port
	private InetAddress ip;
	private InetAddress broadcastIP;
	private DatagramSocket udpSocket;

	private byte[] receivedData = new byte[UDP_BUFFER_LENGTH_MAX];
	private DatagramPacket packet = new DatagramPacket(receivedData, receivedData.length);

	// Variables

	// Packet index. Incremented with every submitted new message.
	private AtomicInteger packetIndex = new AtomicInteger(0);
	private BlockingQueue<WiFiFrame> outputQueue; // Submitted packets that are
													// going to be send.
	private Set<WiFiFrame> waitingACK; // Packets that are waiting for
										// acknowledge.
	// When a new packet is received it is given to these listeners.
	private List<NewDataEventListener> listeners = new ArrayList<>();
	// Executors, for receiving and sending.
	private ExecutorService executorService = Executors.newFixedThreadPool(3, new ThreadFactory() {
		@Override
		public Thread newThread(Runnable r) {
			Thread t = new Thread(r);
			t.setPriority(Thread.MAX_PRIORITY); // max priority for faster
												// reaction.
			return t;
		}
	});

	// For resending packets, that were not acknowledged.
	private ScheduledExecutorService timerExecutorService = Executors.newSingleThreadScheduledExecutor();

	private NetworkInterface iface;

	public WiFiComManager(NetworkInterface iface, int sPort, int dPort) {
		log.setLevel(Level.FINE);
		this.sPort = sPort;
		this.dPort = dPort;
		this.iface = iface;
		outputQueue = new LinkedBlockingDeque<>();
		waitingACK = new HashSet<>();
	}

	/**
	 * Determines IP and broadcast IP. Opens sockets for communication and
	 * creates receiver and sender threads.
	 */
	@Override
	public boolean open() {

		if (isConnected()) {
			log.warning("Is already open.");
			return true;
		}

		this.ip = getmyIP(iface);
		if (ip == null) {
			log.severe("Cannot get IP address.");
			return false; // TODO Return exception.
		}

		try {
			byte[] temp = ip.getAddress();
			temp[3] = (byte) 255;
			broadcastIP = InetAddress.getByAddress(temp);
		} catch (UnknownHostException ex) {
			log.log(Level.SEVERE, "Bad Network", ex);
		}

		if (broadcastIP == null) {
			log.severe("Cannot get broadcast IP address.");
			return false;
		}
		
		try {
			udpSocket = new DatagramSocket(sPort);
			udpSocket.setBroadcast(true);
			udpSocket.setReceiveBufferSize(0x80000);
		} catch (SocketException ex) {
			log.log(Level.SEVERE, "Socket might be in use.", ex);
			return false;
		}

		// UDP WRITE
		executorService.submit(() -> {
			while (true) {
				udpSend();
			}
		});

		// UDP READ
		executorService.submit(() ->{
			while (true) {
				udpRead();
			}
		});

		timerExecutorService.scheduleAtFixedRate( () -> {
				resend();
		}, 0, RESENDING_PERIOD, RESENDING_PERIOD_UNIT);

		// TODO: HACK...if you don't send on broadcast for a long time, the broadcast receptions also blocks
		executorService.submit(new Runnable() {
			@Override
			public void run() {
				while(true){
					WiFiComManager.this.send(new byte[0]);	
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});
		

Logger.getGlobal().warning("Connection opened");
		return true;
	}

	/**
	 * Adds listener for received data.
	 */
	public void addNewDataEventListener(NewDataEventListener listener) {
		listeners.add(listener);
	}

	/**
	 * Sends a UDP message from output queue. If queue is empty, blocks until a
	 * message is available.
	 */
	private void udpSend() {
		try {
			WiFiFrame f = outputQueue.take();

			// // logging
			// String s = String.format("sending Wi-Fi packet, index: %d, len: %d, addr=%s", f.index,f.length(), f.ip.getHostAddress());
			// Logger.getGlobal().fine(s);

			udpSocket.send(f.newPacket());
		} catch (IOException | InterruptedException ex) {
			Logger.getGlobal().log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * Receives a UDP message. Blocks until a message is received and handled by
	 * listeners.
	 */
	private void udpRead() {
		try {
			udpSocket.receive(packet);
		//	String s1 = String.format("received Wi-Fi packet  from %s ", packet.getAddress());
		//	 log.fine(s1);
			if (!packet.getAddress().equals(this.ip)) {
				// ByteBuffer bf = ByteBuffer.wrap(packet.getData());
				// bf.limit(packet.getLength());
				byte[] newFrame = Arrays.copyOfRange(packet.getData(), 0, packet.getLength());
				WiFiFrame f = new WiFiFrame(newFrame, packet.getAddress(), packet.getPort());

			//	String s = String.format("received Wi-Fi packet, index: %d, len: %d", f.index,f.length());
			//	 log.fine(s);

		//		 System.out.println(s);
				handleNewWifiFrame(f);
				// Reset the length of the packet before reusing it.
				packet.setLength(receivedData.length);
			}
		} catch (IOException e) {
			Logger.getGlobal().log(Level.SEVERE, null, e);
		}
	}

	/**
	 * Checks every packet that is waiting for acknowledgement. If the
	 * acknowledgement is timeout it resends the message.
	 */
	public void resend() {
		if (isConnected()) {
			for (WiFiFrame f : waitingACK) {
				f.hurt();
				if (f.getTTL() == 0) {// if there was no ack in 1 second
										// (10x100ms)
					waitingACK.remove(f);
					f.incResent();// change resent index

					if (f.getResend() < 3) {// if packet was resent less than 3
											// times
						f.setTTL(TTL);// reset TTL
						waitingACK.add(f);
						outputQueue.add(f);
					} // else do not send again
				}
			}
		}
	}

	/**
	 * Sends UDP message to NTWKADDR with ack.
	 */
	@Override
	public void send(NetworkAddress ntwkAddr, byte[] data) throws TypeMismatchNetworkAddressException {
		assert data.length < UDP_BUFFER_LENGTH_MAX;
		if (ntwkAddr instanceof WiFiNetworkAddress) {
			InetAddress ip = ((WiFiNetworkAddress) ntwkAddr).getIp();
			WiFiFrame packet = new WiFiFrame(data, (short) packetIndex.getAndIncrement(), TTL, true, ip, dPort);
			waitingACK.add(packet);
			outputQueue.add(packet);
		} else {
			throw new TypeMismatchNetworkAddressException();
		}
	}

	/**
	 * Sends UDP message to broadcast (no ack).
	 */
	@Override
	public void send(byte[] data) {
		assert data.length < UDP_BUFFER_LENGTH_MAX;
		outputQueue.add(new WiFiFrame(data, (short) packetIndex.getAndIncrement(), 0, false, broadcastIP, dPort));
	}

	/**
	 * The Wi-Fi frame is passed to all listeners.
	 */
	public void handleNewWifiFrame(WiFiFrame f) {

		if (f.length() == 3) {
			// it is ack
			if (!f.isAckNeeded()) {
				waitingACK.remove(f);
			}
		} else {
			// it is data
			if (f.isAckNeeded()) {
				outputQueue.add(f.createACK());
			}

			try {
				NewDataEvent event = f.createDataEvent(this);
				// log.fine("Handling message " + event);
				for (NewDataEventListener listener : listeners) {
					listener.handleNewDataEvent(event);
				}
			} catch (Exception ex) {
				Logger.getGlobal().log(Level.SEVERE, "Handling message failed", ex);

			}
		}
	}

	/**
	 * Returns network address.
	 * 
	 * @return
	 */
	@Override
	public NetworkAddress getNetAddr() {
		return new WiFiNetworkAddress(ip);

	}

	/**
	 *
	 * @return
	 * @throws Exception
	 */
	public static InetAddress getmyIP(NetworkInterface iface) {
		try {

			String result = "";

			Enumeration<InetAddress> addresses = iface.getInetAddresses();

			while (addresses.hasMoreElements() && (result == null || result.isEmpty())) {
				InetAddress address = addresses.nextElement();
				if (!address.isLoopbackAddress() && address.isSiteLocalAddress()) {
					result = address.getHostAddress();
				}

			}

			if (result.equals("127.0.0.1") || result.equals("")) {
				JOptionPane.showMessageDialog(null, "No network address", "Network error", JOptionPane.ERROR_MESSAGE);
				throw new Exception();
			}
			return InetAddress.getByName(result);
		} catch (Exception ex) {
			return null;
		}

	}

	public static NetworkInterface selectNetworkInterface() {
		try {
			ArrayList<NetworkInterface> ifacesList = new ArrayList<>(10);

			for (Enumeration<NetworkInterface> e = NetworkInterface.getNetworkInterfaces(); e.hasMoreElements();) {
				NetworkInterface iface = e.nextElement();
				if (iface.isUp()) {
					ifacesList.add(iface);
				}
			}

			NetworkInterface[] ifaces = ifacesList.toArray(new NetworkInterface[0]);

			if (ifaces.length == 0) {
				return null;
			}

			int wlanIndex = 0;

			for (int i = 0; i < ifaces.length; i++) {
				if (ifaces[i].getName().contains("wlan")) {
					wlanIndex = i;
					break;
				}
			}
			NetworkInterface selectedIface = (NetworkInterface) JOptionPane.showInputDialog(null,
					"Select Network Interface", "Interface Selection", JOptionPane.INFORMATION_MESSAGE, null, ifaces,
					ifaces[wlanIndex]);

			return selectedIface;
		} catch (SocketException ex) {
			return null;
		}
	}

	@Override
	public void close() {
		executorService.shutdown();
		timerExecutorService.shutdown();
		udpSocket.close();
	}

	@Override
	public boolean isConnected() {
		return (udpSocket != null && !udpSocket.isClosed());
	}

}

class InvalidNetworkAddressTypeException extends Exception {

	private static final long serialVersionUID = 2546775709088358951L;

	public InvalidNetworkAddressTypeException() {
		super("Type of Address does not match");
	}
}
