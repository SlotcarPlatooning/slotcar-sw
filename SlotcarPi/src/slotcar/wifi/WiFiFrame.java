package slotcar.wifi;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.nio.ByteBuffer;

import slotcar.network.NetworkAddress;
import slotcar.network.NewDataEvent;

public class WiFiFrame{

//	private final byte[] raw; // raw bytes
	private ByteBuffer raw;

	public final InetAddress ip;
	public final int port;
	public final short index;
	private boolean ackNeeded;

	private int TTL; // Time To Live

	private int resend = 0;

	// 2B = Packet ID, 1 B = ACK, N Bytes = DATA |

	public WiFiFrame(byte[] data, short index, int TTL, boolean ackNeeded, InetAddress ip, int port) {
		if (data == null) {
			this.raw = ByteBuffer.wrap(new byte[3]);
		} else {
			this.raw = ByteBuffer.wrap(new byte[3 + data.length]);
			raw.position(3);
			raw.put(data);
		}
	
		raw.position(0);
		// setting index
		this.index = index;
		raw.putShort(index);
		
		
		// setting ack
		this.ackNeeded = ackNeeded;
		raw.put(ackNeeded ? (byte) 1 : (byte) 0);

		this.TTL = TTL;
		this.ip = ip;
		
		this.port = port;
	}
	
	public WiFiFrame(byte[] raw, InetAddress ip, int port) {
		this.raw = ByteBuffer.wrap(raw);

		// setting index
		this.index = this.raw.getShort();

		// setting ackNeeded
		this.ackNeeded = (this.raw.get() == 1);

		this.ip = ip;
		this.port = port;
	}
	
	
	public WiFiFrame createACK(){
		return new WiFiFrame(null, index, TTL, false, ip, port);
	}
	
	public NewDataEvent createDataEvent(Object source){
		long time = System.currentTimeMillis(); 
		NetworkAddress addr = new WiFiNetworkAddress(ip);
		raw.position(3);
		return new NewDataEvent(source, raw.slice(), time, addr);
	}

	public int length() {
		return this.raw.capacity();
	}

	public boolean isAckNeeded() {
		return ackNeeded;
	}

	public int getTTL() {
		return TTL;
	}

	public void setTTL(int TTL) {
		this.TTL = TTL;
	}

	public void hurt() {
		TTL--;
	}

	public int getResend() {
		return resend;
	}

	public void setResend(int resend) {
		this.resend = resend;
	}

	public void incResent() {
		resend++;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 19 * hash + this.index;
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final WiFiFrame other = (WiFiFrame) obj;
		if (this.index != other.index) {
			return false;
		}
		return true;
	}
	
	DatagramPacket newPacket(){
		return new DatagramPacket(raw.array(), length(), ip, port); 
	}
}
