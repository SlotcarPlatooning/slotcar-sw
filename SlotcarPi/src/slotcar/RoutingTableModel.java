package slotcar;

import java.net.InetAddress;
import java.util.ArrayList;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import slotcar.network.NetworkAddress;
import slotcar.wifi.WiFiNetworkAddress;

class RoutingTableItem {
	public static final String[] HEADER = { "Addr", "Network","Status" };
	public static final Class<?>[] TYPES = { Byte.class, InetAddress.class,String.class };
	public static final boolean[] EDITABLE = { false, false, false};

	private String status = "-";
	private final Byte addr;
	private final InetAddress ip;

	public RoutingTableItem(InetAddress ip, Byte addr) {
		this.ip = ip;
		this.addr = addr;
	}
	
	public void setValueAt(int columnIndex, Object aValue) { 
		return;
	}

	public Byte getAddr() {
		return addr;
	}

	public InetAddress getIp() {
		return ip;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof RoutingTableItem))
			return false;

		RoutingTableItem o = (RoutingTableItem) obj;
		if (o.addr == this.addr) {
			return true;
		}
		return false;
	}

	public Object getValueAt(int columnIndex) {
		switch (columnIndex) {
		case 0:
			return getAddr();
		case 1:
			return getIp().getHostAddress();
		case 2:
			return getStatus();
		}
		return null;
	}

	
}

public class RoutingTableModel implements TableModel {

	ArrayList<RoutingTableItem> data = new ArrayList<>();
	

	public int getRow(Byte addr){
		for (int i = 0; i < data.size(); i ++) {
			if(data.get(i).getAddr() == addr){
				return i;
			}
		}
		return -1;
	}

	ArrayList<TableModelListener> listeners = new ArrayList<>();

	void dataChanged() {
		ArrayList<RoutingTableItem> shadow = new ArrayList<>();
		for (Byte b : RoutingTable.getList().keySet()) {
			if(b == Config.ADDR_SERVER){
				continue;
			}
			NetworkAddress nAddr = RoutingTable.get(b);
			if (nAddr instanceof WiFiNetworkAddress) {
				WiFiNetworkAddress wAddress = (WiFiNetworkAddress) nAddr;
				shadow.add(new RoutingTableItem(wAddress.getIp(), b));
			}
		}
		
		for (RoutingTableItem item : shadow) {
			int i = data.indexOf(item);
			if (i >= 0) {
				RoutingTableItem foo = data.get(i);
				item.setStatus(foo.getStatus());
			}
		}
		
		data.clear();
		for (RoutingTableItem item : shadow) {
			data.add(item);
		}

		for (TableModelListener l : listeners) {
			l.tableChanged(new TableModelEvent(this));
		}
	}

	@Override
	public void addTableModelListener(TableModelListener l) {
		listeners.add(l);
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return RoutingTableItem.TYPES[columnIndex];
	}

	@Override
	public int getColumnCount() {
		return RoutingTableItem.HEADER.length;
	}

	@Override
	public String getColumnName(int columnIndex) {
		return RoutingTableItem.HEADER[columnIndex];
	}

	@Override
	public int getRowCount() {
		return data.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		return data.get(rowIndex).getValueAt(columnIndex);
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return RoutingTableItem.EDITABLE[columnIndex];
	}

	@Override
	public void removeTableModelListener(TableModelListener l) {
		listeners.remove(l);
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		data.get(rowIndex).setValueAt(columnIndex, aValue);
	}

	public Byte getAddrAt(int rowIndex) {
		return data.get(rowIndex).getAddr();
	}
	public InetAddress getIpAt(int rowIndex) {
		return data.get(rowIndex).getIp();
	}

}
