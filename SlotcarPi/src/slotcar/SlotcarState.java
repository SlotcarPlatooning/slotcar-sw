package slotcar;

public enum SlotcarState {

	UNKNOWN(-1),
	OFFLINE(0),
	READY(1), 
	EXPERIMENT(2), 
	CALIBRATION(3),  
	POSITIONING(4),
	MANUAL(5);
	
	private final int val; 
	
	private SlotcarState(int val) {
		this.val = val;
	}
	
	public int getVal() {
		return val;
	}
	
	public static SlotcarState getByVal(int val){
		for (SlotcarState type: SlotcarState.values()) {
			if(type.val == val) return type;
		}
		return UNKNOWN;
	}
}
