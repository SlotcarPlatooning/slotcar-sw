package slotcar;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

public class Debugging {

	public static void initCAR() {
		try {
			// System.setProperty("java.util.logging.SimpleFormatter.format",
			// "%1$tY-%1$tm-%1$td %1$tH:%1$tM:%1$tS %4$-6s %2$s %5$s%6$s%n");

			// SimpleFormatter.format=
			FileHandler fh = new FileHandler(Config.getLogFile());
			fh.setFormatter(new MyFormatter());
			Logger.getGlobal().addHandler(fh);
			Logger.getGlobal().setLevel(Level.FINEST);
			Logger.getGlobal().setUseParentHandlers(false);
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		

		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			public void run() {
				for (Handler h : Logger.getGlobal().getHandlers()) {
					h.close();
				}
			}
		}));
	}

	public static void logEverything(Level level) {
	    // Get the logger instance
//	    LogManager logManager = LogManager.getLogManager();
	    Logger rootLogger = Logger.getGlobal();
	

	    // Set the default logging level to all
	    rootLogger.setLevel(level);

        
//        rootLogger.setFormatter(new MyFormatter());

	    // Loop and see if any console handlers are already installed
	    List<ConsoleHandler> consoleHandlers = new ArrayList<ConsoleHandler>();

	    for (Handler handler : rootLogger.getHandlers()) {
	        if (handler instanceof ConsoleHandler) {
	            consoleHandlers.add((ConsoleHandler) handler);
	        }
	    }

	    // Is a console handler already installed?
	    if (consoleHandlers.size() == 0) {
	        // No, create one.  Add it to the list and to the root logger.
	        Handler consoleHandler = new ConsoleHandler();
	        consoleHandlers.add((ConsoleHandler) consoleHandler);
	        rootLogger.addHandler(consoleHandler);
	    }

	    // Loop through all console handlers and make them log everything
	    for (ConsoleHandler consoleHandler : consoleHandlers) {
	        consoleHandler.setLevel(level);
	        consoleHandler.setFormatter(new MyFormatter());
	    }
	}
			
	public static void initGUI() {
		System.setProperty("java.util.logging.SimpleFormatter.format",
				"%1$tY-%1$tm-%1$td %1$tH:%1$tM:%1$tS %4$-6s %2$s %5$s%6$s%n");

		//System.setProperty("java.util.logging.ConsoleHandler.level","FINE");
		
		logEverything(Level.FINEST);
		
//		//Logger.getGlobal().setLevel(Level.ALL);
//		System.out.println(Logger.getGlobal().getLevel());
////		System.out.println("SHOW INFO");
//		Logger.getGlobal().info("Level INFO");
////		System.out.println("SHOW WARNING");
//		Logger.getGlobal().warning("Level WARNING");
////		System.out.println("SHOW SEVERE");
//		Logger.getGlobal().severe("Level SEVERE");
////		System.out.println("SHOW CONFIG");
//		Logger.getGlobal().config("Level CONFIG");
////		System.out.println("SHOW FINE");
//		Logger.getGlobal().fine("Level FINE");
////		System.out.println("SHOW FINER " + Logger.getGlobal().isLoggable(Level.FINER));
//		Logger.getGlobal().finer("Level FINER");
////		System.out.println("SHOW FINEST");
//		Logger.getGlobal().finest("Level FINEST");
	}
	
	public static String getSystemTime(){
		Date date = new Date(System.currentTimeMillis());
		DateFormat formatter = new SimpleDateFormat("HH:mm:ss:SSS");
		return formatter.format(date);
	}
	
	public static String toHex(byte...bytes) {
		StringBuilder sb = new StringBuilder(2*bytes.length);

		for (byte b : bytes) {
			int h = (b & 0xF0);
			int l = (b & 0x0F);
			if(h < 10){
				sb.append(h);	
			}else{
				Character.toChars(65+h-10); 
			}
			if(l < 10){
				sb.append(l);	
			}else{
				Character.toChars(65+l-10); 
			}
		}
		return sb.toString();

	}

}



class MyFormatter extends Formatter {
	//
	// Create a DateFormat to format the logger timestamp.
	//
	private static final DateFormat df = new SimpleDateFormat("hh:mm:ss.SSS");

	public String format(LogRecord record) {
		StringBuilder builder = new StringBuilder(1000);
		builder.append("[").append(record.getLevel()).append("]");
		
		builder.append("[").append(df.format(new Date(record.getMillis()))).append("]");

		builder.append(formatMessage(record));
		builder.append("[");
		builder.append(record.getSourceClassName()).append(".");
		builder.append(record.getSourceMethodName());
		builder.append("]");

		builder.append("\n");
		return builder.toString();
	}

	public String getHead(Handler h) {
		return super.getHead(h);
	}

	public String getTail(Handler h) {
		return super.getTail(h);
	}
	
	
}