package slotcar;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import slotcar.car.CarImage;
import slotcar.pi.controller.CarImageInterface;
import slotcar.pi.controller.CarListInterface;

/**
 * Represents a list containing images of real cars.
 * It is used as a input for @see Controller as @see CarListInterface.
 */
public class CarList implements CarListInterface {
	
	private static final Logger log = Logger.getLogger(CarList.class.getName());

	private static CarList carlist; // singleton

	private final List<CarImage> list = new ArrayList<>();
	private final Map<Byte, CarImage> map = new HashMap<Byte,CarImage>();

	private CarList() {
		// constructor is private, because of singleton
	}

	public static CarList getInstance() {
		if (carlist == null) {
			carlist = new CarList();
		}
		return carlist;
	}

	/**
	 * @return Car on defined position if it exists.
	 * @return Null, when CarList is empty.
	 * 
	 *         if position is greater than the size of the list then it is taken
	 *         modulo e.g. when getting 5. out of 3 cars you get 5%3 = 2.
	 */
	@Override
	public CarImageInterface getByPos(int position) {
		if (list.size() == 0)
			return null;
		// scaling the input position to our scale
		position = position % list.size();
		return list.get((position >= 0) ? position : position + list.size());
	}

	@Override
	public CarImageInterface getByAddr(byte addr) {
		return get(addr);
	}

	public CarImage get(byte addr) {
		return map.get(addr);
	}
	
	public CarImage getOrNew(byte addr) {
		addCar(addr);
		return get(addr);
	}

//  //Don't use, unspecified behavior
//	public void setPosition(byte addr, int newPosition) {
//		CarImage c = get(addr);
//		list.remove(c);
//		list.add(newPosition, c);
//		c.setPosition(newPosition);
//		log.fine(String.format("New position of car %d is %d.", addr,newPosition));
//	}

	public void swapPositions(int pos1, int pos2) {
		list.get(pos1).setPosition(pos2);
		list.get(pos2).setPosition(pos1);
		Collections.swap(list, pos1, pos2);
		log.fine(String.format("Cars on positions %d and %d were swapped.", pos1, pos2));
	}

	@Override
	public int size() {
		return list.size();
	}

	public byte[] getAddresses() {
		ByteBuffer bf = ByteBuffer.allocate(list.size());
		for (CarImage car : list) {
			bf.put(car.getLogAddr());
		}
		return bf.array();
	}

	public List<CarImage> getCars() {
		return list;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("CarList{");
		for (CarImage carImage : list) {
			sb.append(carImage.getLogAddr());
			sb.append(",");
		}
		sb.replace(sb.length(), sb.length(), "}");
		return sb.toString();
	}

	/**
	 * Synchronize online cars and their positions.
	 * If a car does not exists on the list it is created.
	 * 
	 * @param addresses Car's addresses sorted by position
	 */
	public void syncPositions(byte[] addresses) {
		try {
			List<CarImage> shadow = new ArrayList<>(list.size());
			
			for (byte addr : addresses) {
				shadow.add(getOrNew(addr));
			}

			list.clear();

			int position = 0;
			for (CarImage c : shadow) {
				list.add(c);
				c.setPosition(position++);
			}

			log.finer(String.format("New positions are %s.", toString()));

		} catch (Exception ex) {
			log.log(Level.SEVERE, ex.toString(), ex);
		}
	}

	public boolean addCar(byte addr) {
		return addCar(new CarImage(addr));
	}

	public boolean addCar(CarImage car) {
		if (get(car.getLogAddr()) == null) {
			map.put(car.getLogAddr(), car);
			list.add(car);
			log.info(String.format("Added new car %d.", car.getLogAddr()));
			return true;
		}

		return false;
	}

	public int getPos(byte addr) {
		try {
			return map.get(addr).getPosition();
		} catch (NullPointerException ex) {
			log.log(Level.SEVERE, String.format("Car %d is not on the list.", addr), ex);
			return -1;
		}
	}

	/**
	 * Returns the position difference between this car and the car with the
	 * specified logical address
	 * 
	 * @param addr
	 *            The address of the car to which we want to calculate the
	 *            position difference
	 * @return The difference in position
	 */
	public int positionDifference(byte addr, int myPos) {
		int pos = getPos(addr);
		if (pos < 0 || myPos < 0) {
			return 0;
		} else {
			return (pos <= myPos) ? myPos - pos : list.size() - pos + myPos;
		}
	}
	
	public CarImageInterface getSuccessor(int position){
		if (position != size()) {
			return getByPos(position + 1);
		} else {
			return null;
		}
	}
	
	public CarImageInterface getPredecessor(int position){
		if (position != size()) {
			return getByPos(position - 1);
		} else {
			return null;
		}
	}
	
	public CarImageInterface getSuccessor(CarImage car){
		return getSuccessor(car.getPosition());
	}
	
	public CarImageInterface getPredecessor(CarImage car){
		return getPredecessor(car.getPosition());
	}

	@Override
	public CarImageInterface getFirst() {
		if(size() > 0){
			return list.get(0);
		}
		return null;
	}

	@Override
	public CarImageInterface getLast() {
		int size = size();
		if(size > 0){
			return list.get(size - 1);
		}
		return null;
	}
	
}
