package slotcar.network;

import java.util.Arrays;

import org.apache.commons.math3.stat.descriptive.SummaryStatistics;

public class NetworkStatInterpret {

	private static final byte binCount = 20;
	private static final byte binWidth = 5;
	private static final short binMax = binCount * binWidth;
	
	private int[] frequency;
	private int outliers;
	SummaryStatistics stats;
	private double loss;
	
	public NetworkStatInterpret(double loss,short[] array) {
		stats = new SummaryStatistics();
		frequency = new int[binCount];
		this.loss = loss;
		for (int i = 0; i < array.length; i++) {
			stats.addValue(array[i]);
			if (array[i] < binMax && array[i] > 0) frequency[array[i]/binWidth]++;
			else outliers++;
		}
	}
	
	public double getLoss() {
		return loss;
	}
	public double getMean() {
		return stats.getMean();
	}
	
	public double getStandardDeviation() {
		return stats.getStandardDeviation();
	}
	public double getMax() {
		return stats.getMax();
	}
	
	public double getMin() {
		return stats.getMin();

	}
	public String histogram() {
		StringBuilder sb = new StringBuilder();
		int maxcount = Arrays.stream(frequency).max().getAsInt();
		if (maxcount <= 0) return "!! No valid histogram data\n";
	    int scale = ((maxcount + binMax - 1) / binMax) + 1;
	    int width = (maxcount + scale - 1) / scale;
	    String format = "%5d-%-5d | %-" + width + "s | %d\n";
	    for (int i = 0; i < binCount; i++) {
	        int start = (binWidth * i);
	        int end = start + binWidth - 1;
	        sb.append(String.format(format, start, end, buildAsterisks(frequency[i], scale), frequency[i]));
	    }


	    //sb.append(String.format("%11s | %s\n", "", lines(width)));
	    //sb.append(String.format("%-11s | %s\n", "Scale", marks(maxcount, scale)));
	    return sb.toString();
	}

	private static Object marks(int maxcount, int scale) {
	    int width = (int)Math.log10(maxcount - 1) + 2;
	    int count = ((maxcount + scale - 1) / scale) / width;
	    StringBuilder sb = new StringBuilder();
	    int mark = 0;
	    int step = scale * width;
	    for (int i = 0; i < count; i++) {
	        mark += step;
	        sb.append(String.format("%" + width + "d", mark));
	    }
	    return sb.toString();
	}

	private static String lines(int width) {
	    char[] array = new char[width];
	    Arrays.fill(array, '-');
	    return new String(array);
	}

	private static String buildAsterisks(int size, int scale) {
	    char[] array = new char[(size + scale - 1) / scale];
	    Arrays.fill(array, '*');
	    return new String(array);
	}
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Statistics ↴\n");
		sb.append("Mean: ").append(getMean()).append(" ms,\n")
		.append("Std. deviation: ").append(getStandardDeviation()).append(" ms,\n")
		.append("Min & max: ").append(getMin()).append(" ms ").append(getMax()).append(" ms,\n");
		sb.append("\nHistogram ↴\n");
		
		sb.append(histogram());
		sb.append("Outliers: ").append(outliers).append("\n");
		
		return sb.toString();
	}
	
}
