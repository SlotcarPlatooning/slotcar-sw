package slotcar.network;

public interface NewDataEventHandler {

	void handle(NewDataEvent event);

}
