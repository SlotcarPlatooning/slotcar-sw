package slotcar.network;

public enum ServiceType {
	UNKNOWN_MESSAGE_TYPE ((byte)0x00),
	ERROR ((byte)0x01),
	CONTROLLER_CONFIG ((byte)0x10),
	
	COMMUNICATION_TEST_START ((byte)0x21),
	COMMUNICATION_TEST_PACKET ((byte)0x22),
	COMMUNICATION_TEST_END ((byte)0x23),
	COMMUNICATION_TEST_RESULT ((byte)0x24),
	CONTROLLER_CONSTANTS_REQUEST((byte)0x25),
	CONTROLLER_CONSTANTS((byte)0x26),
	//logging services
	
	CAR_LOGGING_REQUEST ((byte)0x3A),
	CAR_LOGGING_ACK ((byte)0x3B),
	
	
	
	CONTROL_START((byte) 0x40),
	CONTROL_STOP((byte) 0x41),
	//POSITIONING_START ((byte)0x42),
	//POSITIONING_STOP ((byte)0x43),
	COMMUNICATION_START ((byte)0x44),
	COMMUNICATION_STOP ((byte)0x45),
	LOGGING_START ((byte)0x46),
	LOGGING_STOP ((byte)0x47),
	MANUAL_SPEED((byte)0x48),
	UPLOAD_SETTINGS ((byte)0x4A),
	IMAGE_REQUEST ((byte)0x4B),	
	IMAGE_REQUESTED((byte)0x4C),
	IMAGE ((byte)0x4D),
	STATE_CHANGE((byte)0x4E),
	
	RECONNECT_REQUEST ((byte)0x5A),
	
	CALIBRATION_A ((byte)0x91),
	CALIBRATION_B ((byte)0x92),


	
	SYNC_ROUTING_TABLE ((byte)0x6A),
	SYNC_CAR_LIST ((byte)0x6B),
	COMMUNICATION_TOPOLOGY ((byte)0x6C),
	SET_POSITION ((byte)0x6D),
	
	STATUS_REQUEST((byte)0x6E),
	STATUS((byte)0x6F),
	
	NET_STAT_LOG_REQ((byte)0x81),
	NET_STAT_LOG((byte)0x82),
	NET_CLOCK_LOG_REQ((byte)0x83),
	NET_CLOCK_LOG((byte)0x84), 
	
	NET_START_PROBE((byte)0x97),
	NET_STOP_PROBE((byte)0x98),
	NET_PROBE((byte)0x99);
	
//	START_EXPERIMENT ((byte)0x7A),
//	STOP_EXPERIMENT ((byte)0x7B); 
	
	
	
	
	
	   
	private final byte serviceNum;
	
	ServiceType(byte serviceNum)
	{
		this.serviceNum = serviceNum;
	}
	
	public byte getServiceNum()
	{
		return serviceNum;
	}
	
	public static ServiceType getServiceType(int serviceNum)
	{
		for (ServiceType type: ServiceType.values()) {
			if(type.serviceNum == serviceNum) return type;
		}
		return ServiceType.UNKNOWN_MESSAGE_TYPE;
	}
	
}
