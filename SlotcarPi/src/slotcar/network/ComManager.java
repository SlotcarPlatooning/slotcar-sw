package slotcar.network;

public abstract class ComManager {//to do another functions
	
//	public abstract void createNetwork();
	public abstract boolean open();
	public abstract void close();
	public abstract void send(NetworkAddress networkAddr, byte [] data) throws TypeMismatchNetworkAddressException;//ack and crc
	public abstract void send(byte[] data);//for no ack
	//public abstract void setSettingsMode(boolean settingMode);
	//public abstract boolean getSettingsMode();
	public abstract boolean isConnected();
	//public abstract void setConnected(boolean connected);
	public abstract NetworkAddress getNetAddr();
	
	public abstract void addNewDataEventListener(NewDataEventListener listener);
}
