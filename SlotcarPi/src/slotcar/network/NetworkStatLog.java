package slotcar.network;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class NetworkStatLog {
	
	public static final short queueLength = 10000;
	
	private int measuredLossCount;
	private int measuredSampleCount;
	private short[] measuredDelay;
	private int pos;
	

	public NetworkStatLog() {
		measuredDelay = new short[queueLength];
		pos = 0;
	}

	public synchronized byte[] toArray() {
		System.out.println("Log to array requested!");
		ByteBuffer bb = ByteBuffer.allocate(8+queueLength*2);
		bb.putInt(measuredLossCount);
		bb.putInt(measuredSampleCount);
			for (int i = 0; i < queueLength; i++) {
				bb.putShort(measuredDelay[i]);
			}
		return bb.array();
	}
	public synchronized boolean registerSample(short delay) {
		
		measuredDelay[pos++] = delay;
		measuredSampleCount++;
		System.out.println("Registered sample # "+ pos);
		
		return pos < queueLength;
	}
	
	public synchronized void registerLoss(int c) {
		measuredSampleCount += c;
	}
	
	public void dump() {
		final DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd:hh-mm-ss");
		BufferedWriter outputWriter = null;
		File dir = new File("/tmp/logs");
		dir.mkdirs();
		try {
			File netlog = new File(dir, "netlog"+formatter.format(new Date())+".dat");
			outputWriter = new BufferedWriter(new FileWriter(netlog));

			for (int i = 0; i < measuredDelay.length; i++) {
				outputWriter.write(Short.toString(measuredDelay[i]));
				outputWriter.newLine();
			} 
			outputWriter.flush();  
			outputWriter.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
