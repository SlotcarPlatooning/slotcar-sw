package slotcar.network;

import slotcar.wifi.WiFiNetworkAddress;

public enum NetworkAddressType {
	

	WIFI(WiFiNetworkAddress.class.getClass(), (byte)1);
	
	private final Class<NetworkAddress> c;
	private final byte t;
	
	
	private NetworkAddressType(Class<?> c, byte type) {
		this.c = (Class<NetworkAddress>) c; // TODO Check casting
		this.t = type;
	}
	 
	public Class<NetworkAddress> getC() {
		return c;
	}
	
	public byte getT(){
		return t;
	}
	
	public static Class<NetworkAddress> getClassByType(byte t)
	{
		for (NetworkAddressType addrType: values()) {
			if(addrType.t == t) return addrType.c;
		}
		return null;
	}
	
	
	

}
