package slotcar.network;

public interface NetworkAddress {

	public NetworkAddressType getType();
	
	public byte[] toBytes();
	
	@Override
	public String toString();
}
