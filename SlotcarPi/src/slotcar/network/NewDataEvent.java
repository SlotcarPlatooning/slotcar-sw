package slotcar.network;

import java.nio.ByteBuffer;
import java.util.EventObject;

public class NewDataEvent extends EventObject{

	private static final long serialVersionUID = -8603545967730833004L;
    private final long timeStamp;
    private final NetworkAddress ntwkAddr;
    private final CarPacket packet;
    private final long milliRec;
    
    public NewDataEvent(Object source, ByteBuffer data, long timeStamp, NetworkAddress ntwkAddr) {
        super(source);
        this.packet = new CarPacket(data);
        this.timeStamp = timeStamp;
        this.ntwkAddr = ntwkAddr;
        this.milliRec = timeStamp;
    }

    public CarPacket getPacket() {
        return packet;
    }

	public long getTimeStamp() {
		return timeStamp;
	}
	
	public NetworkAddress getNtwkAddr() {
		return ntwkAddr;
	}

	@Override
	public String toString() {
		return String.format("addr: %s packet: %s", ntwkAddr, packet);

	}
	
	public long getMilliRec() {
		return milliRec;
	}
}


