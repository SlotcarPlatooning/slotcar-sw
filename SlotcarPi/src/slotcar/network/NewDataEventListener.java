package slotcar.network;

public interface NewDataEventListener {
    public void handleNewDataEvent(NewDataEvent e);
}
