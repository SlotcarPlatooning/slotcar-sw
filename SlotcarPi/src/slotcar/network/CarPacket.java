
package slotcar.network;

import java.nio.ByteBuffer;

public class CarPacket {

	private final ServiceType service;
	private final byte destAddr;
	private final byte sourceAddr;
	private byte[] payload;
	public int milliSent;

	public CarPacket(ByteBuffer packet) {
		packet.position(0);
		milliSent = packet.getInt();
		byte serviceByte = packet.get();
		this.service = ServiceType.getServiceType(serviceByte);
		short l = packet.getShort();
		destAddr = packet.get();
		sourceAddr = packet.get();
		payload = new byte[l];
		packet.get(payload);
	}

	public CarPacket(byte service, byte destAddr, byte sourceAddr, byte[] payload, int timeStamp) {
		this.service = ServiceType.getServiceType(service);
		this.destAddr = destAddr;
		this.sourceAddr = sourceAddr;
		this.payload = payload;
		this.milliSent = timeStamp;
	}

	public byte getDestAddr() {
		return destAddr;
	}

	public byte getSourceAddr() {
		return sourceAddr;
	}

	public byte getServiceByte() {
		return (byte) service.getServiceNum();
	}

	public ServiceType getService() {
		return service;
	}

	public byte[] getPayload() {
		return payload;
	}

	@Override
	public String toString() {
		return String.format("%s from %d", service, sourceAddr);
	}

	public byte[] toArray() {
		int payloadLength = payload != null ? payload.length : 0;
		ByteBuffer bf = ByteBuffer.allocate(5 + 4 + payloadLength);
		bf.putInt(milliSent);
		bf.put((byte) service.getServiceNum());
		bf.putShort((short) payloadLength);
		bf.put(destAddr);
		bf.put(sourceAddr);
		if (payload != null) {
			bf.put(payload);
		}
		return bf.array();
	}

}
