package slotcar.network;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

public class NetworkFaultParameters {
	
	private float forcedLoss;
	private float forcedDelayMean;
	private float forcedDelayVariance;
		
	
	public NetworkFaultParameters(float forcedLoss, float forcedDelayMean, float forcedDelayVariance) {
		super();
		this.forcedLoss = forcedLoss;
		this.forcedDelayMean = forcedDelayMean;
		this.forcedDelayVariance = forcedDelayVariance;
	}
	public NetworkFaultParameters() {
		super();
	}

	
	public float getForcedLoss() {
		return forcedLoss;
	}
	
	public void setForcedLoss(float forcedLoss) {
		this.forcedLoss = forcedLoss;
	}

	public float getForcedDelayMean() {
		return forcedDelayMean;
	}

	public void setForcedDelayMean(float forcedDelayMean) {
		this.forcedDelayMean = forcedDelayMean;
	}

	public float getForcedDelayVariance() {
		return forcedDelayVariance;
	}

	public void setForcedDelayVariance(float value) {
		this.forcedDelayVariance = value;
	}

	@Override
	public String toString() {
		return "NetworkFaultParameters [forcedLoss=" + forcedLoss + ", forcedDelayMean=" + forcedDelayMean
				+ ", forcedDelayVariance=" + forcedDelayVariance + "]";
	}
	public void write(ObjectOutput out) throws IOException {
		out.writeFloat(forcedDelayMean);
		out.writeFloat(forcedDelayVariance);
		out.writeFloat(forcedLoss);
		
	}
	public void read(ObjectInput in) throws IOException {
		forcedDelayMean = in.readFloat();
		forcedDelayVariance = in.readFloat();
		forcedLoss = in.readFloat();
	}

	

}
