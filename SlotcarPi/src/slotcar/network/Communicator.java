package slotcar.network;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.time.Duration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import slotcar.Config;
import slotcar.RoutingTable;
import slotcar.SlotcarState;
import slotcar.car.CarImage;
import slotcar.clock.CarClock;
import slotcar.pi.controller.Controller;

public abstract class Communicator implements NewDataEventListener {

	private static final Logger log = Logger.getLogger(Communicator.class.getName());

	Map<ServiceType, NewDataEventHandler> handlers = new HashMap<>();

	protected ComManager comMan;
	protected byte myLogAddr;
	protected NetworkRole networkRole;
	protected boolean connected;
	protected NetworkStatLog netStat;

	public abstract NewDataEventHandler getDefaultHandler();

	public void addHandler(ServiceType serviceType, NewDataEventHandler handler) {
		this.handlers.put(serviceType, handler);
	}

	public Communicator(ComManager comMan, NetworkRole server, byte myLogAddr) {
		this.myLogAddr = myLogAddr;
		this.networkRole = server;
		this.comMan = comMan;

		comMan.addNewDataEventListener(this);

		RoutingTable.add(myLogAddr, comMan.getNetAddr());

	}

	/**
	 * Hlavni metoda zpracovavajici prichozi zpravy
	 * 
	 * @param e
	 */
	@SuppressWarnings("unused")
	@Override
	public void handleNewDataEvent(NewDataEvent e) {
		NewDataEventHandler h;

		int curTime = CarClock.getTimestampMilli();
		if (netStat != null && e.getPacket().getSourceAddr() != Config.ADDR_SERVER) {
			if (!netStat.registerSample((short) (curTime - e.getPacket().milliSent))) {
				if (NetworkStatLog.queueLength > 200) {
					netStat.dump();
					netStat = null;
					this.sendErrorMessage("Statistics dumped to /tmp/logs", Config.ADDR_SERVER);
				} else {
					byte[] payload = netStat.toArray();
					netStat = null;
					this.send(Config.ADDR_SERVER, ServiceType.NET_STAT_LOG, payload);
				}
			}
		}		
		try {
			h = handlers.get(e.getPacket().getService());
			if (h == null) {
				h = getDefaultHandler();
			}
			h.handle(e);
		} catch (Exception ex) {
			log.log(Level.SEVERE, "Error in handling received data " + ex.getMessage(), ex);
			OutputStream out = new ByteArrayOutputStream();
			PrintStream s = new PrintStream(out);
			ex.printStackTrace(s);
			Logger.getGlobal().log(Level.SEVERE, out.toString());

		}
	}
	
	public void sendTo( byte[] addresses,ServiceType service, byte[] data){
		for (byte addr : addresses) {
			this.send(addr, service,data);
		}
	}
	
	public void sendTo(byte[] addresses, ServiceType service){
		sendTo(addresses, service, null);
	}
	
	public void sendTo( List<CarImage> cars,ServiceType service, byte[] data){
		for (CarImage car : cars) {
			this.send(car.getLogAddr(), service,data);
		}
	}
	
	public void sendTo(List<CarImage> cars, ServiceType service){
		sendTo(cars, service, null);
	}

	public void sendErrorMessage(String s, byte logAddr) {
		try {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			ObjectOutputStream str = new ObjectOutputStream(out);
			str.writeUTF(s);
			str.close();
			send(logAddr, ServiceType.ERROR, out.toByteArray());
		} catch (IOException ex) {
			Logger.getGlobal().log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * Sends the table having positions in the string of all vehicles to all
	 * vehicles. This position table is the outcome of the positioning
	 * procedure. This table is sent to each vehicle based on its address. The
	 * message is acknowledged or broadcast used.
	 * 
	 * @param positions
	 *            The list of logical addresses in the same order as are
	 *            vehicles in the platoon.
	 */
	public void sendSyncPositionTable(byte[] positions, boolean broadcast) {
		if (broadcast) {
			send(ServiceType.SYNC_CAR_LIST, positions);
		} else {
			for (byte addr : positions) {
				this.send(addr, ServiceType.SYNC_CAR_LIST, positions);
			}
		}
	}

	/**
	 * Sends the table having positions in the string of all vehicles to all
	 * vehicles. This position table is the outcome of the positioning
	 * procedure. This table is sent to each vehicle using broadcast.
	 * 
	 * @param positions
	 *            The list of logical addresses in the same order as are
	 *            vehicles in the platoon.
	 */
	public void sendSyncPositionTable(byte[] positions) {
		sendSyncPositionTable(positions, true);
	}

	/**
	 * Sends to all vehicles the command to start determining its own position
	 * in the string. This is sent using broadcast.
	 */
//	public void sendStartPositioning() {
//		send(ServiceType.POSITIONING_START);
//	}

	/**
	 * Sends to all vehicles in the list the command to start determining its
	 * own position in the string. This is sent using vehicle's address, hence
	 * acknowledge is required.
	 * 
	 * @param addresses
	 *            The byte array containing the addresses of the vehicles which
	 *            should be informed about starting of the positioning
	 *            procedure.
	 */
//	public void sendStartPositioning(Byte[] addresses) {
//		for (Byte addr : addresses) {
//			this.send(addr, ServiceType.POSITIONING_START, null);
//		}
//	}

	/**
	 * Send to all vehicles in the command to stop determining its own position
	 * in the string. This is done after all vehicles reported their position in
	 * the string. The message is send using broadcast.
	 */
//	public void sendStopPositioning() {
//		send(ServiceType.POSITIONING_STOP);
//	}

	/**
	 * Send to all vehicles in the command to stop determining its own position
	 * in the string. This is done after all vehicles reported their position in
	 * the string. The message is send using address of all vehicles, hence ACK
	 * is required.
	 * 
	 * @param addresses
	 *            The byte array containing the addresses of the vehicles which
	 *            should be informed about stopping of the positioning
	 *            procedure.
	 */
//	public void sendStopPositioning(Byte[] addresses) {
//		for (Byte addr : addresses) {
//			this.send(addr, ServiceType.POSITIONING_STOP, null);
//		}
//	}

	/**
	 * Sends the routing table to all vehicles using broadcast. The routing
	 * table contains the logical addresses and IP addresses, so it is used to
	 * map the logical address (fixed for each car) to the IP address of the
	 * car. The IP address is mapped dynamically, hence this address translation
	 * is necessary.
	 */
	public void sendSyncRoutingTable() {
		try {
			send(ServiceType.SYNC_ROUTING_TABLE, RoutingTable.serializeToBytes());
		} catch (IOException e) {
			log.log(Level.SEVERE, "", e);
		}
	}

	/**
	 * Sends the routing table to all vehicles using their addresses, so the
	 * communication is acknowledged. The routing table contains the logical
	 * addresses and IP addresses, so it is used to map the logical address
	 * (fixed for each car) to the IP address of the car. The IP address is
	 * mapped dynamically, hence this address translation is necessary.
	 */
	public void sendSyncRoutingTable(byte[] addresses) {
		byte[] data;
		try {
			data = RoutingTable.serializeToBytes();
		} catch (IOException e) {
			Logger.getGlobal().log(Level.SEVERE, "", e);
			return;
		}

		if (data.length > 576) {
			log.severe("Routing table is too big, size " + data.length);
		}
		for (Byte address : addresses) {
			this.send(address, ServiceType.SYNC_ROUTING_TABLE, data);
		}
	}

	// /**
	// * Sends request to all cars to start experiment. This is done using
	// * addressing with acknowledgement.
	// *
	// * @param experimentID
	// * The number of experiment. Based on the number a file is opened
	// * and the data are stored in the file.
	// */
	// public void sendStartExperiment(int experimentID, byte... addresses) {
	// for (byte addr : addresses) {
	// byte data[] = ByteBuffer.allocate(4).putInt(experimentID).array();
	// send(addr, ServiceType.START_EXPERIMENT, data);
	// }
	// }

	// /**
	// * Sends request to all cars to start experiment. This is done using
	// * broadcast.
	// *
	// * @param experimentID
	// * The number of experiment. Based on the number a file is opened
	// * and the data are stored in the file.
	// */
	// public void sendStartExperiment(int experimentID) {
	// sendStartExperiment(experimentID, Config.ADDR_BROADCAST);
	// }

	/**
	 * Sends to the specified car confirmation that it successfully joined the
	 * network.
	 * 
	 * @param logAddr
	 *            The logical address of the car to which to send the request.
	 */
	public void sendCarLoggingAck(byte logAddr) {
		this.send(logAddr, ServiceType.CAR_LOGGING_ACK, this.getNtwkAddr().toBytes());
	}

	/**
	 * Sends the request for logging in the network. This is sent using
	 * broadcast.
	 */
	public void sendCarLoggingRequest() {
		send(ServiceType.CAR_LOGGING_REQUEST, this.getNtwkAddr().toBytes());
	}

	/**
	 * Sends to the server the message that this car has finished the
	 * positioning procedure.
	 */
	public void sendSetPosition() {
		this.send(Config.ADDR_SERVER, ServiceType.SET_POSITION, null);
	}

	/**
	 * Sends the car image to the address which requested it
	 * 
	 * @param carImage
	 *            The car's data to share
	 * @param destAddr
	 *            The address of the device which requested it. Typically the
	 *            server.
	 */

	public void sendImage(CarImage carImage, byte destAddr, boolean requested) {
		try {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			ObjectOutputStream s = new ObjectOutputStream(out);
			carImage.write(s);
			s.flush();
			byte[] a = out.toByteArray();

			s.close();
			out.close();
			this.send(destAddr, requested ? ServiceType.IMAGE_REQUESTED : ServiceType.IMAGE, a);
		} catch (IOException ex) {
			log.log(Level.WARNING, null, ex);
		}
	}

	public void sendImage(CarImage carImage, byte destAddr) {
		this.sendImage(carImage, destAddr, false);
	}

	public void sendImage(CarImage carImage) {
		this.sendImage(carImage, Config.ADDR_BROADCAST, false);
	}

	public void sendStatus(byte destAddr, SlotcarState state, int controlHashCode, long timeOfChange) {
		ByteBuffer bf = ByteBuffer.allocate(4 + 4 + 8);
		bf.putInt(state.getVal());
		bf.putInt(controlHashCode);
		bf.putLong(timeOfChange);
		this.send(destAddr, ServiceType.STATUS, bf.array());
	}

	/**
	 * Informs all the cars in the list that they should get ready for a
	 * communication test. The request is sent based on car's address, so it is
	 * acknowledged.
	 * 
	 * @param cars
	 *            The list of cars to which the request should be sent.
	 */
	public void sendCommTestStart(List<CarImage> cars) {
		for (CarImage car : cars) {
			this.send(car.getLogAddr(), ServiceType.COMMUNICATION_TEST_START, null);
		}
	}

	/**
	 * Sends a given number of packets using broadcast. The packets do not have
	 * any data, only the number of received packets counts.
	 * 
	 * @param numberOfPackets
	 *            The number of packets to be sent.
	 */
	public void sendCommTestPackets(int numberOfPackets) {
		for (int i = 0; i < numberOfPackets; i++) {
			send(ServiceType.COMMUNICATION_TEST_PACKET);
		}
	}

	/**
	 * Informs all the cars in the list that the communication test is finished.
	 * The request is sent based on car's address, so it is acknowledged. The
	 * cars should reply by sending their number of received packets.
	 * 
	 * @param cars
	 *            The list of cars to which this request should be sent.
	 */
	public void sendCommTestEnd(List<CarImage> cars) {
		for (CarImage car : cars) {
			this.send(car.getLogAddr(), ServiceType.COMMUNICATION_TEST_END, null);
		}
	}

	/**
	 * Sends to the server the results of the communication test procedure
	 * 
	 * @param numberOfReceivedPacket
	 *            The number of packets received during the communication test
	 *            procedure.
	 */
	public void sendCommTestResult(int numberOfReceivedPacket) {
		byte[] arr = null;
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try {
			ObjectOutputStream s = new ObjectOutputStream(out);
			s.writeInt(numberOfReceivedPacket);
			s.close();
			arr = out.toByteArray();

		} catch (IOException ex) {
			Logger.getGlobal().log(Level.SEVERE, null, ex);
		}
		this.send(Config.ADDR_SERVER, ServiceType.COMMUNICATION_TEST_RESULT, arr);
	}

	/**
	 * Sends the settings (desired velocity, controller etc.) to each car in the
	 * list. The message is sent to each car separately.
	 * 
	 * @param controllers
	 *            The list of cars to which the parameters should be sent.
	 */
	public void sendUploadSettings(CarImage carImage) {

		try {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			ObjectOutputStream s = new ObjectOutputStream(out);
			carImage.getCarControl().write(s);
			s.close();
			this.send(carImage.getLogAddr(), ServiceType.UPLOAD_SETTINGS, out.toByteArray());
		} catch (IOException ex) {
			Logger.getGlobal().log(Level.SEVERE, null, ex);
		}

	}

	public void sendControllerConstants(Controller c, List<CarImage> list) {
		Logger.getGlobal().fine("Uploading constants of the controller." + c.toString());
		for (CarImage carImage : list) {
			sendControllerConstants(c, carImage.getLogAddr());
		}
	}

	public void sendControllerConstants(Controller c, byte logAddr) {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		c.writeTo(out);
		this.send(logAddr, ServiceType.CONTROLLER_CONSTANTS, out.toByteArray());
	}

	public void sendControllerConstantsRequest(Controller c, byte logAddr) throws NullPointerException {
		this.send(logAddr, ServiceType.CONTROLLER_CONSTANTS_REQUEST, c.getName().getBytes());
	}
	
	public void sendClockLog(byte logAddr, byte[] log) {
		this.send(logAddr, ServiceType.NET_CLOCK_LOG, log);
	}
	
	public void sendNetStatLog(byte logAddr, byte[] log) {
		this.send(logAddr, ServiceType.NET_STAT_LOG, log);
	}
	
	public void sendProbe(int size) {
		this.send(ServiceType.NET_PROBE, new byte[size]);
	}

	// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	protected void send(byte logAddr, ServiceType service,  float forcedDelay, byte... data) {
		CarPacket packet = new CarPacket(service.getServiceNum(), logAddr, this.myLogAddr, data,CarClock.getTimestampMilli());

		byte[] p = packet.toArray();
		if (forcedDelay != 0.0f)
			try {
				Thread.sleep((int)forcedDelay);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		if (logAddr == Config.ADDR_BROADCAST) {
			log.fine(String.format("Sending %s to broadcast ", service.name()));
			comMan.send(p);
		} else {
			NetworkAddress ntwkAddr = RoutingTable.get(logAddr);
			if (ntwkAddr == null) {
				log.severe(String.format("Log address %d it not in routing table!", logAddr));
				log.finer(RoutingTable.print());
			} else {
				try {
					log.fine(String.format("Sending %s to addr: %d", service.name(), logAddr));
					comMan.send(ntwkAddr, p);
				} catch (TypeMismatchNetworkAddressException e) {
					log.log(Level.SEVERE, null, e);
				}
			}
		}
	}
	
	/**
	 * Sends the data to the network. The main method for sending.
	 * 
	 * @param logAddr
	 *            The logical address of the destination. Set 0xFF or 255 for
	 *            broadcast.
	 * @param service
	 *            The number of the service. See the service list in ServiceType
	 * @param payload
	 *            The data to be sent using this service.
	 * @throws InterruptedException 
	 * @throws TypeMismatchNetworkAddressException
	 */
	protected void send(byte logAddr, ServiceType service, byte... data) {

		CarPacket packet = new CarPacket(service.getServiceNum(), logAddr, this.myLogAddr, data,CarClock.getTimestampMilli());

		byte[] p = packet.toArray();
		if (logAddr == Config.ADDR_BROADCAST) {
			log.fine(String.format("Sending %s to broadcast ", service.name()));
			comMan.send(p);
		} else {
			NetworkAddress ntwkAddr = RoutingTable.get(logAddr);
			if (ntwkAddr == null) {
				log.severe(String.format("Log address %d it not in routing table!", logAddr));
				log.finer(RoutingTable.print());
			} else {
				try {
					log.fine(String.format("Sending %s to addr: %d", service.name(), logAddr));
					comMan.send(ntwkAddr, p);
				} catch (TypeMismatchNetworkAddressException e) {
					log.log(Level.SEVERE, null, e);
				}
			}
		}

		// WARNING THIS LOG SLOWS IT SO MUCH
		/*
		 * Logger.getGlobal().finest("Sent car packet: " + byteToHex(data) +
		 * " to:" + (logAddr & 0xFF));
		 */
	}

	@SuppressWarnings("unused")
	private String byteToHex(byte[] bytes) {
		if (bytes == null)
			return "";
		StringBuilder s = new StringBuilder();
		for (byte i : bytes) {
			s.append(String.format("0x%02X", i)).append(",");
		}
		return s.toString();
	}

	protected void send(byte logAddr, ServiceType service) {
		send(logAddr, service, null);
	}

	protected void send(ServiceType service, byte[] payload) {
		this.send(Config.ADDR_BROADCAST, service, payload);
	}

	protected void send(ServiceType service) {
		this.send(service, null);
	}

	/**
	 * Tries to connect to the server. Periodically (every 2s) sends the
	 * connection requests. Once connected, it stops sending the request.
	 */
	public void connectToServer() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				while (!isConnected()) {
					log.info("Connecting to server...");
					sendCarLoggingRequest();
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						log.log(Level.SEVERE, null, e);
					}
				}
				log.info("Connected to server.");
			}
		}).start();
	}

	// public void connectNTWK(int panID, int channel) {
	// if (networkRole == NetworkRole.CAR) {
	// if (comInterface == CommunicationInterface.ZIGBEE) {
	// comMan.connect(panID, channel);
	// } else {
	// comMan.createNetwork();
	// // this.connected = true;
	// }
	// addAddr(serverLogAddr, getNtwkAddr());
	// } else {
	// System.out.println("Server cannot be connect to itself");
	// }
	// }

	public boolean isConnected() {
		return connected;
	}

	public void setConnected(boolean connected) {
		this.connected = connected;
	}

	public NetworkAddress getNtwkAddr() {
		return comMan.getNetAddr();
	}

	public void initNetStatLog() {
		if (netStat == null) {
			netStat = new NetworkStatLog();
		}
		
	}
}
