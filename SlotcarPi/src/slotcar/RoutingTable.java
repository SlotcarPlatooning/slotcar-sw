package slotcar;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.io.IOUtils;

import slotcar.network.NetworkAddress;
import slotcar.wifi.WiFiNetworkAddress;

public class RoutingTable {

	private static final Logger log = Logger.getLogger(RoutingTable.class.getName());

	static {
		log.setLevel(Level.WARNING);
	}

	private static Map<Byte, NetworkAddress> list = new HashMap<>(20);
	private static List<RoutingTableListener> listeners = new ArrayList<>();
	private static RoutingTableModel model = new RoutingTableModel();

	public static void add(byte addr, NetworkAddress nAddr) {
		if (nAddr == null) {
			return;
		}
		if (list.get(addr) == null) {
			list.put(addr, nAddr);
			for (RoutingTableListener routingTableListener : listeners) {
				routingTableListener.added(addr, nAddr);
			}
			model.dataChanged();
		} else {
			list.put(addr, nAddr);
		}
	}

	public static void remove(byte addr) {
		if (list.remove(addr) != null) {
			for (RoutingTableListener routingTableListener : listeners) {
				routingTableListener.deleted(addr);
			}
			model.dataChanged();
		}
	}

	public void clear() {
		list.clear();
		for (RoutingTableListener routingTableListener : listeners) {
			routingTableListener.cleared();
		}
		model.dataChanged();
	}

	public static void serialize(OutputStream out) throws IOException {
		log.finer(print());
		for (Entry<Byte, NetworkAddress> item : list.entrySet()) {
			out.write(item.getKey());
			NetworkAddress n = item.getValue();
			out.write(n.getType().getT());
			out.write(n.toBytes());
		}
	}

	void addRoutingTableListener(RoutingTableListener l) {
		listeners.add(l);
	}

	public static Map<Byte, NetworkAddress> getList() {
		return list;
	}

	public static void deserialize(InputStream in) throws IOException {

		deserializeFromBytes(IOUtils.toByteArray(in));
	}

	public static byte[] serializeToBytes() throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		serialize(out);

		byte[] b = out.toByteArray();
		StringBuilder s = new StringBuilder();
		for (byte i : b) {
			s.append(String.format("0x%02X", i)).append(",");
		}
//		log.finer("data: " + s.toString());
		log.finer("Serializing routing table, length: " + b.length);
		return b;
	}

	public static void deserializeFromBytes(byte[] in) {
		log.finer("Deserializing routing table, length: " + in.length);
//		
//		StringBuilder s = new StringBuilder();
//		for (byte i : in) {
//			s.append(String.format("0x%02X", i)).append(",");
//		}
//		log.finer("data: " + s.toString());

		ByteBuffer b = ByteBuffer.wrap(in);
		try {
			while (b.remaining() > 0) {
				byte logAddr = b.get();
				// TODO add more cases
				if (b.get() == 1) {
					// Class<NetworkAddress> c =
					// NetworkAddressType.getClassByType();
					// NetworkAddress addr = c.newInstance();
					NetworkAddress addr = new WiFiNetworkAddress(b);
					add(logAddr, addr);
					log.log(Level.FINER, print());
				} else {
					log.log(Level.SEVERE, "Ilegal data for deserialization");
					return;
				}
			}
		} catch (IllegalArgumentException e) {

			log.log(Level.SEVERE, null, e);
		}

	}

	public static void main(String[] args) throws Exception {
		// Debugging.logEverything(Level.FINEST);
		InetAddress local = InetAddress.getByName("127.0.0.1");

		InetAddress local2 = InetAddress.getByName("127.0.0.2");
		//
		RoutingTable.add((byte) 10, new WiFiNetworkAddress(local));
		RoutingTable.add((byte) 11, new WiFiNetworkAddress(local2));
		//
		System.out.println(print());
		byte[] data = serializeToBytes();
		//
		deserializeFromBytes(data);
		//
		System.out.println(print());

		// Gson gson = new Gson();
		// gson.newJsonWriter()
		// System.out.println(gson.toJson(list));

	}

	public static NetworkAddress get(byte logAddr) {

		return list.get(logAddr);
	}

	public static RoutingTableModel getModel() {
		return model;
	}

	public static String print() {
		StringBuilder sb = new StringBuilder();
		for (Map.Entry<Byte, NetworkAddress> entry : list.entrySet()) {
			sb.append(entry.getKey()).append(" : ").append(entry.getValue()).append("\n");
		}
		return sb.toString();
	}

}
