package slotcar;

import java.awt.Color;
import java.io.File;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class Config extends ExtProperties {

	private static final long serialVersionUID = 8302035351644544495L;
	
	/*
	 * TIMING
	 */
	
	// STATE LOGGER
	public static final long LOG_SAMPLE_PERIOD = 10;
	public static final TimeUnit LOG_SAMPLE_PERIOD_UNIT = TimeUnit.MILLISECONDS;
	
	// EXPERIMENT
	public static final long EXPERIMENT_PERIOD = 30;
	public static final TimeUnit EXPERIMENT_PERIOD_UNIT = TimeUnit.MILLISECONDS;
	
	// SPI
	public static final long SPI_MEMORY_PERIOD = 5;
	public static final TimeUnit SPI_MEMORY_PERIOD_UNIT = TimeUnit.MILLISECONDS;
	
	/*
	 * STM
	 */
	
	public static final boolean STM_ENABLE = true;

	public static final float REF_DIST_MIN = 5; // [cm]
	public static final float REF_DIST_MAX = 50; // [cm]

	public static final float COEF_DIST_BACK_DEFAULT = 2.5f;
	public static final float COEF_DIST_FRONT_DEFAULT = 2.5f;

	public static final float SPEED_MAX = 2;
	public static final float DISTANCE_MAX = 0.5f; // [m]
	public static final float DISTANCE_MIN = 0.05f; // [m]
	public static final float PWM_MAX = 1;
	
	/*
	 * SSH
	 */
		
	public static final String SSH_USER = "root";
	public static final String SSH_PSWD = "root";
	public static final String SSH_FORMAT = " -ssh " + SSH_USER + "@%s -pw " + SSH_PSWD;
	// RPI PATHS
	public static final String CMD_SLOTCAR = "sudo /bin/bash /slotcar/slotcar.sh ";
	public static final String BASH = "sudo bash ";
	public static final String HOME = "/slotcar/";
	public static final String CAR_CONF = HOME + "slotcar.conf";
	public static final String CAR_BIN = HOME + "slotcar_stm.bin";
	public static final String CAR_JAR = HOME + "SlotcarPi.jar";
	public static final String CONTROLLERS = HOME + "controllers/";
	public static final String EXP_FLASH = HOME + "experiments/";
	public static final String EXP_RAM = "/tmp/slotcar/experiments/";
	// RPI BASH COMMANDS
	public static final String CMD_START = CMD_SLOTCAR + "start";
	public static final String CMD_STOP = CMD_SLOTCAR+ "stop";
	public static final String CMD_STATUS = CMD_SLOTCAR + "status";
	public static final String CMD_RESTART = CMD_SLOTCAR + "restart";
	public static final String CMD_ADDR = CMD_SLOTCAR + "addr";
	public static final String CMD_UPGRADE = CMD_SLOTCAR + "upgrade";

	public static final String CMD_STM = "sudo "+ HOME + "stm32boot";
	public static final String CMD_STM_FLASH = "sudo "+ CMD_STM + " -p " + CAR_BIN + " 0x08000000";
	public static final String CMD_STM_RESTART = "sudo "+ CMD_STM + " -r";

	public static final String CMD_SHUTDOWN = "sudo shutdown 0";
	public static final String CMD_REBOOT = "sudo reboot";
	public static final String CMD_RM_CTRLS = "sudo rm " + CONTROLLERS + "*.jar";

	
	public static final int NET_LOSS_MAX = 1;
	public static final int NET_MEAN_MAX = 30;
	public static final int NET_VARIANCE_MAX = 10;
	

	
	/*
	 * NETWORK
	 */
	public static final byte DEFAULT_ADDR = 0;
	public static final byte ADDR_SERVER = (byte) 100;
	public static final byte ADDR_BROADCAST = (byte) 255;
	public static final int APP_COM_PORT = 10457;
	public static final int MATLAB_COM_PORT = 9877;
	
	/*
	 * GUI
	 */
	
	public static final Color COLOR_TRUE = new Color(90, 206, 32);
	public static final Color COLOR_FALSE = new Color(255, 73, 34);
	public static final Color SELECTED_CAR_COLOR = new Color(128, 128, 128);
	public static final Color UNSELECTED_CAR_COLOR = new Color(255, 255, 255);
	public static final String GUI_CONF = "./gui.conf";
	
	public static Color getColor(boolean value) {
		return value ? COLOR_TRUE : COLOR_FALSE;
	}
	
	public static Color getColor(int value) {
		return value != 0 ? COLOR_TRUE : COLOR_FALSE;
	}
	
	static int syncPreference = 1;
	static boolean onlineSync = true;

	

	

	/*
	 * 
	 */
	
	private static Config config;

	private Config() {
		super();
	}

	public static Config getInstance() {
		if (config == null) {
			config = new Config();
		}
		return config;
	}

	public static File getBin() {
		return getInstance().getFile("binPath");
	}

	public static File getJar() {
		return getInstance().getFile("jarPath");
	}

	public static File getCtr() {
		return getInstance().getFile("ctrPath");
	}

	public static void clearIpList() {
		setIpList("");
	}

	public static String getIpList() {
		return getInstance().get("IPLIST");
	}

	public static void setIpList(String ipList) {
		getInstance().setProperty("IPLIST", ipList);
	}

	public static void addIp(String ip) {
		getInstance().add("IPLIST", ip);
	}

	public static void setCtrPath(String path) {
		getInstance().set("ctrPath", path);
	}

	/**
	 * @return logical address, if in configuration
	 * OR
	 * @return default address 0
	 * */
	public static byte getAddr() {
		return getInstance().getByte("LogAddr", (byte) 0);
	}

	public long getInteger(String key, int defaultValue) {
		String s = get(key);
		return s == null ? defaultValue : Long.parseLong(s);
	}

//	public static String getControllersConfigPath() {
//		return getInstance().getProperty("ControllerConfigDir", "./controllers");
//	}

	public static File getControllersPath() {
		String path = getInstance().getProperty("ControllersDir", "/slotcar/controllers");
		File dir = new File(path);
		if (!dir.exists()) {
			dir.mkdirs();
		}
		return dir;
	}

	public static String getControllersPaths() {
		return getInstance().getProperty("CONTROLLERS","./controllers");
	}

	public static String getLogFile() {
		return getInstance().get("LogFile", "/var/log/slotcar.log");
	}

	public static String getPutty() {
		return getInstance().get("PUTTY");
	}

	public static void setPutty(String path) {
		getInstance().setProperty("PUTTY", path);
	}

	public static int getSyncPreference() {
		return syncPreference;
	}

	public static void setSyncPreference(int syncPreference) {
		Config.syncPreference = syncPreference;
	}

	public static boolean isOnlineSync() {
		return onlineSync;
	}

	public static void setOnlineSync(boolean onlineSync) {
		Config.onlineSync = onlineSync;
	}

}
