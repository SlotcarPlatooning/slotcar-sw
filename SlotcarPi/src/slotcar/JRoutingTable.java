package slotcar;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTable;
import javax.swing.event.TableModelEvent;

public class JRoutingTable extends JTable{
	
	private static final long serialVersionUID = 5824239250048914736L;
	
	private final RoutingTableModel m;
	
	
	
	public JRoutingTable(RoutingTableModel m) {
		this.m = m;
		setModel(m);
	}
	
	public void selectUpDevs(){
		clearSelection();
		for(int i = 0; i < m.getRowCount() ; i++){
			if(CarList.getInstance().get(m.getAddrAt(i)) != null ){
				addRowSelectionInterval(i, i);
			}
		}
	}
	
	public List<Byte> getSelectedAddrAll() {
		ArrayList<Byte> addresses = new ArrayList<>();
		for (int i : getSelectedRows()) {
			addresses.add( m.getAddrAt(i));
		}
		return addresses;
	}
	
	public List<InetAddress> getSelectedIpAll() {
		ArrayList<InetAddress> iplist = new ArrayList<>();

		for (int i : getSelectedRows()) {
			iplist.add(m.getIpAt(i));
		}
		return iplist;
	}
	
	public void selectDev(Byte addr){
		int row = m.getRow(addr);
		if(row > 0){
			addRowSelectionInterval(row, row);
		}
	}
	
	@Override
	public void tableChanged(TableModelEvent e) {
		List<Byte> sel = getSelectedAddrAll();
		super.tableChanged(e);
		selectDev(sel);
		
	}
	
	public void selectDev(List<Byte> addresses){
		for(Byte addr : addresses){
			selectDev(addr);
		}
	}
	

}
