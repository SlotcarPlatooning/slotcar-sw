package slotcar.gui;

import slotcar.Config;
import slotcar.car.CarImage;
import slotcar.gui.peripherals.CarParameter;
import slotcar.gui.peripherals.plot.Graph;
import slotcar.pi.controller.CarImageInterface;

public class ParamsGraphs {

        // Bacha zalexi na poradi !!!
	@SuppressWarnings("serial")
	public final static CarParameter[] params = {
			// first
			new CarParameter("Reference distance", 0.5f, 0.0f, Config.DISTANCE_MAX, 0.01f, "0.00 m") {

				@Override
				public void setter(CarImage car, float value) {
					car.getCarControl().setReferenceDistance(value);
				}

				@Override
				public float getter(CarImage car) {
					return car.getCarControl().getReferenceDistance();
				}
			},
			// second
			new CarParameter("Reference PWM", 0.0f, -Config.PWM_MAX, Config.PWM_MAX, 0.1f, "0.0 %") {

				@Override
				public void setter(CarImage car, float value) {
					car.getCarControl().setReferencePwm(value);
				}

				@Override
				public float getter(CarImage car) {
					return car.getCarControl().getReferencePwm();
				}
			},
			// third
			new CarParameter("Reference Speed", 0.0f, -Config.SPEED_MAX, Config.SPEED_MAX, 0.1f, "0.0 m/s") {

				@Override
				public void setter(CarImage car, float value) {
					car.getCarControl().setReferenceSpeed(value);
				}

				@Override
				public float getter(CarImage car) {
					return car.getCarControl().getReferenceSpeed();
				}
			},
			// fourth
			new CarParameter("Time Headway", 0.0f, 0.0f, 5.0f, 0.01f, "0.00 s") {

				@Override
				public void setter(CarImage car, float value) {
					car.getCarControl().setTimeHeadway(value);
				}

				@Override
				public float getter(CarImage car) {
					return car.getCarControl().getTimeHeadway();
				}
			},
			new CarParameter("Net Delay", 0.0f, 0.0f, Config.NET_MEAN_MAX, 1.0f, "0.00 ms") {

				@Override
				public void setter(CarImage car, float value) {
					car.getNetFaultParams().setForcedDelayMean(value);
				}

				@Override
				public float getter(CarImage car) {
					return car.getNetFaultParams().getForcedDelayMean();
				}
			},
			
		};
	
	

	public final static Graph[] graphs = {
			// first
			new Graph("Speed") {
				@Override
				public float getSample(CarImageInterface car) {
					return car.getCarMeasurement().getSpeed();
				}
			},
			// second
			new Graph("Distance Back") {
				@Override
				public float getSample(CarImageInterface car) {
					return car.getCarMeasurement().getDistanceBack();
				}
			},
			// third
			new Graph("Distance Front") {
				@Override
				public float getSample(CarImageInterface car) {
					return car.getCarMeasurement().getDistanceFront();
				}
			} };

}
