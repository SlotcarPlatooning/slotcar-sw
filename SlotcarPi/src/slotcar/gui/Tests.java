package slotcar.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.Timer;

import org.apache.commons.lang3.NotImplementedException;

import slotcar.car.CarImage;

public class Tests {
	
	private Timer timerSendCommTest = null;
	private Timer timerStopCommTest = null;
	
	/**
	 * The number of packets to be sent when doing a communication test
	 */
	int numOfCommTestPackets = 100;

	/**
	 * The number of received results of the communication test.When the
	 * communication test is over, the cars should report
	 */
	private int numberOfReceivedComTestResults = 0;
	
	/**
	 * Test how reliable the communication is. The test consists of sending a
	 * given number of packets and after the test finishes, the car returns how
	 * much packets was received.
	 * 
	 * @param list
	 *            List of cars to which the test should be done.
	 */
	public void commTest(List<CarImage> list) {
		throw new NotImplementedException("");
//		if (cars.size() > 0) {
//			if (list.size() > 0) {
//				System.out.println("Starting the communication test.");
//				numberOfReceivedComTestResults = 0;
//				// send start communication test to all selected cars
//
//				comMan.sendCommTestStart(list);
//				System.out.println("Sending packets in the communication test.");
//				timerSendCommTest = new Timer(1000, new ActionListener() {
//					public void actionPerformed(ActionEvent evt) {
//						System.out.println("Ending the communication test.");
//						comMan.sendCommTestPackets(numOfCommTestPackets);
//						timerSendCommTest.stop();
//					}
//				});
//				timerSendCommTest.start();
//				// comMan.sendCommTestPackets(numOfCommTestPackets);
//
//				timerStopCommTest = new Timer(2500, new ActionListener() {
//					public void actionPerformed(ActionEvent evt) {
//						System.out.println("Ending the communication test.");
//						comMan.sendCommTestEnd(list);
//						timerStopCommTest.stop();
//					}
//				});
//				timerStopCommTest.start();
//
//			} else {
//				JOptionPane.showMessageDialog(mFrame, "At least one car has to be selected for test.");
//			}
//		} else {
//			JOptionPane.showMessageDialog(mFrame,
//					"At least one car has to be connected to the network in order to test the communication.");
//		}
	}
	
void commTesthandleResults(){
	throw new NotImplementedException("");
//	CarImage car = cars.get(sourceAddr);
//
//	car.setReceivedCommTestPackets(numberOfPackets);
//	numberOfReceivedComTestResults++;
//	if (numberOfReceivedComTestResults == carSelectionCtr.getSelectedCars().size()) {
//		StringBuilder builder = new StringBuilder();
//		builder.append("The test procedure finished with the following reception results. Out of ")
//				.append(numOfCommTestPackets).append(" packets the cars received:\r\n");
//		for (CarImage carIm : carSelectionCtr.getSelectedCars()) {
//			CarImage cc = cars.get(carIm.getLogAddr());
//			builder.append("Car ").append(cc.getLogAddr()).append(": ").append(cc.getReceivedCommTestPackets())
//					.append(" => ").append(cc.getReceivedCommTestPackets() / (double) numOfCommTestPackets * 100)
//					.append("%");
//			builder.append("\r\n");
//		}
//		String str = builder.toString();
//		JOptionPane.showMessageDialog(mFrame, str);
//	}
}

}
