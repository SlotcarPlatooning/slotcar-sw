package slotcar.gui.peripherals.plot;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;

public class ExperimentV extends JPanel {

	private static final long serialVersionUID = 2672049482462717458L;
	
	private JButton btnStart;
	private JButton btnStop;
	private JButton btnPositioning;
	
	private JPanel plotSelectionPanel;

	public ExperimentV() {

		setLayout(new BorderLayout(0, 0));

		JPanel buttonsPanel = new JPanel();
		add(buttonsPanel, BorderLayout.SOUTH);




		plotSelectionPanel = new JPanel();
		add(plotSelectionPanel, BorderLayout.CENTER);
		plotSelectionPanel.setLayout(new BoxLayout(plotSelectionPanel, BoxLayout.Y_AXIS));

	}

	public void addBtnStartActionListener(ActionListener l) {
		btnStart.addActionListener(l);
	}

	public void addBtnStopActionListener(ActionListener l) {
		btnStop.addActionListener(l);
	}

	public void addBtnPositioningActionListener(ActionListener l) {
		btnPositioning.addActionListener(l);
	}

	public void addCbox(JCheckBox checkBox) {
		plotSelectionPanel.add(checkBox);
	}
	
	public void addSelectionItem(JPanel item){
		plotSelectionPanel.add(item);
	}
}
