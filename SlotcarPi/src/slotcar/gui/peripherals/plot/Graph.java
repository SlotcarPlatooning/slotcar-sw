package slotcar.gui.peripherals.plot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

import com.xeiam.xchart.Chart;
import com.xeiam.xchart.ChartBuilder;
import com.xeiam.xchart.StyleManager.ChartTheme;
import com.xeiam.xchart.XChartPanel;

import slotcar.pi.controller.CarImageInterface;

public abstract class Graph {

	private HashMap<Byte, List<Float>> data, time;

	private boolean enabled;
	private final String name;
	private boolean active;

	private final JCheckBox checkBox;

	private Chart chart;
	private XChartPanel chartPanel;

	public Graph(String name) {
		this.data = new HashMap<>();
		this.time = new HashMap<>();
		this.name = name;
		checkBox = new JCheckBox(name);
		this.checkBox.addChangeListener((e) -> {
			enabled = checkBox.isSelected();
		});
	}

	public void init(byte[] cars) {
		if (!enabled || !active) {
			System.out.println("Graph " + name + " was NOT selected");
			return;
		} else {
			System.out.println("Graph " + name + " was selected");

		}

		if (cars == null || cars.length == 0) {
			System.out.println("No cars were selected");
			return;
		}

		chart = buildChart();
		chart.setChartTitle(name);
		
		data.clear();
		time.clear();

		for (byte b : cars) {
			// Initialization
			data.put(b, new LinkedList<>());
			time.put(b, new LinkedList<>());
			// put default values
			data.get(b).add(0.0f);
			time.get(b).add(0.0f);
			// adding series
			chart.addSeries(String.valueOf(b), time.get(b), data.get(b));
		}

		chartPanel = new XChartPanel(chart);
		javax.swing.SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {

				// Create and set up the window.
				JFrame frame = new JFrame("XChart - " + name);
				frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				frame.getContentPane().add(chartPanel);

				// Display the window.
				frame.pack();
				frame.setVisible(true);
			}
		});
	}

	public boolean isEnabled() {
		return enabled;
	}

	public JCheckBox getCheckBox() {
		return checkBox;
	}

	public abstract float getSample(CarImageInterface car);

	public void makeSample(byte addr, CarImageInterface car, float time){
		if(enabled && active){
			this.data.get(addr).add(getSample(car));
			this.time.get(addr).add(time);
		}
	}
	
	private void updatePlot(byte addr) {
		if (enabled && active) {
			chartPanel.updateSeries(String.valueOf(addr), time.get(addr), data.get(addr));
			List<Float> list = time.get(addr);
			float max = list.get(list.size() - 1);
			chart.getStyleManager().setXAxisMax(max);
			chart.getStyleManager().setXAxisMin(max > 10 ? max - 10 : 0);
		}
	}
	
	public void updatePlotAll() {
		for (Byte logAddr : data.keySet()) {
			updatePlot(logAddr);
		}
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
		checkBox.setEnabled(!active);
	}

	static Chart buildChart() {
		ChartBuilder chartBuilder = new ChartBuilder();
		chartBuilder.width(400);
		chartBuilder.height(500);
		chartBuilder.theme(ChartTheme.Matlab);
		chartBuilder.xAxisTitle("Time");
		return chartBuilder.build();
	}

	public JPanel getSelectionPanel() {
		JPanel p = new JPanel();
		p.add(checkBox);
		return p;
	}
}
