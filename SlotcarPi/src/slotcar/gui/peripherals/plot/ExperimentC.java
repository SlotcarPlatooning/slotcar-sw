package slotcar.gui.peripherals.plot;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.Timer;

import slotcar.gui.MainApp;
import slotcar.pi.controller.CarImageInterface;

public class ExperimentC {

	private boolean running;
	private long samplingPeriod; // [ms]

	private final List<Graph> graphs;

	private Timer updatePlotTimer;
	private long timeOfStart;

	public long getPlotSamplePeriod() {
		return samplingPeriod;
	}

	public void setPlotSamplePeriod(long plotSamplePeriod) {
		this.samplingPeriod = plotSamplePeriod;
	}

	public void startPlotting(byte[] cars) {
		timeOfStart = System.currentTimeMillis();
		updatePlotTimer = new Timer((int) samplingPeriod, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				for (Graph g : graphs) {
					g.updatePlotAll();

					for (byte addr : cars) {
						makeSample(addr);
					}
				}
			}
		});

		for (Graph g : graphs) {
			g.setActive(true);
			g.init(cars);
		}

		running = true;
		updatePlotTimer.start();
		System.out.println("Plot has started");

	}

	public void makeSample(byte logAddr) {
		CarImageInterface car = MainApp.cars.getByAddr(logAddr);
		float diff = (float) (System.currentTimeMillis() - timeOfStart) / 1000.0f;
		for (Graph g : graphs) {
			g.makeSample(logAddr, car, diff);
		}
	}

	public void stopPlotting() {
		try {
			updatePlotTimer.stop();
		} catch (NullPointerException ex) {
			System.err.println("No plot to stop");
		}
		for (Graph g : graphs) {
			g.setActive(false);
		}
		running = false;
		JOptionPane.showMessageDialog(MainApp.getInstance().getFrame(), "Plot has stopped");
	}

	public void saveGraph() {
		// TODO: needs to be implemented
	}

	public boolean isRunning() {
		return running;
	}

	ExperimentV view;

	public void addGraph(Graph g) {
		graphs.add(g);
		view.addSelectionItem(g.getSelectionPanel());
	}

	public ExperimentC(ExperimentV view, Graph[] graphs) {
		samplingPeriod = 50;

		this.view = view;

		this.graphs = new ArrayList<>();
		for (Graph graph : graphs) {
			addGraph(graph);
		}


	}

	
}
