package slotcar.gui.peripherals;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import slotcar.gui.MainApp;

public class ManualControl implements KeyListener {
	
	private MainApp app;

	private boolean is_up;
	private boolean is_down;
	
	
	 public ManualControl(MainApp app) {
	        this.app = app;
	 }

	@Override
	public void keyPressed(KeyEvent e) {
//		System.out.println("Key pressed");
		
		switch(e.getKeyCode()){
		case KeyEvent.VK_UP :
			if(!is_up){
				System.out.println("UP pressed");
				is_up = true;
				app.sendManualSpeed(0.5f);
			}
			break;
		case KeyEvent.VK_DOWN :
			if(!is_down){
				System.out.println("DOWN pressed");
				is_down = true;
				app.sendManualSpeed(-0.5f);
			}
			break;
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {

//		System.out.println("Key released");
		switch(e.getKeyCode()){
		case KeyEvent.VK_UP :
			if(is_up){
				System.out.println("UP released");
				is_up = false;
				app.sendManualSpeed(0.0f);
			}
			
			break;
		case KeyEvent.VK_DOWN :
			if(is_down){
				is_down = false;
				System.out.println("DOWN released");
				app.sendManualSpeed(0.0f);
			}
			break;
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {

	}
}
