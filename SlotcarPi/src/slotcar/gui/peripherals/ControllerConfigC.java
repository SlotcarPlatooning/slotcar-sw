package slotcar.gui.peripherals;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import slotcar.pi.controller.Controller;

public class ControllerConfigC {
		
	@SuppressWarnings("unused")
	private static final long serialVersionUID = -5827297293555391677L;

	private ControllerConfigV view;

	public ControllerConfigC(ControllerConfigV view) {
		this.view = view;
	}

	public void setController(Controller controller) {
		if (controller == null) {
			System.err.println("Controller is null.");
			return;
		}
		
		view.setName(controller.getName());
		view.setDescription(controller.getDescription());
		view.clearList();
		
		for (Field f : controller.getClass().getFields()) {
			view.addPanel(new ControllerConstant(f, controller));
		}
	}
	
	class ControllerConstant extends JPanel {
		
		private static final long serialVersionUID = -6117823814601882030L;

		private Field field;
		private Controller controller;
		private JLabel name;
		private JTextField tf;

		public ControllerConstant(Field field, Controller controller) {
			this.field = field;
			this.controller = controller;
		
			// NAME
			setLayout(new FlowLayout());
			name = new JLabel(Controller.getFieldLabel(field));
			name.setToolTipText(Controller.getFieldDescription(field));
			add(name);
			
			// TEXTFIELD
			tf = new JTextField(10);
			tf.setToolTipText(Controller.getFieldDescription(field));
			tf.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO kontrolovat hodnoty
					updateToController();
					updateFromController();
				}
			});
			add(tf);
			
			updateFromController();
		}

		private void updateToController() {
			try {
				controller.string2field(field, tf.getText());
			} catch (IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}

		private void updateFromController() {
			try {
				this.tf.setText(controller.field2String(field));
			} catch (IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}
	}
}
