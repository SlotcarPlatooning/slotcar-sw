package slotcar.gui.peripherals;

import java.awt.Color;
import java.awt.FlowLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JSpinner.NumberEditor;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import slotcar.Config;
import slotcar.car.CarImage;

public abstract class CarParameter extends JPanel{

	private static final long serialVersionUID = 2958181022880818132L;

	private final JSlider slider = new JSlider();
	private final JSpinner spinner = new JSpinner();
	private final JLabel label = new JLabel();
	
	public final String name;
	public final String format;
	public final float min;
	public final float max;
	public final float step;
	
	private volatile float value;

	private final List<ChangeListener> listener = new ArrayList<>();

	public CarParameter(String name, float value, float min, float max, float step, String format) {
		this.format = format;
		this.name = name;
		this.min = min;
		this.max = max;
		this.step = step;
		
		setLayout(new FlowLayout());
		setBackground(Color.WHITE);
		
		// Spinner initialization
		spinner.setModel(new SpinnerNumberModel(new Float(getValue()),new Float(min),new Float(max),new Float(step)));
		spinner.setEditor(new NumberEditor(spinner, format));
		spinner.addChangeListener((e) -> {
			setValue((float) spinner.getValue());
		});
		add(spinner);
		
		// Slider initialization
		slider.setMinimum(normalizeValue(min));
		slider.setMaximum(normalizeValue(max));
		slider.setValue(normalizeValue(getValue()));
		slider.setPaintLabels(true);
		slider.addChangeListener((e) -> {
			setValue(min + step * (float) slider.getValue());
		});
		add(slider);

		// Label initialization
		label.setText(name);
		add(label);
	}

	private void callChangeListeners() {
		for (ChangeListener l : listener) {
			l.stateChanged(new ChangeEvent(this));
		}
	}

	public void addChangeListener(ChangeListener e) {
		listener.add(e);
	}

	@Override
	public void setBackground(Color bg) {
		super.setBackground(bg);
		try {
			slider.setBackground(bg);
			spinner.setBackground(bg);
			label.setBackground(bg);
		} catch (NullPointerException e) {

		}
	}

	public int compare(CarImage car) {

		int ret = Float.compare(getValue(), getter(car));
		if (ret == 0) {
			setBackground(Config.COLOR_TRUE);
		} else {
			setBackground(Config.COLOR_FALSE);
		}

		revalidate();
		return ret;
	}

	private int normalizeValue(float value) {
		return (int) Math.round((value - min) / step);
	}
	
	public abstract float getter(CarImage car);
	
	public abstract void setter(CarImage car, float value);

	public void writeTo(CarImage car){
		setter(car, getValue());
	}
	
	public void readFrom(CarImage car){
		setValue(getter(car));
	}
	
	public synchronized void setValue(float value) {
		if( this.value != value){
			this.value = value;	
			slider.setValue(normalizeValue(value));
			spinner.setValue(new Float(value));
			callChangeListeners();
		}
	}
	
	public synchronized float getValue(){
		return this.value;
	}
}
