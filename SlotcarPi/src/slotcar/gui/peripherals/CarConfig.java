package slotcar.gui.peripherals;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import slotcar.Controllers;
import slotcar.car.CarImage;
import slotcar.gui.MainApp;
import slotcar.pi.controller.Controller;

public class CarConfig extends JPanel {

    private static final long serialVersionUID = -3495834585727839937L;

    static final Color BACKGROUND = Color.orange;

    List<ChangeListener> changeListeners = new ArrayList<>();

    protected JPanel parametersListPanel = new JPanel();
    protected JScrollPane scrollPane = new JScrollPane(parametersListPanel);
    protected JPanel buttonsPanel = new JPanel();
    protected JCheckBox cbReadOnClick = new JCheckBox("Read on Click");

    private ControllerSelectionC controllerSelectionC;
    private ControllerSelectionV controllerSelectionV;

    private List<CarParameter> paramsGui = new ArrayList<>();

    private final CarParameter[] params;

    public CarConfig(final MainApp mainApp, final CarParameter[] params) {
        setLayout(new BorderLayout());

        /* Adding car controller selection */
        controllerSelectionV = new ControllerSelectionV();
        controllerSelectionC = new ControllerSelectionC(controllerSelectionV);

        /* Adding parameters */
        parametersListPanel.setLayout(new GridLayout(0, 1));
        this.params = params;
        for (CarParameter p : params) {
            p.addChangeListener((e) -> {
                callChangeListeners();
            });
            paramsGui.add(p);
            parametersListPanel.add(p);
        }

        // colors
        buttonsPanel.setBackground(BACKGROUND);
        cbReadOnClick.setBackground(BACKGROUND);

        // adding
        add(controllerSelectionV, BorderLayout.NORTH);
        add(scrollPane, BorderLayout.CENTER);
        add(buttonsPanel, BorderLayout.SOUTH);
        buttonsPanel.add(cbReadOnClick);

        controllerSelectionC.addControllerChangeListner((e) -> {
            callChangeListeners();
        });
    }

    public Controller getSelectedController() {
        return controllerSelectionC.getSelected();
    }

    private void callChangeListeners() {
        for (ChangeListener l : changeListeners) {
            l.stateChanged(new ChangeEvent(this));
        }
    }

    public void addChangeListener(ChangeListener l) {
        changeListeners.add(l);
    }

    public void addReadOnClickActionListener(ActionListener l) {
        cbReadOnClick.addActionListener(l);
    }

    public boolean isReadOnClick() {
        return cbReadOnClick.isSelected();
    }

    public Controllers getControllers() {
        return controllerSelectionC.controllers;
    }

    public void addBtn(String name, ActionListener l) {
        JButton btn = new JButton(name);
        btn.setBackground(BACKGROUND);
        btn.addActionListener(l);
        this.buttonsPanel.add(btn);
    }

    public boolean compare(CarImage car) {
        boolean ret = true;
        for (CarParameter p : paramsGui) {
            ret &= p.compare(car) != 0;
        }
        ret &= controllerSelectionC.compare(car);
        return ret;
    }

    public void readFrom(CarImage car) {
        for (CarParameter p : params) {
            p.readFrom(car);
        }
        controllerSelectionC.readFrom(car);
    }

    public void writeTo(CarImage car) {
        for (CarParameter p : params) {
            p.writeTo(car);
        }
        controllerSelectionC.writeTo(car);
        compare(car);
    }

    public void writeParamsTo(CarImage car) {
        for (CarParameter p : params) {
            p.writeTo(car);
        }
        compare(car);
    }

    public void update(CarImage car, boolean foo) {
        //controllerSelectionC.update(car, foo);
//		if(foo){
//			readFrom(car);
//		}
        compare(car);
    }
}
