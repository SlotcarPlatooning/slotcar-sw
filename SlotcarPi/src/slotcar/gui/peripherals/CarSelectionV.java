package slotcar.gui.peripherals;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.MatteBorder;

import slotcar.Config;
import slotcar.car.CarImage;

public class CarSelectionV extends JPanel {

	private static final long serialVersionUID = -3966058224882105405L;

	private List<CarSelectionItem> carItems = new ArrayList<>();
	MouseListener itemClickListener;
	JScrollPane scrollPane;

	public CarSelectionV(JControlPanel jControlPanel) {
		setLayout(new BorderLayout(0, 0));
		scrollPane = new JScrollPane();
		
		add(scrollPane, BorderLayout.CENTER);
		add(jControlPanel, BorderLayout.NORTH);

	}

	public void updateSelection(List<CarImage> selectedCars) {
		
		for (CarSelectionItem c : carItems) {
			if (selectedCars.contains(c.getCarImage())) {
				c.setSelected(true);
			} else {
				c.setSelected(false);
			}
		}
	}

	public void updateList(List<CarImage> cars, List<CarImage> selectedCars) {
		JPanel p = new JPanel();
		p.setLayout(new GridBagLayout());
	
		carItems.clear();

		int groupCounter = 1;

		HashMap<Integer, Integer> groups = new HashMap<>();

		int gridy = 0;
		for (CarImage carImage : cars) {
			int hash = carImage.getCarControl().hashCode();
			
			if (!groups.containsKey(hash)) {
				groups.put(hash, groupCounter);
				groupCounter++;
			}

			boolean isSelected = selectedCars.contains(carImage);
			int groupId = groups.get(hash);
			
			CarSelectionItem c = new CarSelectionItem(carImage, isSelected, false, groupId);
			
			//if(!carItems.contains(c)){
				carItems.add(c);
				
			    GridBagConstraints gbc = new GridBagConstraints();
	//            gbc.gridwidth = GridBagConstraints.BASELINE;
	       
	            gbc.gridheight = 1;
	            gbc.fill = GridBagConstraints.BOTH;
	            gbc.anchor = GridBagConstraints.CENTER;
	            gbc.weightx = 1;
	            gbc.weighty = 1;
	            gbc.gridx = 0;
	            gbc.gridy = gridy;
				p.add(c, gbc, 0);
				gridy++;
			//}
		}
		
		// add listener for each item on the list
		for (CarSelectionItem c : carItems) {
			c.addMouseListener(itemClickListener);
		}
		
		revalidate();
        repaint();
        scrollPane.setViewportView(p);
	}

	public void addItemClickListener(MouseAdapter m) {
		itemClickListener = m;
	}

	class CarSelectionItem extends JPanel {
		private static final long serialVersionUID = -6427466187981661990L;

		private CarImage car;
		JLabel lbName = new JLabel();
		JLabel lbStatus = new JLabel();
		JLabel lbGroup = new JLabel();
		JLabel lbSync = new JLabel();

		public void update(boolean selected, int group ){
			lbStatus.setText(car.getState().name());
			lbGroup.setText("group: " + group);
			setSelected(selected);
			
			int foo = car.getSyncStatus();
			
			String sync;
			if (foo < 0) {
				sync = "gui < car"; // image in gui is obsolete
			} else if (foo > 0) {
				sync = "gui > car"; // image in car is obsolete
			} else {
				sync = "gui == car"; // images are synchronized
			}
			
			lbSync.setText(sync);
		}
		
		public CarSelectionItem(CarImage carImage, boolean selected, boolean offline, int group) {
			this.car = carImage;

			BoxLayout boxLayout = new BoxLayout(this, BoxLayout.Y_AXIS);
			this.setLayout(boxLayout);
			this.setBorder(new MatteBorder(0, 0, 1, 0, Color.GRAY));

			// NAME
			lbName.setText("Car " + carImage.getLogAddr());
			lbName.setAlignmentX(Component.CENTER_ALIGNMENT);
			this.add(lbName);
			
			// STATUS
			lbStatus.setAlignmentX(Component.CENTER_ALIGNMENT);
			this.add(lbStatus);
			
			// GROUP
			lbGroup.setAlignmentX(Component.CENTER_ALIGNMENT);
			this.add(lbGroup);
			
			// SYNC STATUS
			lbSync.setAlignmentX(Component.CENTER_ALIGNMENT);
			this.add(lbSync);
			
			update(selected, group);
		}
		
		public void setSelected(boolean selected) {
			setBackground(selected ? Config.SELECTED_CAR_COLOR : Config.UNSELECTED_CAR_COLOR);
			revalidate();
		}

		public CarImage getCarImage() {
			return car;
		}
		
		@Override
		public boolean equals(Object obj) {
			if(obj instanceof CarSelectionItem){
				return ((CarSelectionItem)obj).car.equals(car);
			}
			return false;
		}

		@Override
		public int hashCode() {
			// TODO Auto-generated method stub
			return super.hashCode();
		}
	}
}
