package slotcar.gui.peripherals;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import net.miginfocom.swing.MigLayout;
import slotcar.car.CarImage;
import slotcar.gui.MainApp;
import slotcar.network.ServiceType;

import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.nio.ByteBuffer;
import java.util.List;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JCheckBox;

public class NetControl extends JPanel {

	private static final long serialVersionUID = -7589749253913526918L;

	private JTabbedPane tabbedPaneClock;

	private JTabbedPane tabbedPaneStat;
	private JCheckBox checkStatFile;
	private JButton buttonStartProbe;
	private JButton btnNewButton;
	private JLabel labelPS;
	private JTextField textPS;
	private JLabel labelIAT;
	private JTextField textIAT;
	/**
	 * Create the panel.
	 */
	public NetControl(MainApp mainApp) {
		
		setLayout(new MigLayout("", "[grow][grow]", "[100px,grow][fill][300px,grow][][fill]"));
		
		this.tabbedPaneClock = new JTabbedPane(JTabbedPane.TOP);
		
		add(tabbedPaneClock, "cell 0 0 2 1,grow");
		
		JButton buttonClock = new JButton("Clock statistics");
		buttonClock.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				List<CarImage> cars = mainApp.getCarSelection().getSelectedCars();
				mainApp.comMan.sendTo(cars, ServiceType.NET_CLOCK_LOG_REQ);
			}
		});
		add(buttonClock, "flowx,cell 0 1");
		
		this.tabbedPaneStat = new JTabbedPane(JTabbedPane.TOP);
		add(tabbedPaneStat, "cell 0 2 2 1,grow");
		
		JButton buttonStat = new JButton("Delay statistics");
		buttonStat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				List<CarImage> cars = mainApp.getCarSelection().getSelectedCars();
				mainApp.comMan.sendTo(cars, ServiceType.NET_STAT_LOG_REQ);	
			}
		});
		add(buttonStat, "flowx,cell 0 3");
		
		checkStatFile = new JCheckBox("Dump to file");
		add(checkStatFile, "cell 1 3");
		
		buttonStartProbe = new JButton("Probe");
		buttonStartProbe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ByteBuffer bb = ByteBuffer.allocate(8);
				bb.putInt(Integer.parseInt(textPS.getText()));
				bb.putInt(Integer.parseInt(textIAT.getText()));
				
				mainApp.comMan.sendTo(mainApp.getCarSelection().getSelectedCars(),
						ServiceType.NET_START_PROBE, bb.array());
			}
		});
		add(buttonStartProbe, "flowx,cell 0 4");
		
		labelPS = new JLabel("PS [B]");
		add(labelPS, "flowx,cell 1 4");
		
		textPS = new JTextField();
		add(textPS, "cell 1 4");
		textPS.setColumns(4);
		
		labelIAT = new JLabel("IAT [ms]");
		add(labelIAT, "cell 1 4");
		
		textIAT = new JTextField();
		add(textIAT, "cell 1 4");
		textIAT.setColumns(3);
		
		btnNewButton = new JButton("Stop");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				mainApp.comMan.sendTo(mainApp.getCarSelection().getSelectedCars(), ServiceType.NET_STOP_PROBE);
			}
		});
		add(btnNewButton, "cell 0 4");
		
		JButton btnCloseClock = new JButton("x");
		btnCloseClock.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Component selected = tabbedPaneClock.getSelectedComponent();
                if (selected != null) {

                    tabbedPaneClock.remove(selected);
                }
            }
		});
		add(btnCloseClock,"cell 0 1");
		
		JButton btnCloseStat = new JButton("x");
		btnCloseStat.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Component selected = tabbedPaneStat.getSelectedComponent();
                if (selected != null) {

                    tabbedPaneStat.remove(selected);
                }
            }
		});
		add(btnCloseStat,"cell 0 3");

	}
	
	public void setClockLog(int carNum, String text) {
		JTextArea textArea = new JTextArea(text);
		textArea.setEditable(false);
		textArea.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
		JScrollPane jsp = new JScrollPane(textArea);
		tabbedPaneClock.addTab("Car "+carNum, null, jsp, null);
	}

	public void setStatLog(int carNum, String text) {
		JTextArea textArea = new JTextArea(text);
		textArea.setEditable(false);
		textArea.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
		JScrollPane jsp = new JScrollPane(textArea);
		tabbedPaneStat.addTab("Car "+carNum, null, jsp, null);
	}

	public boolean isDumpEnabled() {
		return checkStatFile.isSelected();
	}
} 
