package slotcar.gui.peripherals;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.Serializable;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ListModel;
import javax.swing.border.LineBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import slotcar.Config;
import slotcar.JRoutingTable;
import slotcar.RoutingTable;

public class CarUpgradeV extends JPanel implements Serializable {
	private static final long serialVersionUID = -6450009192085074892L;

	protected JPanel deviceListPanel;
	protected JPanel firmwarePanel;
	protected JPanel panel;
	protected JButton btnCtrPath;
	protected JButton btnBinPath;
	protected JButton btnJarPath;

	protected JCheckBox cbBin;
	protected JCheckBox cbJar;
	protected JCheckBox cbCtr;
	protected JCheckBox cbResetAfrerLoad;
	protected JCheckBox cbFlashAfterLoad;
	protected JCheckBox cbRemovePrevious;
	private JTextArea textArea;
	protected JRoutingTable tbDevices;

	public void setDevicesListModel(ListModel<IpListItem> model) {
		// devicesList.setModel(model);
	}

	public void textPrintln(String s) {
		textArea.setText(textArea.getText() + s);
	}
	
	int gridy = 0;
	
	public void addButton(String name, ActionListener l){
		GridBagConstraints gbc_restartButton = new GridBagConstraints();
		gbc_restartButton.fill = GridBagConstraints.BOTH;
		gbc_restartButton.gridwidth = 2;
		gbc_restartButton.insets = new Insets(0, 0, 5, 0);
		gbc_restartButton.gridx = 0;
		gbc_restartButton.gridy = gridy++;
		JButton b = new JButton(name);
		b.addActionListener(l);
		firmwarePanel.add(b, gbc_restartButton);
	}

	public CarUpgradeV() {
		firmwarePanel = new JPanel();
		setLayout(new BorderLayout(0, 0));

		deviceListPanel = new JPanel();

		this.add(deviceListPanel, BorderLayout.WEST);
		deviceListPanel.setLayout(new BorderLayout(0, 0));

		panel = new JPanel();
		deviceListPanel.add(panel, BorderLayout.NORTH);
		FlowLayout fl_panel = new FlowLayout(FlowLayout.CENTER, 5, 5);
		panel.setLayout(fl_panel);

		tbDevices = new JRoutingTable(RoutingTable.getModel());
		tbDevices.setBorder(new LineBorder(new Color(0, 0, 0)));
		deviceListPanel.add(tbDevices, BorderLayout.CENTER);

		JScrollPane fpScrollPane = new JScrollPane(firmwarePanel);

		this.add(fpScrollPane, BorderLayout.CENTER);

		/* Device Panel */

		GridBagLayout gbl_firmwarePanel = new GridBagLayout();
		gbl_firmwarePanel.columnWidths = new int[] { 0, 0, 0 };
		gbl_firmwarePanel.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gbl_firmwarePanel.columnWeights = new double[] { 0.0, 0.0, Double.MIN_VALUE };
		gbl_firmwarePanel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				Double.MIN_VALUE };
		firmwarePanel.setLayout(gbl_firmwarePanel);

		cbCtr = new JCheckBox("Controllers");
		GridBagConstraints gbc_chckbxControllers = new GridBagConstraints();
		gbc_chckbxControllers.fill = GridBagConstraints.BOTH;
		gbc_chckbxControllers.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxControllers.gridx = 0;
		gbc_chckbxControllers.gridy = gridy;
		firmwarePanel.add(cbCtr, gbc_chckbxControllers);

		btnCtrPath = new JButton("Path");

		GridBagConstraints gbc_btnControllersPath = new GridBagConstraints();
		gbc_btnControllersPath.fill = GridBagConstraints.BOTH;
		gbc_btnControllersPath.insets = new Insets(0, 0, 5, 0);
		gbc_btnControllersPath.gridx = 1;
		gbc_btnControllersPath.gridy = gridy++;
		firmwarePanel.add(btnCtrPath, gbc_btnControllersPath);

		cbRemovePrevious = new JCheckBox("Remove previous controllers");
		GridBagConstraints gbc_cbRemovePrevious = new GridBagConstraints();
		gbc_cbRemovePrevious.fill = GridBagConstraints.BOTH;
		gbc_cbRemovePrevious.gridwidth = 2;
		gbc_cbRemovePrevious.insets = new Insets(0, 0, 5, 0);
		gbc_cbRemovePrevious.gridx = 0;
		gbc_cbRemovePrevious.gridy = gridy++;
		firmwarePanel.add(cbRemovePrevious, gbc_cbRemovePrevious);

		cbBin = new JCheckBox("STM firmware");
		GridBagConstraints gbc_binCB = new GridBagConstraints();
		gbc_binCB.fill = GridBagConstraints.BOTH;
		gbc_binCB.insets = new Insets(0, 0, 5, 5);
		gbc_binCB.gridx = 0;
		gbc_binCB.gridy = gridy;
		firmwarePanel.add(cbBin, gbc_binCB);
		this.btnBinPath = new JButton("Path");

		GridBagConstraints gbc_binPathButton = new GridBagConstraints();
		gbc_binPathButton.fill = GridBagConstraints.BOTH;
		gbc_binPathButton.insets = new Insets(0, 0, 5, 0);
		gbc_binPathButton.gridx = 1;
		gbc_binPathButton.gridy = gridy++;
		firmwarePanel.add(btnBinPath, gbc_binPathButton);

		cbJar = new JCheckBox("RPI firmware");
		GridBagConstraints gbc_jarCB = new GridBagConstraints();
		gbc_jarCB.fill = GridBagConstraints.BOTH;
		gbc_jarCB.insets = new Insets(0, 0, 5, 5);
		gbc_jarCB.gridx = 0;
		gbc_jarCB.gridy = gridy;
		firmwarePanel.add(cbJar, gbc_jarCB);

		this.btnJarPath = new JButton("Path");

		GridBagConstraints gbc_jarPathButton = new GridBagConstraints();
		gbc_jarPathButton.fill = GridBagConstraints.BOTH;
		gbc_jarPathButton.insets = new Insets(0, 0, 5, 0);
		gbc_jarPathButton.gridx = 1;
		gbc_jarPathButton.gridy = gridy++;
		firmwarePanel.add(btnJarPath, gbc_jarPathButton);

	

		cbFlashAfterLoad = new JCheckBox("Flash STM after load?");
		GridBagConstraints gbc_flashAfterLoadCB = new GridBagConstraints();
		gbc_flashAfterLoadCB.fill = GridBagConstraints.HORIZONTAL;
		gbc_flashAfterLoadCB.gridwidth = 2;
		gbc_flashAfterLoadCB.insets = new Insets(0, 0, 5, 0);
		gbc_flashAfterLoadCB.gridx = 0;
		gbc_flashAfterLoadCB.gridy = gridy++;
		firmwarePanel.add(cbFlashAfterLoad, gbc_flashAfterLoadCB);

		cbResetAfrerLoad = new JCheckBox("Restart after load?");
		GridBagConstraints gbc_resetAfrerLoadCB = new GridBagConstraints();
		gbc_resetAfrerLoadCB.fill = GridBagConstraints.BOTH;
		gbc_resetAfrerLoadCB.gridwidth = 2;
		gbc_resetAfrerLoadCB.insets = new Insets(0, 0, 5, 0);
		gbc_resetAfrerLoadCB.gridx = 0;
		gbc_resetAfrerLoadCB.gridy = gridy++;
		firmwarePanel.add(cbResetAfrerLoad, gbc_resetAfrerLoadCB);




	





		textArea = new JTextArea();
		textArea.setEditable(false);
		textArea.setFont(new Font("Monospaced", Font.PLAIN, 12));
		textArea.setRows(10);

		JScrollPane sp = new JScrollPane(textArea);

		add(sp, BorderLayout.SOUTH);
	}

	public void addBtnJarPathActionListener(ActionListener l) {
		btnJarPath.addActionListener(l);
	}

	public void addBtnCtrPathActionListener(ActionListener l) {
		btnCtrPath.addActionListener(l);
	}

	public void addBtnBinPathActionListener(ActionListener l) {
		btnBinPath.addActionListener(l);
	}

	public File chooseFile(String extension, String fileDescription, String confPrevFile) {

		JFileChooser fc = new JFileChooser();

		try {
			String s = Config.getInstance().get(confPrevFile);
			File f = new File(s);
			fc.setSelectedFile(f);
		} catch (Exception e) {
			// do nothing
		}

		fc.setFileFilter(new FileNameExtensionFilter(fileDescription, extension));
		fc.setMultiSelectionEnabled(false);
		fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
		int returnVal = fc.showOpenDialog(this);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			Config.getInstance().set(confPrevFile, fc.getSelectedFile().getAbsolutePath());
			return fc.getSelectedFile();
		} else {
			return null;
		}
	}
}
