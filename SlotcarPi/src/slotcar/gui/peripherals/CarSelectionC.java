package slotcar.gui.peripherals;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import slotcar.car.CarImage;
import slotcar.gui.peripherals.CarSelectionV.CarSelectionItem;

public class CarSelectionC {

	private CarSelectionV view;
	private final List<CarImage> cars;
	private final List<CarImage> selectedCars;
	
	private List<ChangeListener> listeners = new ArrayList<>();

	public void addChangeListener(ChangeListener l) {
		listeners.add(l);
	}

	public CarSelectionC(CarSelectionV view, List<CarImage> cars) {
		this.view = view;
		this.cars = cars;
		this.selectedCars = new ArrayList<>();

		// add mouse listener for selecting cars
		this.view.addItemClickListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				super.mousePressed(e);
				CarImage car = ((CarSelectionItem) e.getSource()).getCarImage();

				// if you hold ctrl, the cars are added to current selection
				if (!e.isControlDown()) {
					clearSelection();
				}
				
				// TODO add shift btn func

				if (selectedCars.contains(car)) {
					removeSelected(car);
				} else {
					addSelected(car);
				}
				
				for (ChangeListener l : listeners) {
					l.stateChanged(new ChangeEvent(this));
				}
			}
		});

		updateList();
	}

	public void updateList() {
		// remove non-existing car from selection
		selectedCars.removeIf((car) -> {
			return !cars.contains(car);
		});
		// update visualization of the list
		view.updateList(this.cars, this.selectedCars);
	}

	public void clearSelection() {
		selectedCars.clear();
		view.updateSelection(selectedCars);
	}

	public void addSelected(CarImage car) {
		if (!selectedCars.contains(car)) {
			selectedCars.add(car);
			view.updateSelection(selectedCars);
		}
	}

	public void removeSelected(CarImage car) {
		if (selectedCars.contains(car)) {
			selectedCars.remove(car);
			view.updateSelection(selectedCars);
		}
	}

	public List<CarImage> getSelectedCars() {
		return selectedCars;
	}

}
