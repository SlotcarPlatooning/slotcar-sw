package slotcar.gui.peripherals;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.apache.commons.lang3.SystemUtils;

import net.schmizz.sshj.DefaultConfig;
import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.common.IOUtils;
import net.schmizz.sshj.connection.channel.direct.Session;
import net.schmizz.sshj.connection.channel.direct.Session.Command;
import net.schmizz.sshj.transport.cipher.AES128CTR;
import net.schmizz.sshj.xfer.FileSystemFile;
import net.schmizz.sshj.xfer.LocalSourceFile;
import slotcar.Config;
import slotcar.RoutingTable;
import slotcar.gui.MainApp;
import slotcar.network.NetworkAddress;
import slotcar.wifi.WiFiNetworkAddress;

class IpListItem {
	private byte addr;
	private InetAddress ip;

	public IpListItem(String s) {
		String[] x = s.split(":");
		if (x.length == 2) {
			addr = (byte) (Integer.valueOf(x[0]) & 0xFF);
			try {
				ip = InetAddress.getByName(x[1]);
			} catch (UnknownHostException e) {
				ip = null;
				e.printStackTrace();
			}
		}

	}

	public IpListItem(byte addr, InetAddress ip) {
		this.addr = addr;
		this.ip = ip;
	}

	public byte getAddr() {
		return addr;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof IpListItem) {
			IpListItem o = (IpListItem) obj;
			if (o.addr == addr && o.ip.equals(ip)) {
				return true;
			}
		}
		return false;
	}

	public InetAddress getIp() {
		return ip;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}
}

public class CarUpgradeC implements Serializable {

	private static final long serialVersionUID = -3594684333470787932L;
	
	private final MainApp mApp;
	private final CarUpgradeV view;
	
	private File jar;
	private File bin;
	private File ctr;

	private DefaultListModel<IpListItem> ipList = new DefaultListModel<>();

	public CarUpgradeC(MainApp mainApp, CarUpgradeV view) {
		this.mApp = mainApp;
		this.view = view;

		view.setDevicesListModel(this.ipList);

		bin = Config.getBin();
		jar = Config.getJar();
		ctr = Config.getCtr();

		view.addButton("Load", (e) -> {
			upgradeProcess(view.cbBin.isSelected() || view.cbJar.isSelected() || view.cbCtr.isSelected(),
					view.cbResetAfrerLoad.isSelected(), view.cbFlashAfterLoad.isSelected());
		});

		view.addButton("Flash STM", (e) -> {
			upgradeProcess(false, false, true);
		});

		view.addButton("Restart", (e) -> {
			upgradeProcess(false, true, false);
		});

		view.addButton("Open SSH terminal", (e) -> {
			openSSHTerminal();
		});

		view.addButton("Reboot", (e) -> {
			CarUpgradeC.this.exec(Config.CMD_REBOOT, 100);
		});

		view.addButton("Shutdown", (e) -> {
			CarUpgradeC.this.exec(Config.CMD_SHUTDOWN, 100);
		});

		view.addButton("Upgrade", (e) -> {
			CarUpgradeC.this.exec(Config.CMD_UPGRADE, 100);
		});

//		view.addButton("Get connected", (e) -> {
//			SSHWorker worker = new SSHWorker("pi", "123Platooning");
//			StringBuilder b = new StringBuilder();
//			ArrayList<String> list = null;
//			worker.addTextListener((s) -> {
//				view.textPrintln(s);
//			});
//			worker.addJob(new exec(20, "sudo bash /home/pi/cars"));
//			try {
//				list = worker.doJobs(InetAddress.getByName("192.168.1.1"));
//			} catch (UnknownHostException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			}
//			
//			if(list == null){
//				return;
//			}
//
//
//			String[] x = list.get(0).split("\n");
//
//			for (int i = 0; i < x.length; i++) {
//				String[] l = x[i].split(" ");
//				String ip = l[1];
//				String host = l[0].replaceAll("slotcar", "");
//				
//				try {
//					RoutingTable.add(Byte.parseByte(host), new WiFiNetworkAddress(ip));
//				} catch (NumberFormatException | UnknownHostException e1) {
//					// TODO Auto-generated catch block
//					e1.printStackTrace();
//				}
//			}
//
//		});

		view.addBtnJarPathActionListener((e) -> {
			jar = view.chooseFile("jar", "Slotcar firmware", "jarPath");
		});

		view.addBtnCtrPathActionListener((e) -> {
			ctr = view.chooseFile("jar", "Slotcar controllers", "ctrPath");
		});

		view.addBtnBinPathActionListener((e) -> {
			bin = view.chooseFile("bin", "STM32 binary", "binPath");
		});

		view.addButton("Download Experiments", (e) -> {
			downloadExperimets(true, false);
		});

		// view.addBtnClearListActionListener((e) -> {
		// this.ipList.clear();
		// Config.clearIpList();
		// });
	}
	
	void insertIpItem(IpListItem ip) {
		if (ipList.contains(ip)) {
			int index = ipList.indexOf(ip);
			ipList.add(index, ip);
		} else {
			ipList.addElement(ip);
		}
	}

	private void openSSHTerminal() {
		String s = null;

		if (SystemUtils.IS_OS_WINDOWS) {
			s = CarUpgradeC.class.getResource("/putty.exe").getFile();
		} else {
			throw new UnsupportedOperationException("Linux bash is not yet implemented");
		}

		if (s == null) {
			Logger.getGlobal().warning("Can not open SSH");
			return;
		}

		for (InetAddress ip : selected()) {
			String puttyCmd = s + String.format(Config.SSH_FORMAT, ip.getHostAddress());
			try {
				Runtime.getRuntime().exec(puttyCmd);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private void upgradeProcess(boolean upload, boolean restart, boolean flash) {

		if (!(upload | restart | flash)) {
			JOptionPane.showMessageDialog(mApp.getFrame(), "Please select one or more operations",
					"No operation selected", JOptionPane.WARNING_MESSAGE);
			return;
		}

		SSHWorker worker = new SSHWorker();
		worker.addTextListener((s) -> {
			view.textPrintln(s);
		});
		if (view.cbRemovePrevious.isSelected()) {
			worker.addJob(new exec(1, Config.CMD_RM_CTRLS));
		}

		if (upload) {
			if (view.cbJar.isSelected()) {
				worker.addJob(new upload(jar, Config.CAR_JAR));
			}

			if (view.cbBin.isSelected()) {
				worker.addJob(new upload(bin, Config.CAR_BIN));
			}

			if (view.cbCtr.isSelected()) {
				worker.addJob(new upload(ctr, Config.CONTROLLERS));
			}
		}

		if (flash) {
			worker.addJob(new exec(5, Config.CMD_STOP));
			worker.addJob(new exec(5, Config.CMD_STM_FLASH));
			worker.addJob(new exec(5, Config.CMD_START));
		}

		if (restart) {
			worker.addJob(new exec(5, Config.CMD_RESTART));
		}

		worker.doJobs(selected());
	}

	public void downloadExperimets(boolean flash, boolean ram) {
		JFileChooser chooser = new JFileChooser();

		String s2 = Config.getInstance().get("PlotSavingPath");
		chooser.setCurrentDirectory(new java.io.File(s2 == null ? "." : s2));

		chooser.setDialogTitle("Select where should we download the experiments");
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		chooser.setMultiSelectionEnabled(false);
		chooser.setAcceptAllFileFilterUsed(false);

		if (chooser.showSaveDialog(mApp.getFrame()) == JFileChooser.APPROVE_OPTION) {
			Config.getInstance().set("PlotSavingPath", chooser.getSelectedFile().getAbsolutePath());
		} else {
			Logger.getGlobal().warning("Destination directory for experiments was not selected");
			return;
		}

		if (flash || ram) {
			SSHWorker worker = new SSHWorker();
			worker.addTextListener((s) -> {
				view.textPrintln(s);
			});
			String path = chooser.getSelectedFile().getAbsolutePath();

			if (flash)
				worker.addJob(new download(Config.EXP_FLASH, path));
			if (ram)
				worker.addJob(new download(Config.EXP_RAM, path));

			worker.doJobs(selected());
		}
	}

	private InetAddress[] selected() {
		return view.tbDevices.getSelectedIpAll().toArray(new InetAddress[0]);
	}

	void exec(String cmd, int timeout) {
		SSHWorker worker = new SSHWorker();
		worker.addTextListener((s) -> {
			view.textPrintln(s);
		});
		worker.addJob(new exec(timeout, cmd));
		worker.doJobs(selected());
	}

	abstract class SSHJob {
		abstract String doJob(SSHClient ssh) throws IOException;
	}

	class upload extends SSHJob {
		private LocalSourceFile localFile = null;
		private String localPath = null;
		private final String remotePath;

		public upload(File localFile, String remotePath) {
			this.remotePath = remotePath;
			this.localFile = new FileSystemFile(localFile);
		}

		public upload(String localPath, String remotePath) {
			this.remotePath = remotePath;
			this.localPath = localPath;
		}

		@Override
		String doJob(SSHClient ssh) throws IOException {
			if (localFile != null)
				ssh.newSCPFileTransfer().upload(localFile, remotePath);
			else if (localPath != null) {
				ssh.newSCPFileTransfer().upload(localPath, remotePath);
			} else {
				Logger.getGlobal().severe("Cannot do upload job");
			}
			return "";
		}

		@Override
		public String toString() {
			if (localFile != null)
				return String.format("upload from %s to %s", localFile, remotePath);
			else {
				return String.format("upload from %s to %s", localPath, remotePath);
			}
		}
	}

	class download extends SSHJob {

		private LocalSourceFile localFile = null;
		private String localPath = null;
		private final String remotePath;

		// public download(String remotePath, File localFile) {
		// this.remotePath = remotePath;
		// this.localFile = new FileSystemFile(localFile);
		// }

		public download(String remotePath, String localPath) {
			this.remotePath = remotePath;
			this.localPath = localPath;
		}

		@Override
		String doJob(SSHClient ssh) throws IOException {
			if (localPath != null) {
				ssh.newSCPFileTransfer().download(remotePath, localPath);
			} else {
				Logger.getGlobal().severe("Cannot do download job");
			}
			return "";
		}

		@Override
		public String toString() {
			return String.format("download from %s to %s", remotePath, localPath);
		}
	}

	class exec extends SSHJob {
		final String cmd;
		final int timeout;

		public exec(int timeout, String cmd) {
			this.cmd = cmd;
			this.timeout = timeout;
		}

		@Override
		String doJob(SSHClient ssh) throws IOException {
			final Session session = ssh.startSession();
			final Command cmd = session.exec(this.cmd);
			cmd.join(timeout, TimeUnit.SECONDS);
			String s = String.format("Command %s, exit status : %d.", this.cmd, cmd.getExitStatus());
			Logger.getGlobal().info(s);
			session.close();
			return IOUtils.readFully(cmd.getInputStream()).toString();

		}

		@Override
		public String toString() {
			return String.format("cmd: %s", cmd);
		}

	}

	class SSHConfig extends DefaultConfig {

		@SuppressWarnings("unchecked")
		protected void initCipherFactories() {
			setCipherFactories(new AES128CTR.Factory());
		}
	}

	class SSHWorker {
		
		final String user;
		final String password;

		private List<SSHJob> jobs = new ArrayList<>();
		private List<TextListener> listeners = new ArrayList<>();
		

		public SSHWorker() {
			this.user = Config.SSH_USER;
			this.password = Config.SSH_PSWD;
		}

		public SSHWorker(String user, String password) {
			this.user = user;
			this.password = password;
		}

		public ArrayList<String> doJobs(InetAddress... ips) {

			if (ips.length == 0) {
				JOptionPane.showMessageDialog(mApp.getFrame(), "Please select one or more targets",
						"No target selected", JOptionPane.WARNING_MESSAGE);
				return null;
			}

			ExecutorService executor = Executors.newWorkStealingPool();
			List<Callable<Boolean>> l = new ArrayList<>();

			printProgress(String.format("SSH Work, cars: %d, jobs: %d", ips.length, jobs.size()));

			ArrayList<String> output = new ArrayList<>();
			for (InetAddress ip : ips) {
				Callable<Boolean> task = new Callable<Boolean>() {
					int i = 0;

					@Override
					public Boolean call() throws Exception {
						try {
							SSHClient ssh = new SSHClient(new SSHConfig());

							ssh.addHostKeyVerifier((host, port, key) -> {
								return true; // TODO: verification
							});
							// ssh.loadKnownHosts();
							// Make sure JZlib is in classpath for this to work
							//ssh.useCompression();

							ssh.connect(ip);

							if (!ssh.isConnected()) {
								Logger.getGlobal().severe("SSH: can not connect to " + ip);
								ssh.close();
								return false;
							}

							try {

								ssh.authPassword(SSHWorker.this.user, SSHWorker.this.password);

								if (!ssh.isAuthenticated()) {
									Logger.getGlobal().severe("SSH: can not connect to " + ip);
									ssh.close();
									return false;
								}
								for (SSHJob job : jobs) {
									i++;
									printProgress(String.format("%d/%d %s %s...EXECUTING\n", i, jobs.size(),
											ip.getHostAddress(), job));
									String ret = job.doJob(ssh);
									output.add(ret);
									printProgress(ret);
									printProgress(String.format("%d/%d %s %s...DONE\n", i, jobs.size(),
											ip.getHostAddress(), job));
								}
							} finally {
								ssh.disconnect();
							}
						} catch (IOException e) {
							Logger.getGlobal().log(Level.SEVERE, "Job could not be done ip: " + ip, e);
							return false;
						}
						return true;
					}
				};
				l.add(task);
			}

			try {
				executor.invokeAll(l).stream().map(future -> {
					try {
						return future.get();
					} catch (Exception e) {
						throw new IllegalStateException(e);
					}
				}).forEach(System.out::println);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			printProgress("SSH Work...DONE\n");
			
			return output;

		}

		public void addJob(SSHJob job) {
			jobs.add(job);
		}

		public void addTextListener(TextListener l) {
			this.listeners.add(l);
		}

		private void printProgress(String s) {
			for (TextListener l : listeners) {
				l.print(s);
			}
		}
	}

	interface TextListener {
		public void print(String s);
	}

	public void restart(byte logAddr) {
		SSHWorker worker = new SSHWorker();
		worker.addJob(new exec(5, Config.CMD_RESTART));
		NetworkAddress n = RoutingTable.get(logAddr);
		if (n instanceof InetAddress) {
			worker.doJobs((InetAddress) n);
		}
	}

}
