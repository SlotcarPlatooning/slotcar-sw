package slotcar.gui.peripherals;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.filechooser.FileNameExtensionFilter;

import slotcar.Config;

public class ControllerConfigV extends JPanel {

	private static final long serialVersionUID = -7341558884313914261L;

	private JLabel lblName;

	private JButton btnUpload;
	private JButton btnDownload;
	private JButton btnSave;
	private JButton btnLoad;

	private Box paramListPanel;

	public ControllerConfigV() {
		setLayout(new BorderLayout());

		lblName = new JLabel();
		lblName.setHorizontalAlignment(SwingConstants.CENTER);
		add(lblName, BorderLayout.NORTH);

		JPanel buttonsPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
		add(buttonsPanel, BorderLayout.SOUTH);

		/* UPLOAD button */
		btnUpload = new JButton("Write to Car");
		buttonsPanel.add(btnUpload);

		/* DOWNLOAD button */
		btnDownload = new JButton("Read from Car");
		buttonsPanel.add(btnDownload);

		/* SAVE button */
		btnSave = new JButton("Save");
		buttonsPanel.add(btnSave);

		/* LOAD button */
		btnLoad = new JButton("Load");
		buttonsPanel.add(btnLoad);

		/* PANEL with list of setting forms */
		paramListPanel = Box.createVerticalBox();
		JScrollPane scrollPane = new JScrollPane(paramListPanel);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		add(scrollPane, BorderLayout.CENTER);
	}

	public void addUploadActionListener(ActionListener l) {
		btnUpload.addActionListener(l);
	}

	public void addDownloadActionListener(ActionListener l) {
		
		btnDownload.addActionListener(l);
	}

	public void addLoadActionListener(ActionListener actionListener) {
		btnLoad.addActionListener(actionListener);
	}

	public void addSaveActionListener(ActionListener actionListener) {
		btnSave.addActionListener(actionListener);
	}
	
	public File chooseFile() {
		String s = Config.getInstance().get("REG_CONF_PATH");
		JFileChooser chooser = new JFileChooser(s == null ? "" : s);
		FileNameExtensionFilter filter = new FileNameExtensionFilter("XML", "xml");
		chooser.setFileFilter(filter);
		int returnVal = chooser.showOpenDialog(null);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			System.out.println("You chose to open this file: " + chooser.getSelectedFile().getName());
			Config.getInstance().set("REG_CONF_PATH", chooser.getSelectedFile().getAbsolutePath());
			return chooser.getSelectedFile();
		}
		return null;
	}

	public void setName(String string) {
		lblName.setText("Controller: " + string);
	}

	public void setDescription(String description) {
		lblName.setToolTipText(description);
	}

	public void clearList() {
		paramListPanel.removeAll();
	}

	public void addPanel(JPanel p) {
		paramListPanel.add(p);
	}
}
