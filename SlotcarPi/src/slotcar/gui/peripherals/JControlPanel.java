package slotcar.gui.peripherals;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

public class JControlPanel extends JPanel {

	private static final long serialVersionUID = 3058862094347997236L;

	public JControlPanel() {
		this.setLayout(new GridLayout(0, 1, 0, 0));
	}

	public void addStartStopBtn(String name, ActionListener startListner, ActionListener stopListener) {
		JPanel foo = new JPanel();
		foo.setLayout(new FlowLayout(FlowLayout.RIGHT, 0, 0));
		JButton start = new JButton(name);
		JButton stop = new JButton(".");

		start.addActionListener(startListner);
		stop.addActionListener(stopListener);

		foo.add(start);
		foo.add(stop);
		this.add(foo);
	}

	public JButton addBtn(String name, ActionListener listner) {
		JButton btn = new JButton(name);
		btn.addActionListener(listner);
		this.add(btn);
		return btn;
	}

}
