package slotcar.gui.peripherals;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;

import slotcar.Config;

public class ControllerSelectionV extends JPanel {
	private static final long serialVersionUID = -3161033466133887718L;

	private JComboBox<String> cbControllers;
	private JButton btnAdd;
	JPanel buttonsPanel;
	JLabel label = new JLabel("Controller");
	
	public File[] chooseFiles() {
		File f = Config.getCtr();
		JFileChooser fc = new JFileChooser((f != null) ? f.getAbsolutePath() : "");
		fc.setFileFilter(new FileNameExtensionFilter("Java SlotCarPi Controller", "jar"));
		fc.setMultiSelectionEnabled(true);
		fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
		int returnVal = fc.showOpenDialog(this);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			Config.setCtrPath(fc.getSelectedFile().getAbsolutePath());
			return fc.getSelectedFiles();
		} else {
			return null;
		}
	}

	void addBtnAddListener(ActionListener l) {
		btnAdd.addActionListener(l);
	}

	void addCbActionListener(ActionListener l) {
		cbControllers.addActionListener(l);
	}

	public void addItem(String i) {
		cbControllers.addItem(i);
	}

	public void clearCb() {
		cbControllers.removeAllItems();
	}

	public String getSelected() {
		return (String) cbControllers.getSelectedItem();
	}

	public ControllerSelectionV() {
		setLayout(new BorderLayout(0, 0));

		cbControllers = new JComboBox<>();
		add(cbControllers);

		buttonsPanel = new JPanel();
		buttonsPanel.setLayout(new FlowLayout());
		add(buttonsPanel, BorderLayout.EAST);
		add(label, BorderLayout.WEST);
		btnAdd = new JButton("Add");
		buttonsPanel.add(btnAdd);
		
		setBackground(Color.WHITE);

	}

	@Override
	public void setBackground(Color bg) {
		super.setBackground(bg);
		try{
			buttonsPanel.setBackground(bg);
			btnAdd.setBackground(bg);
			label.setBackground(bg);
		}catch(NullPointerException ex){
			// do nothing
		}
	}

	public void setSelected(String controller) {
		cbControllers.setSelectedItem(controller);
	}

	
}
