package slotcar.gui.peripherals;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import slotcar.Controllers;
import slotcar.car.CarImage;
import slotcar.Config;
import slotcar.pi.controller.Controller;

public class ControllerSelectionC {
	
	@SuppressWarnings("unused")
	private static final long serialVersionUID = -6299185929304913994L;
	
	/*
	 * List of known controllers.
	 */
	protected Controllers controllers = new Controllers();

	/*
	 * Selected controller.
	 */
	private Controller selected;

	/*
	 * Graphical interface.
	 */
	private ControllerSelectionV view;

	/*
	 * Change listeners
	 */
	private List<ChangeListener> controllerChangeListners = new ArrayList<>();
	
	/*
	 * Constructor
	 */
	public ControllerSelectionC(ControllerSelectionV view) {
		this.view = view;
		
		// Select default controller.
		selected = controllers.getDefault();
		view.setSelected(selected.getName());
		
		// Load other controllers.
		try {
			for (String pathController : Config.getControllersPaths().split(";")) {
				controllers.load(0, new File(pathController));
			}
		} catch (NullPointerException ex) {
			Logger.getGlobal().warning("No Controllers path is set");
		}
		itemsReload();
		
		this.view.addBtnAddListener((e) -> {
			controllers.load(1, view.chooseFiles());
			itemsReload();
		});

		view.addCbActionListener((e) -> {
			setSelected(view.getSelected());
		});
	}
	
	public synchronized void itemsReload() {
		this.view.clearCb();
		for (Controller c : controllers.getValues()) {
			this.view.addItem(c.getName());
		}
	}

	public synchronized Controller getSelected() {
		return selected;
	}
	
	private synchronized void setSelected(String controller) {
		if( !selected.getName().equals(controller) ) {
			selected = controllers.get(controller);
			view.setSelected(controller);
			for (ChangeListener l : controllerChangeListners) {
				l.stateChanged(new ChangeEvent(this));
			}
		}
	}


	public void addControllerChangeListner(ChangeListener l) {
		controllerChangeListners.add(l);
	}

	public boolean compare(CarImage carImage) {
		boolean ret = true;
		String c1 = null;
		String c2 = null;
		try {
			c1 = getSelected().getName();
			c2 = carImage.getCarControl().getController();
			ret &= c1.equals(c2);
		} catch (NullPointerException e) {
			Logger.getGlobal().warning(String.format("Comparing to null c1:%s c2:%s ", c1, c2));
			System.err.println("comparing controller to null");
			ret = false;
		}

		view.setBackground(Config.getColor(ret));
		return ret;
	}

	public void update(CarImage car, boolean foo) {
		// if (foo) {
		// setSelected(car.getCarControl().getController());
		// }

		// porovna controller s vybranymi auty a kdyztak dat cervene
		compare(car);
	}

	public void readFrom(CarImage car) {
		setSelected(car.getCarControl().getController());
	}
	
	public void writeTo(CarImage car) {
		car.getCarControl().setController(getSelected().getName());
	}
}