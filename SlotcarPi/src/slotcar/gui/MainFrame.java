package slotcar.gui;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JTabbedPane;

import slotcar.Config;

class MainFrame extends JFrame {

	private static final long serialVersionUID = -2288426695803734530L;
	private JTabbedPane tabbedPane;
	private JPanel leftPanel;
	private JButton btnCreateNetwork;

	private JCheckBoxMenuItem cbOnlineUpdates;

	public MainFrame(String title) {
		super(title);
		
		leftPanel = new JPanel(new BorderLayout());
		getContentPane().add(leftPanel, BorderLayout.WEST);

		btnCreateNetwork = new JButton("Create Network");
		leftPanel.add(btnCreateNetwork, BorderLayout.SOUTH);

		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		getContentPane().add(tabbedPane, BorderLayout.CENTER);
		
		
		setExtendedState(MAXIMIZED_BOTH);
		setLocationRelativeTo(null); // position at center
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		
		
//	   // f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//	    JMenuBar bar = new JMenuBar();
//	    JMenu menu = new JMenu("Synchronization");
//	    menu.setMnemonic(KeyEvent.VK_O);
//
//	    ButtonGroup group = new ButtonGroup();
//	    
//	    cbOnlineUpdates = new JCheckBoxMenuItem("Online updates");
//	    cbOnlineUpdates.setSelected(Config.isOnlineSync());
//	    cbOnlineUpdates.addActionListener((e)->{
//	    	Config.setOnlineSync(cbOnlineUpdates.isSelected());
//	    });
//	    menu.add(cbOnlineUpdates);
//
//	    menu.addSeparator();
//	    JRadioButtonMenuItem menuItem = new JRadioButtonMenuItem("gui > car ");
//	    menuItem.addActionListener((e)->{
//	    	Config.setSyncPreference(1);
//	    });
//	    if(Config.getSyncPreference() == 1){
//	    	menuItem.setSelected(true);    	
//	    }
//	    group.add(menuItem);
//	    menu.add(menuItem);
//
//	    menuItem = new JRadioButtonMenuItem("gui < car");
//	    menuItem.addActionListener((e)->{
//	    	Config.setSyncPreference(-1);
//	    });
//	    if(Config.getSyncPreference() == -1){
//	    	menuItem.setSelected(true);    	
//	    }
//	    group.add(menuItem);
//	    menu.add(menuItem);
//
//	    menuItem = new JRadioButtonMenuItem("gui == car");
//	    menuItem.addActionListener((e)->{
//	    	Config.setSyncPreference(0);
//	    });
//	    if(Config.getSyncPreference() == 0){
//	    	menuItem.setSelected(true);    	
//	    }
//	    group.add(menuItem);
//	    menu.add(menuItem);
//
//
//	    menu.addSeparator();
//
//	    bar.add(menu);
//	    setJMenuBar(bar);
	   
	}

	public void addTab(String title, JPanel panel) {
		tabbedPane.addTab(title, panel);
	}

	public void addToWest(JPanel panel) {
		leftPanel.add(panel, BorderLayout.CENTER);
	}

	public void addBtnCreateNetworkActionListener(ActionListener l) {
		btnCreateNetwork.addActionListener(l);
	}
	
}