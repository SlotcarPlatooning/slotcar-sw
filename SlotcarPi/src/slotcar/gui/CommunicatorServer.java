package slotcar.gui;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.ByteBuffer;
import java.nio.ShortBuffer;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.List;
import java.util.logging.Logger;

import slotcar.Config;
import slotcar.RoutingTable;
import slotcar.SlotcarState;
import slotcar.car.CarImage;
import slotcar.clock.CarClock;
import slotcar.network.ComManager;
import slotcar.network.Communicator;
import slotcar.network.NetworkRole;
import slotcar.network.NetworkStatInterpret;
import slotcar.network.NewDataEvent;
import slotcar.network.NewDataEventHandler;
import slotcar.network.ServiceType;

public class CommunicatorServer extends Communicator {

	protected final NetworkEventListenerServer owner;

	public CommunicatorServer(NetworkEventListenerServer owner, ComManager comMan) {
		super(comMan, NetworkRole.SERVER, Config.ADDR_SERVER);
		this.owner = owner;

		addHandler(ServiceType.CAR_LOGGING_REQUEST, (event) -> {
			byte logAddr = event.getPacket().getSourceAddr();
			RoutingTable.add(logAddr, event.getNtwkAddr());
			// confirm the car that is successfully joined the network
			this.sendCarLoggingAck(logAddr);
			this.owner.handleCarLoggingRequest(logAddr);
		});

		addHandler(ServiceType.IMAGE, (event) -> {
			owner.handleImage(event.getPacket().getSourceAddr(), event.getPacket().getPayload());
		});

		addHandler(ServiceType.IMAGE_REQUESTED, (event) -> {
			owner.handleImageRequested(event.getPacket().getSourceAddr(), event.getPacket().getPayload());
		});

		addHandler(ServiceType.SET_POSITION, (event) -> {
			owner.handleSetPosition(event.getPacket().getSourceAddr());
		});

		addHandler(ServiceType.COMMUNICATION_TEST_RESULT, (event) -> {
			int numPckReceived = 0;
			try {
				ObjectInputStream in2 = new ObjectInputStream(new ByteArrayInputStream(event.getPacket().getPayload()));
				numPckReceived = in2.readInt();
				in2.close();
			} catch (IOException e2) {
				e2.printStackTrace();
			}
			owner.handleCommTestResult(event.getPacket().getSourceAddr(), numPckReceived);
		});

		addHandler(ServiceType.ERROR, (event) -> {
			try {
				ByteArrayInputStream inp = new ByteArrayInputStream(event.getPacket().getPayload());
				ObjectInputStream str = new ObjectInputStream(inp);
				String s = str.readUTF();
				str.close();
				inp.close();
				owner.handleErrorMessage(s);
			} catch (IOException ex) {
				System.out.println("Error in processing error message");
			}
		});

		addHandler(ServiceType.CONTROLLER_CONSTANTS, (event) -> {
			owner.handleControllerConstans(event.getPacket().getPayload());
		});

		addHandler(ServiceType.STATUS, (event) -> {
			byte logAddr = event.getPacket().getSourceAddr();
			ByteBuffer bf = ByteBuffer.wrap(event.getPacket().getPayload());
			SlotcarState state = SlotcarState.getByVal(bf.getInt());
			int hash = bf.getInt();
			long timeOfChange = bf.getLong();
			owner.handleStatus(logAddr, state, hash, timeOfChange);
		});

		addHandler(ServiceType.NET_CLOCK_LOG, (event) -> {
			String text = new String(event.getPacket().getPayload(), StandardCharsets.UTF_8);
			owner.handleClockLog(event.getPacket().getSourceAddr(), text);
		});
		
		addHandler(ServiceType.NET_STAT_LOG, (event) -> {
			ByteBuffer bb = ByteBuffer.wrap(event.getPacket().getPayload());
			int lossCount = bb.getInt();
			int sampleCount = bb.getInt();
			ShortBuffer sb = bb.asShortBuffer();
			short[] buf = new short[sb.remaining()];
			sb.get(buf);
			NetworkStatInterpret nsi = new NetworkStatInterpret(lossCount/sampleCount, buf);
			owner.handleNetStatLog(event.getPacket().getSourceAddr(), nsi.toString());
			owner.dumpNetStatLog(buf);
		});
		
		addHandler(ServiceType.NET_PROBE, (event) -> {
		});
	}

	@Override
	public NewDataEventHandler getDefaultHandler() {
		return (event) -> {
			owner.handleUnknownPacket(event.getPacket());
		};
	}

	/**
	 * Main method for events. Whenever a packet is received, it processes its
	 * data and passes it to the upper layer.
	 */
	@Override
	public void handleNewDataEvent(NewDataEvent e) {

		super.handleNewDataEvent(e);

		// StringBuilder s = new StringBuilder();
		// for(byte i : e.getPacket().getPayload()){
		// s.append(String.format("0x%02X", i)).append(",");
		// }
		//
		// Logger.getGlobal().fine(e.toString());

		owner.updateTimeWhenLastMet(e.getPacket().getSourceAddr());
	}

	public void sendCalibrationA(byte[] addresses, float refDistance) {

		ByteBuffer bf = ByteBuffer.allocate(4);
		bf.putFloat(refDistance);
		Logger.getGlobal().info("sending calibration A " + refDistance + "cm");

		sendTo(addresses, ServiceType.CALIBRATION_A, bf.array());
	}

	public void sendCalibrationB(byte[] addresses) {
		sendTo(addresses, ServiceType.CALIBRATION_B);
	}

	public void sendStateLoggingStart(int id, boolean ram, byte[] addresses) {
		ByteBuffer bf = ByteBuffer.allocate(8);
		bf.putInt(id);
		bf.putInt(ram ? 1 : 0);
		sendTo(addresses, ServiceType.LOGGING_START, bf.array());
	}

	public void sendStateLoggingStop(byte[] addresses) {
		sendTo(addresses, ServiceType.LOGGING_STOP);
	}

	public void sendCommunicationStart(byte[] addresses) {
//		for (byte addr : addresses) {
//			this.send(addr, ServiceType.COMMUNICATION_START);
//		}
		for (int i = 0; i < 5; i++)
			send(ServiceType.COMMUNICATION_START);
	}

	public void sendCommunicationStop(byte[] addresses) {
//		for (byte addr : addresses) {
//			this.send(addr, ServiceType.COMMUNICATION_STOP);
//		}
		for (int i = 0; i < 5; i++)
			send(ServiceType.COMMUNICATION_STOP);
	}

	public void sendImageRequest(byte logAddr) {
		send(logAddr, ServiceType.IMAGE_REQUEST);
	}

	public void sendReconnectRequest() {
		send(ServiceType.RECONNECT_REQUEST);
	}

	public void sendControlStop(List<CarImage> selectedCars) {
//		for (CarImage carImage : selectedCars) {
//			send(carImage.getLogAddr(), ServiceType.CONTROL_STOP);
//		}
		for (int i = 0; i < 5; i++)
			send(ServiceType.CONTROL_STOP);
	}

	public void sendControlStart(List<CarImage> selectedCars) {
//		for (CarImage carImage : selectedCars) {
//			send(carImage.getLogAddr(), ServiceType.CONTROL_START);
//		}
		for (int i = 0; i < 5; i++)
			send(ServiceType.CONTROL_START);
	}

	public void sendStatusRequest(byte logAddr) {
		send(logAddr, ServiceType.STATUS_REQUEST);
	}

	public void sendStateChange(SlotcarState positioning, byte[] addresses) {
		for (byte addr : addresses) {
			ByteBuffer bf = ByteBuffer.allocate(4);
			bf.putInt(positioning.getVal());
			this.send(addr, ServiceType.STATE_CHANGE, bf.array());
		}
	}
}
