package slotcar.gui;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import slotcar.CarList;
import slotcar.car.CarImage;

public abstract class CommunicatorMatlab {

	private final int port;
	private final CarList cars;
	private final Thread communicationThread;
	private final ByteBuffer in;
	private final ByteBuffer out;
	private final DatagramPacket receivePacket;
	private final DatagramPacket sendPacket;

	private DatagramSocket socket;
	private long timeOfUpdate;
	
	public void setTimeOfUpdate(long timeOfUpdate) {
		this.timeOfUpdate = timeOfUpdate;
	}

	public long getTimeOfUpdate() {
		return timeOfUpdate;
	}

	public CommunicatorMatlab(int port, int sizeIn, int sizeOut, CarList cars) {
		this.cars = cars;
		this.port = port;

		this.in = ByteBuffer.wrap(new byte[sizeIn]);
		this.out = ByteBuffer.wrap(new byte[sizeOut]);
		out.order(ByteOrder.LITTLE_ENDIAN);
		in.order(ByteOrder.LITTLE_ENDIAN);
		
		this.receivePacket = new DatagramPacket(in.array(), in.capacity());
		this.sendPacket = new DatagramPacket(out.array(), out.capacity());

		communicationThread = new Thread(() -> {
			execution();
		});
	}

	void start() throws SocketException {
		socket = new DatagramSocket(port);
		communicationThread.start();
	}

	private void execution() {
		while (true) {
			try {
				socket.receive(receivePacket);
				in.rewind();
				out.rewind();
				processData(in, out);
				sendPacket.setAddress(receivePacket.getAddress());
				sendPacket.setPort(receivePacket.getPort());
				socket.send(sendPacket);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void processData(ByteBuffer in, ByteBuffer out) {

		float msg_type = in.getFloat();

		if (msg_type != 0.0f) {
			// Received data request.
			out.putFloat((float) 2);
		} else {
			// Received car data.
			boolean enable = in.getFloat() > 0.0f;
			boolean ctl = in.getFloat() > 0.0f;

			if(enable){
				/* Saving data received from MATLAB. */
				for (CarImage car : cars.getCars())
					car.getCarControl().setControlled(ctl);
				for (CarImage car : cars.getCars())
					car.getCarControl().setReferencePwm(in.getFloat());
				for (CarImage car : cars.getCars())
					car.getCarControl().setReferenceSpeed(in.getFloat());
				for (CarImage car : cars.getCars())
					car.getCarControl().setReferenceDistance(in.getFloat());
				for (CarImage car : cars.getCars())
					car.getCarControl().setTimeHeadway(in.getFloat());
	
				/* If data changed, write it to real car. */
	//			long currentTime = System.currentTimeMillis();
	//			if (currentTime - getTimeOfUpdate() > 50l) { // 100 [ms]
					for (CarImage car : cars.getCars()) {
						if (car.getCarControl().hasChanged()) {
							write(car);
							car.getCarControl().clearUpdated();
						}
					}
	//				setTimeOfUpdate(currentTime);
	//			}
			}
			
//			for (CarImage car : cars.getCars())
//				System.out.println(car);

			/* Sending output to MATLAB */
			for (CarImage car : cars.getCars())
				out.putFloat(car.getCarMeasurement().getSpeed());
			for (CarImage car : cars.getCars())
				out.putFloat(car.getCarMeasurement().getDistanceFront());
			for (CarImage car : cars.getCars())
				out.putFloat(car.getCarMeasurement().getDistanceBack());
			for (CarImage car : cars.getCars())
				out.putFloat(car.getCarMeasurement().getAppliedPwm());
			for (CarImage car : cars.getCars())
				out.putFloat(car.getCarMeasurement().getAccX());
			for (CarImage car : cars.getCars())
				out.putFloat(car.getCarMeasurement().getAccY());
			for (CarImage car : cars.getCars())
				out.putFloat(car.getCarMeasurement().getAccZ());
			for (CarImage car : cars.getCars())
				out.putFloat(car.getCarMeasurement().getPitchRate());
			for (CarImage car : cars.getCars())
				out.putFloat(car.getCarMeasurement().getRollRate());
			for (CarImage car : cars.getCars())
				out.putFloat(car.getCarMeasurement().getYawRate());
			for (CarImage car : cars.getCars())
				out.putFloat(car.getCarMeasurement().getSpeedIrc());
			for (CarImage car : cars.getCars())
				out.putFloat(car.getCarMeasurement().getSpeedBemf());
			for (CarImage car : cars.getCars())
				out.putFloat(car.getCarMeasurement().getMotorBemf());
			for (CarImage car : cars.getCars())
				out.putFloat(car.getCarMeasurement().getMotorI());
			for (CarImage car : cars.getCars())
				out.putFloat(car.getCarMeasurement().getRailV());
			for (CarImage car : cars.getCars())
				out.putFloat(car.getCarMeasurement().getSupercapV());
			for (CarImage car : cars.getCars())
				out.putFloat(car.getCarMeasurement().getDesiredI());
			for (CarImage car : cars.getCars())
				out.putFloat(car.getCarControl().getReferencePwm());
			for (CarImage car : cars.getCars())
				out.putFloat(car.getCarMeasurement().getDesiredSpeed());
			for (CarImage car : cars.getCars())
				out.putFloat(car.getCarControl().getReferenceDistance());
//			for (CarImage car : cars.getCars())
//				out.putFloat(car.getCarMeasurement().getFriction());
		}
	}

	public abstract void write(CarImage car);
}