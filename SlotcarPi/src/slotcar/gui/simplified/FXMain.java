/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package slotcar.gui.simplified;

import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

/**
 *
 * @author Marek
 */
public class FXMain extends Application {

    private boolean fullscreen = true;

    @Override
    public void start(Stage primaryStage) throws IOException {

        primaryStage.setTitle("Slotcar platooning");
        primaryStage.setFullScreen(true);

        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml")); //problem

        Scene scene = new Scene(root);

        primaryStage.setScene(scene);
        primaryStage.show();

        //fullscreen if F11 pressed
        scene.setOnKeyPressed((KeyEvent event) -> {
            if (event.getCode() == KeyCode.F11) {
                fullscreen = !fullscreen;
                primaryStage.setFullScreen(fullscreen);
            }
        });
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
