/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package slotcar.gui.simplified;

import javafx.scene.text.Text;
import com.jfoenix.controls.*;
import eu.hansolo.medusa.*;
//import java.awt.event.KeyEvent;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.effect.BoxBlur;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import slotcar.gui.MainApp;

/**
 *
 * @author Marek
 */
public class FXMLDocumentController implements Initializable {

    private ColorAdjust colorAdjust;
    private BoxBlur boxBlur;
    @FXML
    private ImageView ACC;
    @FXML
    private ImageView CACC;
    @FXML
    private JFXSlider distanceSlider;
    @FXML
    private BorderPane gaugeBox;
    @FXML
    private JFXSlider speedSlider;
    @FXML
    private Text text;
    @FXML
    private JFXToggleButton toggle;
    @FXML
    private JFXButton startButton;
    private static MainApp main;
    private int on = 1;

    @FXML
    private void handleToggleAction(ActionEvent event) {
        if (toggle.isSelected()) {
            System.out.println("CACC");
            main.writeController("src.CACC");

            ACC.setEffect(boxBlur);
            CACC.setEffect(null);

        } else {
            System.out.println("ACC");
            main.writeController("src.BPID");

            CACC.setEffect(boxBlur);
            ACC.setEffect(null);
        }
    }
    
    @FXML
    private void handleCACCPictureMousePressed(ActionEvent event){
        toggle.setSelected(true);
    }
    
        @FXML
    private void handleACCPictureMousePressed(ActionEvent event){
        toggle.setSelected(false);
    }

    @FXML
    private void handleStartButtonAction(ActionEvent event) {
        if ("Start leader".equals(startButton.getText())) {

            //kod pro start leadera
            System.out.println("Startujeme");
            startButton.setText("Stop leader");
            main.setRefSpeed((float) speedSlider.getValue());
            on = 1;
            speedSlider.valueProperty().setValue(speedSlider.valueProperty().getValue() + 0.0001);

        } else {
            //kod pro stop leadera
            System.out.println("Stop");
            startButton.setText("Start leader");
            main.setRefSpeed(0);
            on = 0;
            speedSlider.valueProperty().setValue(speedSlider.valueProperty().getValue() - 0.0001);
        }
    }

    public static void setMain(MainApp main) {
        FXMLDocumentController.main = main;
    }

    private void setupGauge() {
        //Create gauge
        Gauge gauge = new Gauge();
        gauge.setMinValue(0); // Set the start value of the scale  
        gauge.setMaxValue(1); // Set the end value of the scale
        gauge.setValue(0.5);    // Set initial value
//        gaugeBox.getChildren().add(gauge);  // Add gauge
        gaugeBox.setCenter(gauge);      //Add gauge
        gauge.setTickLabelDecimals(1);

        //Make gauge correspond with speed slider
        speedSlider.valueProperty().addListener((ObservableValue<? extends Number> ov, Number curVal, Number newVal) -> {
            double tmp = (double) newVal;
            gauge.setValue(tmp);
            main.setRefSpeed((float) tmp * on);
        });
    }

    private void setupDistanceCarsAnimation() {
        distanceSlider.valueProperty().addListener((ObservableValue<? extends Number> ov, Number curVal, Number newVal) -> {
            text.setWrappingWidth((double) newVal * 4 + 70);
        });
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //create ImageView Effect for ACC/CACC images (blur and grayscale)
        colorAdjust = new ColorAdjust();
        colorAdjust.setSaturation(-1);
        colorAdjust.setContrast(-0.2);
        boxBlur = new BoxBlur();
        boxBlur.setInput(colorAdjust);
        CACC.setEffect(boxBlur);

        toggle.setToggleColor(javafx.scene.paint.Paint.valueOf("#0F9D58"));

        //Slider displays values round to one decimal place
        speedSlider.setValueFactory(slider -> Bindings.createStringBinding(() -> ((double) ((int) (10 * slider.getValue())) / 10) + "", slider.valueProperty()));

        setupGauge();
        setupDistanceCarsAnimation();

        //Distance of cars animation
        distanceSlider.valueProperty().addListener((ObservableValue<? extends Number> ov, Number curVal, Number newVal) -> {
            double tmp = (double) newVal;
            text.setWrappingWidth(tmp * 4 + 70);
            main.setRefDistance((float) tmp / 100);
        });
        
        
    }
}
