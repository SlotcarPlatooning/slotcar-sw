package slotcar.gui;

import javax.swing.UIManager.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.stage.Stage;

import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import slotcar.CarList;
import slotcar.Config;
import slotcar.Debugging;
import slotcar.RoutingTable;
import slotcar.SlotcarState;
import slotcar.car.CarImage;
import slotcar.gui.peripherals.*;
import slotcar.gui.peripherals.plot.ExperimentC;
import slotcar.gui.peripherals.plot.ExperimentV;
import slotcar.gui.simplified.FXMLDocumentController;
import slotcar.network.CarPacket;
import slotcar.network.ComManager;
import slotcar.network.ServiceType;
import slotcar.pi.controller.Controller;
import slotcar.pi.controller.UnsupportedControllerException;
import slotcar.wifi.WiFiComManager;

import slotcar.gui.simplified.FXMain;

/**
 * GUI main class
 */
public class MainApp implements NetworkEventListenerServer, Serializable {

    private static final long serialVersionUID = -7483514934251452723L;
    private static MainApp mainApp; // singleton

    private ScheduledExecutorService statusCheckExecutor = Executors.newScheduledThreadPool(1);

    // HANDLERS
    public CommunicatorServer comMan;
    public MainFrame mFrame;

    // VARIABLES
    private SlotcarState state = SlotcarState.READY;

    private CarConfig carConfig;
    private ControllerConfigC controllerConfigC;
    private ControllerConfigV controllerConfigV;
    private CarSelectionC carSelectionC;
    private CarSelectionV carSelectionV;
    private CarUpgradeC carUpgradeC;
    private CarUpgradeV carUpgradeV;
    private ExperimentC experimentC;
    private ExperimentV experimentV;
    private ManualControl manualControl;
    private NetControl netControl;

    private FXMain simpleGui;

    private JControlPanel jControlPanel;

    private CarImage dummy = new CarImage();

    private int posIndex;
    private byte[] positioningProc;

    public static final CarList cars = CarList.getInstance();

    public static MainApp getInstance() {
        if (mainApp == null) {
            mainApp = new MainApp();
        }
        return mainApp;
    }

    public JFrame getFrame() {
        return mFrame;
    }

    void uploadToCars(List<CarImage> cars) {
        for (CarImage car : cars) {
            carConfig.writeTo(car);
        }
        writeSettings(cars);
    }

    void uploadToSelectedCars() {
        uploadToCars(carSelectionC.getSelectedCars());
    }

    void updateConfig(boolean changeConfig) {
        CarImage car = null;
        try {
            car = carSelectionC.getSelectedCars().get(0);
        } catch (IndexOutOfBoundsException e) {
            return;
        }
        carConfig.update(car, changeConfig);
    }

    void updateConfig() {
        updateConfig(carConfig.isReadOnClick());
        controllerConfigC.setController(carConfig.getSelectedController());
    }

    private MainApp() {

        // Initialization of components
        // TODO osetrit na null
        NetworkInterface iface = WiFiComManager.selectNetworkInterface();
        if (iface == null) {
            JOptionPane.showMessageDialog(null, "No interface was choosen, exiting");
            System.exit(1);
        }
        ComManager comM = new WiFiComManager(iface, Config.APP_COM_PORT, Config.APP_COM_PORT);
        if (!comM.open()) {
            JOptionPane.showMessageDialog(null, "Socket is in use, check if enother GUI is running");
            System.exit(1);
        }
        RoutingTable.add(Config.ADDR_SERVER, comM.getNetAddr());
        this.comMan = new CommunicatorServer(this, comM);

        state = SlotcarState.READY;

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException
                | UnsupportedLookAndFeelException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        jControlPanel = new JControlPanel();

        /* Car Upgrade */
        carUpgradeV = new CarUpgradeV();
        carUpgradeC = new CarUpgradeC(this, this.carUpgradeV);
        /* Car parameters configuration */
        carConfig = new CarConfig(this, ParamsGraphs.params);

        /* Car controller config */
        controllerConfigV = new ControllerConfigV();
        controllerConfigC = new ControllerConfigC(controllerConfigV);
        /* Car parameters configuration */
        carSelectionV = new CarSelectionV(jControlPanel);
        carSelectionC = new CarSelectionC(carSelectionV, cars.getCars());
        /* Experiment panel */
        experimentV = new ExperimentV();
        experimentC = new ExperimentC(experimentV, ParamsGraphs.graphs);

        manualControl = new ManualControl(this);
        netControl = new NetControl(this);

        // status check service and synchronization
        statusCheckExecutor.scheduleAtFixedRate(() -> {
            MainApp.this.checkStatus();
        }, 0, 1, TimeUnit.SECONDS);

        controllerConfigV.addSaveActionListener((e) -> {
            File f = controllerConfigV.chooseFile();
            if (f == null) {
                return;
            }
            try {
                FileOutputStream out = new FileOutputStream(f);
                carConfig.getSelectedController().writeTo(out);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        controllerConfigV.addLoadActionListener((e) -> {
            File f = controllerConfigV.chooseFile();
            if (f == null) {
                return;
            }
            try {
                FileInputStream in = new FileInputStream(f);
                carConfig.getControllers().readControllerConstantsFrom(in);
                controllerConfigV.revalidate();
            } catch (FileNotFoundException ex) {
                ex.printStackTrace();
            } catch (UnsupportedControllerException ex) {
                System.out.println(ex.getMessage());
            }

        });

        controllerConfigV.addUploadActionListener((e) -> {
            comMan.sendControllerConstants(carConfig.getSelectedController(), carSelectionC.getSelectedCars());
        });

        controllerConfigV.addDownloadActionListener((e) -> {
            try {
                Controller cont = carConfig.getSelectedController();
                System.out.println("Controller: " + cont);
                comMan.sendControllerConstantsRequest(cont, carSelectionC.getSelectedCars().get(0).getLogAddr());
            } catch (NullPointerException | IndexOutOfBoundsException e2) {
                final String s = "No car or controller selected";
                System.out.println(s);
                JOptionPane.showMessageDialog(mFrame, s);
            }

        });

        jControlPanel.addBtn("Positioning", (e) -> {
            positioningStart();
        });

        jControlPanel.addStartStopBtn("Com", (e) -> {
            communicationStart();
        }, (e) -> {
            communicationStop();
        });

        jControlPanel.addBtn("Calibration", (e) -> {

            String dist = JOptionPane.showInputDialog("Reference Distance [cm]");

            boolean A = true; // means calibration A
            float refDistance = 0;
            try {
                refDistance = Float.valueOf(dist).floatValue();
            } catch (NumberFormatException | NullPointerException ex) {
                Logger.getGlobal().warning("calibration B instead of A");
                A = false;
            }

            if (A) {
                if (refDistance > Config.REF_DIST_MIN && refDistance < Config.REF_DIST_MAX) {
                    calibrationA(refDistance / 100); // to convert it to meters
                } else {
                    JOptionPane.showMessageDialog(null, "Reference Distance is not from [" + Config.REF_DIST_MIN + ","
                            + Config.REF_DIST_MAX + "] cm");
                }
            } else {
                calibrationB();
            }
        });

        jControlPanel.addStartStopBtn("Log", (e) -> {
            int id;

            String sId = JOptionPane.showInputDialog("Insert log id", "1");
            try {
                id = Integer.valueOf(sId).intValue();
            } catch (NumberFormatException | NullPointerException ex) {
                JOptionPane.showMessageDialog(null, "Wrong id number");
                return;
            }

            int sRam = JOptionPane.showConfirmDialog(null, "Log to RAM?", "State Logger", JOptionPane.YES_NO_OPTION);

            StateLoggingStart(id, sRam == JOptionPane.YES_OPTION);
        }, (e) -> {
            StateLoggingStop();
        });

        jControlPanel.addStartStopBtn("Ctr", (e) -> {
            controlStart();
        }, (e) -> {
            controlStop();
        });

        jControlPanel.addBtn("Ready", (e) -> {
            sendStateChange(SlotcarState.READY);
        });

        jControlPanel.addBtn("Manual", (e) -> {
            sendStateChangeSelected(SlotcarState.MANUAL);
        }).addKeyListener(manualControl);

        carSelectionC.addChangeListener((e) -> {

            CarImage car = null;
            try {
                car = carSelectionC.getSelectedCars().get(0);
            } catch (IndexOutOfBoundsException ex) {
                // do nothing
            }

            if (car == null) {
                return;
            }

            if (carConfig.isReadOnClick()) {
                carConfig.readFrom(car);
            } else {
                carConfig.compare(car);
            }
        });

        carConfig.addReadOnClickActionListener((e) -> {
            updateConfig();
        });

        carConfig.addChangeListener((e) -> {
            updateConfig();
            // controllerConfigC.setController(carConfig.getSelectedController());
        });

        carConfig.addBtn("Write changes", (e) -> {
            uploadToSelectedCars();
        });

        carConfig.addBtn("Read changes", (e) -> {
            System.out.println("Downloading selected cars.");
            for (CarImage car : carSelectionC.getSelectedCars()) {
                System.out.println(car.getLogAddr());
                comMan.sendImageRequest(car.getLogAddr());
            }
        });

        /* Main Frame */
        mFrame = new MainFrame("Slotcar GUI");

        mFrame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                Config.getInstance().save(Config.GUI_CONF);
            }
        });

        mFrame.addTab("Parameters", carConfig);
        mFrame.addTab("Network", netControl);
        mFrame.addTab("Controller", controllerConfigV);
        mFrame.addTab("Experiment", experimentV);
        mFrame.addTab("Upgrade", carUpgradeV);

        mFrame.setFocusable(true);
        mFrame.requestFocusInWindow();

        mFrame.addToWest(carSelectionV);

        jControlPanel.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0), "stop_control");

        jControlPanel.getActionMap().put("stop_control", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                MainApp.this.controlStop();
            }
        });

        mFrame.addBtnCreateNetworkActionListener((e) -> {
            reconnectRequest();
        });

        CommunicatorMatlab matlab = new CommunicatorMatlab(Config.MATLAB_COM_PORT, 481,
                480, cars) {
            @Override
            public void write(CarImage carImage) {
                MainApp.this.writeSettings(carImage);
            }
        };

        try {
            matlab.start();
        } catch (SocketException e) {
            Logger.getGlobal().log(Level.SEVERE, "Cannot open the MATLAB socket", e);
        }

        //FXMain simpleGui = new FXMain();
        //FXMLDocumentController simpleC = new FXMLDocumentController(this);
    }

    public void sendToAllAck(ServiceType service, byte[] data) {
        comMan.sendTo(cars.getAddresses(), service, data);
    }

    public void sendToSelected(ServiceType service, byte[] data) {
        comMan.sendTo(carSelectionC.getSelectedCars(), service, data);
    }

    public void sendToSelected(ServiceType service) {
        comMan.sendTo(carSelectionC.getSelectedCars(), service);
    }

    private void StateLoggingStart(int id, boolean ram) {
        String s = String.format("Starting Logging, id = %d, to ram ? %b", id, ram);
        Logger.getGlobal().info(s);
        comMan.sendStateLoggingStart(id, ram, cars.getAddresses());
    }

    private void StateLoggingStop() {
        comMan.sendStateLoggingStop(cars.getAddresses());
    }

    private void communicationStop() {
        comMan.sendCommunicationStop(cars.getAddresses());
    }

    private void communicationStart() {
        syncPositionTable();
        comMan.sendCommunicationStart(cars.getAddresses());
    }

    private void calibrationA(float refDistance) {
        comMan.sendCalibrationA(cars.getAddresses(), refDistance);
    }

    private void calibrationB() {
        comMan.sendCalibrationB(cars.getAddresses());
    }

    public static void main(String[] args) {

        try {
            for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (Exception e) {
            // If Nimbus is not available, you can set the GUI to another look and feel.
        }

        Config.getInstance().load(Config.GUI_CONF);
        Debugging.initGUI();
        MainApp app = MainApp.getInstance();

        FXMLDocumentController.setMain(app);
        FXMain.main(args);

    }

    public void setState(SlotcarState state) {
        this.state = state;
    }

    public void setRefSpeed(float value) {
        ParamsGraphs.params[2].setValue(value);

        cars.getCars().forEach((car) -> {
            carConfig.writeParamsTo(car);
        });
        writeSettings(cars.getCars());
    }

    public void setRefDistance(float value) {
        ParamsGraphs.params[0].setValue(value);
        cars.getCars().forEach((car) -> {
            carConfig.writeParamsTo(car);
        });
        writeSettings(cars.getCars());
    }

    public SlotcarState getState() {
        return state;
    }

    /**
     * Loads selected controller and its configuration into all car
     *
     * @param controller
     */
    public void writeController(String controller) {

        if (cars.size() != 0) {
            System.out.println("Switching controllers.");
            cars.getCars().forEach((car) -> {
                //TODO write right controller
                car.getCarControl().setController(controller);
                carConfig.writeParamsTo(car);
            });
            writeSettings(cars.getCars());

        } else {
            System.out.println("No cars in list");
        }
    }

    public void writeSettings(CarImage carImage) {
        comMan.sendUploadSettings(carImage);
    }

    public void writeSettings(List<CarImage> list) {
        for (CarImage carImage : list) {
            writeSettings(carImage);
        }
    }

    public void syncPositionTable() {
        System.out.print("Synchronizing positioning table. cars: ");
        byte[] addrs = cars.getAddresses();
        for (byte b : addrs) {
            System.out.print(b + ",");
        }
        System.out.println();

        // uses acknowledged communication
        comMan.sendSyncPositionTable(addrs, false);

    }

    public void positioningStart() {
        System.out.println("Starting positioning procedure.");
        positioningProc = new byte[cars.size()];
        posIndex = 0;
        state = SlotcarState.POSITIONING;
        System.out.println(cars);
//		comMan.sendStartPositioning(cars.getAddresses()); // use acknowledged
//															// communication
        comMan.sendStateChange(SlotcarState.POSITIONING, cars.getAddresses());
    }

    public void control(List<CarImage> cars, boolean enable) {
        for (CarImage car : cars) {
            car.getCarControl().setControlled(enable);
        }
        if (enable) {
            comMan.sendControlStart(cars);
        } else {
            comMan.sendControlStop(cars);
        }
    }

    public void controlStart() {
        control(cars.getCars(), true);
    }

    public void controlStop() {
        control(cars.getCars(), false);
    }

    public void controlStartSelected() {
        control(carSelectionC.getSelectedCars(), true);
    }

    public void controlStopSelected() {
        control(carSelectionC.getSelectedCars(), false);
    }

    public void reconnectRequest() {
        comMan.sendReconnectRequest();
    }

    /// Handlers of the event of the communication layer
    @Override
    public void handleCarLoggingRequest(byte logAddr) {
        cars.addCar(logAddr);
        carSelectionC.updateList();
        comMan.sendImageRequest(logAddr);
        comMan.sendSyncRoutingTable(cars.getAddresses());
    }

    public static void processImageData(byte logAddr, byte[] raw, boolean force) {

        CarImage car = cars.get(logAddr);

        if (car == null) {
            Logger.getGlobal().warning("Car " + logAddr + " is not on the list.");
            return;
        }

        //long timeOfChange = car.getCarControl().getTimeOfChange();
        try {
            ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(raw));
            car.read(in, force);
            in.close();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    @Override
    public void handleImage(byte logAddr, byte[] raw) {
        processImageData(logAddr, raw, false);
    }

    @Override
    public void handleImageRequested(byte logAddr, byte[] raw) {
        processImageData(logAddr, raw, true);
        updateConfig(true);
    }

    @Override
    public void handleUnknownPacket(CarPacket e) {
        System.out.println("Service with number " + e.getService() + " is not implemented.");
    }

    @Override
    public void handleSetPosition(byte logAddr) {
        System.out.println("Set position handle");
        try {
            positioningProc[posIndex++] = logAddr;
            System.out.println(cars);
            if (posIndex == cars.size()) {
                System.out.println("All cars reported their position, stopping positioning");
                cars.syncPositions(positioningProc);
//			positioningStop();
                sendStateChange(SlotcarState.READY);
                syncPositionTable();
            }
        } catch (ArrayIndexOutOfBoundsException ex) {
            System.out.println(logAddr + " is late with position response...");
        }
    }

    @Override
    public void handleCommTestResult(byte sourceAddr, int numberOfPackets) {

    }

    @Override
    public void handleControllerConstans(byte[] data) {
        try {
            ByteArrayInputStream s = new ByteArrayInputStream(data);
            carConfig.getControllers().readControllerConstantsFrom(s);
            controllerConfigC.setController(carConfig.getSelectedController());
            controllerConfigV.revalidate();
        } catch (UnsupportedControllerException ex) {
            JOptionPane.showMessageDialog(mFrame, ex.getMessage());
        }
    }

    @Override
    public void handleErrorMessage(String msg) {
        try {
            JOptionPane.showMessageDialog(mFrame, msg);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    static String getTime(long millis) {
        SimpleDateFormat time_formatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss.SSS");
        return time_formatter.format(millis);
    }

    @Override
    public void handleStatus(byte sourceAddr, SlotcarState status, int hashCode, long timeOfChange) {

        String s = String.format("car %d status : %s , hashcode %d , time of change %s", sourceAddr, status.toString(),
                hashCode, getTime(timeOfChange));
        Logger.getGlobal().info(s);
        CarImage car = cars.get(sourceAddr);
        if (car != null) {
            car.setState(status);

            int foo = Long.compare(car.getTimeOfChange(), timeOfChange);

            String s2 = String.format("car %d status : %d", sourceAddr, foo);
            Logger.getGlobal().info(s2);

            // double check from with hashcode
            if (foo == 0 && hashCode != car.getCarControl().hashCode()) {
                car.setSyncStatus(1); // preferer GUI
            } else {
                car.setSyncStatus(foo);
            }

            carSelectionC.updateList();
        }

    }

    public void synchronizeOnline(CarImage car) {

        if (!Config.isOnlineSync()) {
            return;
        }
        switch (Config.getSyncPreference()) {
            case 0:
                int direction = car.getSyncStatus();

                switch (direction) {
                    case -1:
                        comMan.sendImageRequest(car.getLogAddr());
                        break;
                    case 1:
                        comMan.sendUploadSettings(car);
                        break;
                    default:
                        Logger.getGlobal().warning("No synchronization");
                        break;
                }
                break;
            case 1:
                comMan.sendUploadSettings(car);
                break;
            case -1:
                comMan.sendImageRequest(car.getLogAddr());
                break;
        }
    }

    public void checkStatus() {

        for (CarImage car : cars.getCars()) {

            long t = System.currentTimeMillis() - car.getTimeLastSeen();

            if (t > 10000) {
                if (car.getState() == SlotcarState.OFFLINE) {
                    // car is offline, we are done with it
                    // needs to be restarted manualy
                    continue;
                }
                // try to restart the car
                Logger.getGlobal().info("Restarting car " + car.getLogAddr());
                car.setState(SlotcarState.OFFLINE);
                // new Thread(() -> {
                // carUpgradeC.restart(car.getLogAddr());
                // }).start();
            } else if (t > 2000) {
                comMan.sendStatusRequest(car.getLogAddr());
            } else {
                // status is ok, i guess...
            }

            carSelectionC.updateList();
        }
    }

    @Override
    public void updateTimeWhenLastMet(byte sourceAddr) {
        CarImage car = cars.get(sourceAddr);
        if (car != null) {
            car.setTimeLastSeen(System.currentTimeMillis());
            //Logger.getGlobal().info("updating time " + car.getTimeLastSeen());
        }
    }

    public CarSelectionC getCarSelection() {
        return carSelectionC;
    }

    public void sendStateChange(SlotcarState state) {
        comMan.sendStateChange(state, cars.getAddresses());
    }

    public void sendStateChangeSelected(SlotcarState state) {
        for (CarImage car : carSelectionC.getSelectedCars()) {
            comMan.sendStateChange(state, new byte[]{car.getLogAddr()});
        }
    }

    public void sendManualSpeed(float speed) {
        ByteBuffer bf = ByteBuffer.allocate(4);
        bf.putFloat(speed);
        sendToAllAck(ServiceType.MANUAL_SPEED, bf.array());
    }

    @Override
    public void handleClockLog(byte sourceAddr, String text) {
        netControl.setClockLog(sourceAddr, text);

    }

    @Override
    public void handleNetStatLog(byte sourceAddr, String text) {
        netControl.setStatLog(sourceAddr, text);

    }

    @Override
    public void dumpNetStatLog(short[] array) {
        if (netControl.isDumpEnabled()) {
            final DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd:hh-mm-ss");
            BufferedWriter outputWriter = null;
            File dir = new File("logs");
            dir.mkdirs();
            try {
                File netlog = new File(dir, "netlog" + formatter.format(new Date()) + ".dat");
                outputWriter = new BufferedWriter(new FileWriter(netlog));

                for (int i = 0; i < array.length; i++) {
                    outputWriter.write(Short.toString(array[i]));
                    outputWriter.newLine();
                }
                outputWriter.flush();
                outputWriter.close();

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

}
