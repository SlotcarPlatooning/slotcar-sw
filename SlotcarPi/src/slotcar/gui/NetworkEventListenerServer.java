package slotcar.gui;
import slotcar.SlotcarState;
import slotcar.network.CarPacket;
import slotcar.network.ServiceType;

public interface NetworkEventListenerServer  {
    public void handleCarLoggingRequest(byte logAddr);    
    public void handleImage(byte logAddr, byte[] raw);
    public void handleImageRequested(byte logAddr, byte[] raw);
    public void handleSetPosition(byte logAddr);
    public void handleUnknownPacket(CarPacket packet);
    public void handleCommTestResult(byte sourceAddr, int numberOfPackets);
	public void handleControllerConstans(byte[] data);
	public void handleErrorMessage(String msg);
	public void handleStatus(byte sourceAddr, SlotcarState status, int hashCode, long timeOfChange);
	public void updateTimeWhenLastMet(byte sourceAddr);
	public void handleClockLog(byte sourceAddr, String text);
	public void handleNetStatLog(byte sourceAddr, String text);
	public void dumpNetStatLog(short[] array);
}
